import React, {Component} from "react";
//import { NativeRouter, Switch, Route } from "react-router-native";
import { Router, Scene } from 'react-native-router-flux'

import SingleProduct from "../screens/SingleProduct";

//export default class Router extends Component {

    const Routes = () => (
        <Router>
            <Scene key = "root">
                <Scene key = "single" component = {SingleProduct} title = "Single" initial = {true} />
                <Scene key = "test" component = {SingleProduct} title = "Test" />
            </Scene>
        </Router>
    )

    /*render() {
        return (
            <NativeRouter>
                    <Switch>
                        <Route exact path="/single" component={SingleProduct} />
                    </Switch>
            </NativeRouter>
        );
    }*/
//}

export default Routes