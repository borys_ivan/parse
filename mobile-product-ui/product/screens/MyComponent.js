import React, {Component} from 'react';
import {ScrollView, Image, Text} from 'react-native';
import SingleProduct from "./SingleProduct";

class MyComponent extends Component {
    constructor(props) {
        super(props);
        this.backToList = this.backToListOfProduct.bind(this);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            isAboutVisible: false,
            getProductById: null
        };
    }

    componentDidMount() {///listProduct
        fetch("http://localhost:8083/listProduct")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.content
                    });
                    console.log("result.items" + result.content);
                },
                // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
                // чтобы не перехватывать исключения из ошибок в самих компонентах.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {

        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            if (this.state.isAboutVisible === false) {
                return (
                    <ScrollView>

                        {items.map(item => (

                                <table style={{width: '100%'}} key={item.id}
                                       onClick={() => this.goToSinglePage(item.id)}>
                                    <tr>
                                        <td>
                                            <img src={item.image} style={{width: '100px'}} alt="альтернативный текст"/>
                                        </td>
                                        <td>
                                            {item.name}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {item.oldPrice}
                                        </td>
                                        <td>
                                            {item.price}
                                        </td>
                                    </tr>
                                </table>
                            )
                        )
                        }
                    </ScrollView>
                );

            } else {
                return (
                    <div>
                        {this.state.isAboutVisible && this.state.getProductById != null ?
                            <SingleProduct product={this.state.getProductById} backToList={this.backToList}/> : null}
                    </div>
                );
            }
        }
    }

    goToSinglePage = (idOfProduct) => {
        !this.state.isAboutVisible ?
            this.setState({isAboutVisible: true}) :
            this.setState({isAboutVisible: false})

        //fetch('http://localhost:8083/product/', { id: idOfProduct })
                //name=${name}
        //fetch(`http://localhost:8083/product/${idOfProduct}`)
        fetch(`http://localhost:8083/product/${idOfProduct}`)
            .then(response => response.json()).then(result => {
            this.setState({
                getProductById: result
            });
        })
    };

    backToListOfProduct = () => {
        this.setState({isAboutVisible: false})
    }
}

export default MyComponent;
