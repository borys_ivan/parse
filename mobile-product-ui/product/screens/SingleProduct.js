import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import '../style/myComponents.css';

class SingleProduct extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {product} = this.props
        return (
            <View style={styles.container}>
                <table style={{width: '100%'}} key={product.id}>
                    <tr>
                        <td>
                            <div className="back" onClick={() => this.props.backToList()}></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src={product.image}/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>{product.name}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>{product.oldPrice}</p>
                        </td>
                        <td>
                            <p>{product.price}</p>
                        </td>
                    </tr>
                </table>
            </View>
        );
    }
}

SingleProduct.propTypes = {
    product: PropTypes.object.isRequired,
}

export default SingleProduct;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});