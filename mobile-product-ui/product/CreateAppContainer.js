import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
/*import Explore from './screens/Explore';
import Saved from './screens/Saved';
import Inbox from './screens/Inbox';
import Trip from './screens/Trip';*/
import Profile from './screens/Profile';
import Trip from './screens/Trip';
import Explore from './screens/Explore';
import MyComponent from './screens/MyComponent';

const TabNavigator = createBottomTabNavigator({
  MyComponent: {
    screen: MyComponent,
  },
  Explore: {
    screen: Explore,
  },
  Trip: {
    screen: Trip,
  },
  Profile: {
    screen: Profile,
  },
});
const CreateAppContainer = createAppContainer(TabNavigator);

export default CreateAppContainer;

//export default createAppContainer(TabNavigator);

