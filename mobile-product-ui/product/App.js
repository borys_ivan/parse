import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CreateAppContainer from './CreateAppContainer';
import Profile from './screens/Profile';

/*import {createBottomTabNavigator} from 'react-navigation-tabs';
const TabNavigator = createBottomTabNavigator({*/
    /*Explore: {
      screen: Explore,
    },
    Saved: {
      screen: Saved,
    },
    Trip: {
      screen: Trip,
    },*/
    /*Profile: {
        scrTabNavigatoreen: Profile,
    },
});


export default class App extends React.Component {
    render() {
        return <TabNavigator/>;
    }

}*/



export default function App() {
    return (
        /*<View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
            <StatusBar style="auto" />

        </View>*/
    <CreateAppContainer />
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
