package com.parse.Parse.recognizeImage;

import com.google.gson.JsonElement;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.parse.Parse.entity.Access;
import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.repository.AccessRepository;
import com.parse.Parse.repository.CategoryListProductRepository;
import com.parse.Parse.test.ParameterStringBuilder;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ConnectToApi {


    @Autowired
    @Qualifier("accessRepository")
    private AccessRepository accessRepository;



    public void getToken() {

        try {
        URL url = new URL("https://boiling-headland-32210.herokuapp.com/oauth/token");
        //    URL url = new URL("http://localhost:8080/oauth/token");
        HttpURLConnection con = null;

            con = (HttpURLConnection) url.openConnection();


        String encoded = Base64.getEncoder().encodeToString(("devglan-client:devglan-secret").getBytes(StandardCharsets.UTF_8));  //Java 8
        con.setRequestProperty("Authorization", "Basic "+encoded);


        con.setRequestMethod("POST");

            Map<String, String> parameters = new HashMap<>();
            parameters.put("username", "admin");
            parameters.put("password", "pass");
            parameters.put("grant_type", "password");
            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(ParameterStringBuilder.getParamsString(parameters));

            out.flush();
            out.close();


            con.setConnectTimeout(30000);
            con.setReadTimeout(30000);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }


            JsonElement tmpJsonElement = Streams.parse(new JsonReader(new StringReader(content.toString())));
            String accessToken = tmpJsonElement.getAsJsonObject().get("access_token").toString().replaceAll("\"", "");

            Access admin = accessRepository.findFirstById(1);

            if(admin.getAccess_token() == null || !admin.getAccess_token().equals(accessToken)){
                admin.setAccess_token(accessToken);

                accessRepository.save(admin);
            }


            in.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public ArrayList<EntityProduct> connect() {

        JsonElement tmpJsonElement;
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> imageSrc = new ArrayList<>();
        ArrayList<EntityProduct> arrayProduct = new ArrayList<>();
        int count = 0;

        getToken();

        String path = new File("src/main/resources/image").getAbsolutePath();

        try {

            String strImageName;
            String dumpImage;
            String ImagePath;


            Stream<Path> paths = Files.walk(Paths.get(path));
            List<Path> pathsImage = paths.filter(Files::isRegularFile).collect(Collectors.toList());


            for (Path pathImage : pathsImage) {

                strImageName = pathImage.toString().substring(pathImage.toString().lastIndexOf("/") + 1);


                byte[] fileContent = FileUtils.readFileToByteArray(new File(pathImage.toUri()));
                String encodedString = Base64.getEncoder().encodeToString(fileContent);


                URL url = new URL("https://boiling-headland-32210.herokuapp.com/hello");
                //URL url = new URL("http://localhost:8080/hello");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();


                con.setRequestMethod("POST");

                Map<String, String> parameters = new HashMap<>();

                parameters.put("dateImage", encodedString);
                parameters.put("pathImage", "http://kishenya.ua/images/tt/" + strImageName);

                parameters.put("access_token", accessRepository.findFirstById(1).getAccess_token());

                con.setDoOutput(true);
                DataOutputStream out = new DataOutputStream(con.getOutputStream());
                out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
                out.flush();
                out.close();


                String contentType = con.getHeaderField("Content-Type");

                con.setConnectTimeout(30000);
                con.setReadTimeout(30000);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                in.close();



                tmpJsonElement = Streams.parse(new JsonReader(new StringReader(content.toString())));

                dumpImage = tmpJsonElement.getAsJsonObject().get("dumpImage").toString().replaceAll("\"", "");
                ImagePath = tmpJsonElement.getAsJsonObject().get("pathImage").toString().replaceAll("\"", "");

                System.out.println(dumpImage);

                name.add(dumpImage);
                imageSrc.add(ImagePath);


                arrayProduct.add(new EntityProduct(count, name.get(count), 0.0, 0.0,
                        new Date(), null, "Kishenya", imageSrc.get(count)));

                count++;


                //removed files
                Files.delete(pathImage);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return arrayProduct;
    }


}
