package com.parse.Parse.parseTimer;

import com.parse.Parse.Bot.thead.MessageProcessRun;
import com.parse.Parse.system.TimeRestart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("RunAfterStart")
public class RunAfterStart implements InitializingBean, DisposableBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartParseATB.class);

    @Autowired
    private static ApplicationContext context;

    @Autowired
    private StartParseATB startParseATB;

    @Autowired
    private StartParseFurshet startParseFurshet;

    @Autowired
    private StartParseSilpo startParseSilpo;

    @Autowired
    private TimeRestart timeRestart;

    @Autowired
    public MessageProcessRun messageProcessRun;


    public static void main(String[] args) {

        try {

            RunAfterStart service =
                    (RunAfterStart) context.getBean("RunAfterStart");
            service.afterPropertiesSet();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info("JOB TIMER START!");
        startParseATB.start();
        startParseFurshet.start();
        startParseSilpo.start();


        timeRestart.start();


//Run RemindMessageProduct, inform about new product
        messageProcessRun.runProcessBot();



    }

    @Override
    public void destroy() throws Exception {

    }
}
