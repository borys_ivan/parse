package com.parse.Parse.parseTimer;

import com.parse.Parse.entity.Company;
import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.parse.Parse;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Component
public class StartParseFurshet {

    private Date date;
    private DateFormat format;
    private String string;

    @Autowired
    @Qualifier("companyRepository")
    private CompanyRepository companyRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private ApplicationContext companyParse;

    private static final Logger LOGGER = LoggerFactory.getLogger(StartParseFurshet.class);

    public void start() {

        List<Company> company = companyRepository.findAll(Sort.by("id"));

        try {

             string = company.get(1).getStartTime().toString();

             format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            try {
                date = format.parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Timer timer=new Timer();
            timer.schedule(new FurshetTask(), date);

        }catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    class FurshetTask extends TimerTask {

        @Override
        public void run() {
            DateFormat dateFormat = new SimpleDateFormat("EE MMM dd HH:mm:ss zzz yyyy",Locale.ENGLISH);
            LOGGER.info("Plan parse " + new Date().toString());
            LOGGER.info("Updated or Added products from Furshet");

            Parse parse=(Parse) companyParse.getBean("Furshet");

            String state = productService.saveProduct(parse.getInform());

            if (state.equals("Parse this market")) {

                try {

                    Calendar c = Calendar.getInstance();

//add date and plus days
                    c.setTime(format.parse(string));
                    c.add(Calendar.DATE, 1);

//updated date
                    Company dateStart = companyRepository.findAllByName("Furshet");
                    date = dateFormat.parse(c.getTime().toString());
                    dateStart.setStartTime(date);
                    companyRepository.save(dateStart);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                LOGGER.info("Checking product");
                productService.checkProduct(parse.getInform(),"Furshet");

                LOGGER.info("Updated product");
                productService.updateProduct(parse.getInform());

                LOGGER.info("it's OK");
            } else {
                LOGGER.info("Problem with Parse");
            }
        }
    }
}
