package com.parse.Parse;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.parseTimer.RunAfterStart;
import com.parse.Parse.parseTimer.StartParseATB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@SpringBootApplication
public class ParseApplication extends SpringBootServletInitializer {


	/*static {
		ApiContextInitializer.init();

		TelegramBotsApi botsApi = new TelegramBotsApi();

		try {
			botsApi.registerBot(new ProductBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}*/


	public static void main(String[] args) {

		//ApiContextInitializer.init();
		//SpringApplication.run(ProductBot.class, args);


		SpringApplication.run(ParseApplication.class, args);



	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ParseApplication.class);
	}


	/*public static void restart() {
		ApplicationArguments args = context.getBean(ApplicationArguments.class);

		Thread thread = new Thread(() -> {
			context.close();
			context = SpringApplication.run(ParseApplication.class, args.getSourceArgs());
		});

		thread.setDaemon(false);
		thread.start();
	}*/




}
