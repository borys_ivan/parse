package com.parse.Parse.system;

import com.parse.Parse.entity.ProcessValue;
import com.parse.Parse.parseTimer.StartParseATB;
import com.parse.Parse.repository.ProcessValueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

@Service
public class TimeRestart {

    private Date date;

    @Autowired
    private Restart restart;

    @Autowired
    @Qualifier("ProcessValueRepository")
    private ProcessValueRepository processValueRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(StartParseATB.class);

    public void start() {

        LOGGER.info("Restart process");

        ProcessValue dateProcess = processValueRepository.findAllByName("restart");
        Timer timer = new Timer();
        timer.schedule(new DoRestart(), dateProcess.getDate());

//add date and add days

        Calendar c = Calendar.getInstance();
        c.setTime(dateProcess.getDate());
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        dateProcess.setDate(date);

        LOGGER.info("updated Restart time");

        processValueRepository.save(dateProcess);

    }

    public class DoRestart extends TimerTask {

        @Override
        public void run() {
            restart.restartApp();
        }
    }


}
