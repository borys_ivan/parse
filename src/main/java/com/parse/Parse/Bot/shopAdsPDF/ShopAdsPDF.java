package com.parse.Parse.Bot.shopAdsPDF;

import com.parse.Parse.entity.Company;
import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;


@Service("ShopAdsPDF")
public class ShopAdsPDF extends AbstractParser {

    @Autowired
    @Qualifier("companyRepository")
    private CompanyRepository companyRepository;

    @Autowired
    @Qualifier("MakePdfFile")
    private MakePdfFile makePdfFile;

    @Autowired
    @Qualifier("ForaPDF")
    private ForaPDF foraPDF;


    @Autowired
    @Qualifier("parseProcessRepository")
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    public void parsePDF() {

        TransformToPDF transformToPDF = new ForaPDF();



        //-------------------------------------------------------------------------------------

        String UrlEko = "https://www.eko.com.ua/ua/newspaper/city/12/";

        String pdfFileEko = null;
        try {
            pdfFileEko = connectSetting(UrlEko).get().body().select("div.newspaper_item a").toString().replaceAll(".*href=\"(.*?)\" target.*","$1");
        } catch (IOException e) {
            e.printStackTrace();
        }

        saveJsonInFieldOfDB(pdfFileEko,"EkoMarket","links");

        //-------------------------------------------------------------------------------------

        String UrlPchelkamarket = "https://pchelkamarket.kiev.ua/specialproposals/";

        String pdfFilePchelkamarket = null;
        try {
            pdfFilePchelkamarket = connectSetting(UrlPchelkamarket).get().body().select("div.content-text a").toString().replaceAll(".*href=\"(.*?)\" target.*","$1");
        } catch (IOException e) {
            e.printStackTrace();
        }

        saveJsonInFieldOfDB(pdfFilePchelkamarket,"Pchelka","links");





        //-------------------------------------------------------------------------------------


        //Company fora = companyRepository.findAllByName("Fora");

        ArrayList<String> filesPDF = new ArrayList<>();


         transformToPDF.createdPDF("https://fora.ua","foraPropositions",foraPDF.aktsiynaHazeta());

        filesPDF.add("src/main/java/com/parse/Parse/Bot/shopAdsPDF/filePDF/foraPropositions.pdf");
        //fora.setLinkPDF("src/main/java/com/parse/Parse/Bot/shopAdsPDF/filePDF/foraPropositions.pdf");
        //companyRepository.save(fora);


        ArrayList<String[]> otherAktsiya = foraPDF.otherAktsiya();

        int countPDFfile = 0;

        for(String[] aktsiya : otherAktsiya) {

            transformToPDF.createdPDF("https://fora.ua", "otherAktsiya"+countPDFfile, aktsiya);

            countPDFfile++;

            filesPDF.add("src/main/java/com/parse/Parse/Bot/shopAdsPDF/filePDF/otherAktsiya"+countPDFfile+".pdf");
        }

        System.out.println(filesPDF);

        saveJsonInFieldOfDB(filesPDF,"Fora","paths");

    }

    public void saveJsonInFieldOfDB(String pdfFile,String nameOfCompany, String typeOfArray) {

        String link;

        switch (typeOfArray) {

            case "links":
                link = "{\"links\": [\"" + pdfFile + "\"]}";
                break;
            case "paths":
                link = "{\"paths\": [\"" + pdfFile + "\"]}";
                break;
            default:
                link = "{\"null\": [\"\"]}";
                break;
        }

        JSONObject json = null;
        JSONParser parser = new JSONParser();
        try {
            json = (JSONObject) parser.parse(link);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Company company = companyRepository.findAllByName(nameOfCompany);
        if (json != null) {
            company.setLinkPDF(json.toString());
            companyRepository.save(company);
        }

    }

    public void saveJsonInFieldOfDB(ArrayList<String> pdfFiles,String nameOfCompany, String typeOfArray) {

        StringBuilder link = null;

        switch (typeOfArray) {

            case "links":
                link = new StringBuilder("{\"links\": [\"" + pdfFiles.get(0));
                for (int i = 1; i<pdfFiles.size();i++){
                    link.append(",").append(pdfFiles.get(i));
                }
                link.append("\"]}");
                break;
            case "paths":
                link = new StringBuilder("{\"paths\": [\"" + pdfFiles.get(0));
                for (int i = 1; i<pdfFiles.size();i++){
                    link.append(",").append(pdfFiles.get(i));
                }
                link.append("\"]}");
                break;
            default:
                link = new StringBuilder("{\"null\": [\"\"]}");
                break;
        }

        JSONObject json = null;
        JSONParser parser = new JSONParser();
        try {
            json = (JSONObject) parser.parse(link.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Company pdf = companyRepository.findAllByName(nameOfCompany);
        if (json != null) {
            pdf.setLinkPDF(json.toString());
            companyRepository.save(pdf);
        }

    }

    }
