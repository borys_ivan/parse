package com.parse.Parse.Bot.shopAdsPDF;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.parse.Parse.parse.AbstractParser;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

@Service("ForaPDF")
public class ForaPDF extends AbstractParser implements TransformToPDF {

    public String[] aktsiynaHazeta(){

        String Url = "https://fora.ua/aktsiyi/aktsiyna-hazeta/";

        String linksImage = null;

        try {

            linksImage = Pattern.compile(
                    "(?:<div class=\"default-wrapper\">.*?)?\\s?\\s?<div class=\"slide\">\\s*<div class=\"desktop-photo\">\\s*.*?" +
                            "img src\\=\\\"(.*?)\\\"\\s*alt=\"photo\"\\>?\\s*\\<\\/div\\>\\s*\\<\\/div\\>\\s?(?:\\s*<\\/section>\\s*<div class=\"default-editor grey\">" +
                            ".*?<\\/div\\>\\s*\\<\\/div\\>)?",
                    Pattern.DOTALL)
                    .matcher(connectSetting(Url).get().body().select("div.default-wrapper").toString()).replaceAll("$1");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return linksImage.split("\\n");

    }

    public ArrayList<String[]> otherAktsiya(){

        ArrayList<String> arrayLink = new ArrayList<>();

        ArrayList<String[]> arrayImages = new ArrayList<>();

        arrayLink.add("https://fora.ua/aktsiyi/lovi-znizhki/");
        arrayLink.add("https://fora.ua/aktsiyi/inshi-aktsii/");
        arrayLink.add("https://fora.ua/aktsiyi/smachniy-katalog/");

        String linksImage = null;

        for (String link : arrayLink) {

            try {

                linksImage = connectSetting(link).get().body().select("div.desktop-photo img").toString().replaceAll("<img src=\"(.*?)\" alt=\"photo\">", "$1");

            } catch (IOException e) {
                e.printStackTrace();
            }

            arrayImages.add(linksImage.split("\\n"));

        }

        return arrayImages;

    }

    @Override
    public void createdPDF(String linkOfCompany, String namePdfFile, String[] arrayLinks) {

        try {

            Image img = Image.getInstance(linkOfCompany + arrayLinks[0]);
            Document document = new Document(img);
            PdfWriter.getInstance(document, new FileOutputStream("src/main/java/com/parse/Parse/Bot/shopAdsPDF/filePDF/"+namePdfFile+".pdf"));

            document.open();

            for (String link : arrayLinks) {

                img = Image.getInstance(linkOfCompany + link);

                document.setPageSize(img);
                document.newPage();
                img.setAbsolutePosition(0, 0);

                document.add(img);

            }

            document.close();

        } catch (DocumentException | IOException e) {
            e.printStackTrace();

        }

    }


}
