package com.parse.Parse.Bot;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.Bot.message.MessageAction;
import com.parse.Parse.Bot.session.UserSession;
import com.parse.Parse.Bot.template.Template;
import com.parse.Parse.Bot.thead.DowloadMenuThread;
import com.parse.Parse.Bot.thead.MessageProcessRun;
import com.parse.Parse.Bot.thead.StartMenu;
import com.parse.Parse.Bot.thead.ThreadProcessBot;
import com.parse.Parse.entity.*;
import com.parse.Parse.repository.*;
import com.parse.Parse.service.ProductService;
import com.parse.Parse.service.ProductServiceImpl;
import com.parse.Parse.service.RequestUserBotServiceImpl;
import com.parse.Parse.service.UsersBotServiceImpl;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.util.*;
import java.util.List;


@Component
public class ProductBot extends TelegramLongPollingBot {


    int listCount = 5;


    @Autowired
    private ApplicationContext context;

    @Autowired
    public ProductService productService;

    @Autowired
    private ProductServiceImpl productTest;

    @Qualifier("productRepository")
    @Autowired
    private ProductRepository productRepository;

    @Qualifier("companyRepository")
    @Autowired
    private CompanyRepository companyRepository;

    @Qualifier("usersBotRepository")
    @Autowired
    private UsersBotRepository usersBotRepository;

    @Qualifier("userListProductsRepository")
    @Autowired
    private UserListProductsRepository userListProductsRepository;

    @Qualifier("requestUserBotRepository")
    @Autowired
    private RequestUserBotRepository requestUserBotRepository;

    @Qualifier("shopsRepository")
    @Autowired
    private ShopsRepository shopsRepository;

    @Autowired
    private RequestUserBotServiceImpl requestUserBotServiceImpl;

    @Autowired
    private MessageAction messageAction;

    @Autowired
    private UsersBotServiceImpl usersBotService;

    @Autowired
    private ThreadProcessBot threadProcessBot;

    @Autowired
    private StartMenu startMenu;

    @Autowired
    public Template template;

    @Autowired
    public DowloadMenuThread dowloadMenuThread;

    static {
        ApiContextInitializer.init();
    }


    public SendMessage reguestMessage = new SendMessage();
    public SendMessage globalMessageChat = new SendMessage();
    public SendPhoto testImage = new SendPhoto();

    //public GeoListOfShops geoListOfShops;

    public static boolean geoSearch = false;
    public static Location geoLocationForProduct = new Location();


    public Map<String, SendMessage> reguestAPI = new HashMap<>();


    public Map<String, Integer> pageCount = new HashMap<>();

    public static Map<Integer, UserSession> userSessions = new HashMap<>();


    private Message compareReguest;

    Location location = new Location();

    @Override
    public void onUpdateReceived(Update update) {

        if (update.getMessage() != null && update.getMessage().hasText()) {

            usersBotService.createdOrUpdatedUserBot(update);

//System.out.println(update);
            //if (message.getText().equals("/help")) {
            //sendMsg(message, "Привет, я робот");
            //System.out.println("MAIN"+message.getText());

        }
        //System.out.println("--------AAAAAAAAAAAAAAAAAAAAAAAAAA-------------------------------");


        if(update.getMessage()!=null){
            //if(update.getMessage()!=null && userSessions.get(update.getMessage().getChat().getId().intValue())!=null){
            //System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBB "+userSessions.get(update.getMessage().getChat().getId().intValue()).getCallBackRequest().getData());}
            UserSession userSession = new UserSession(update.getMessage().getFrom().getId(), update.getMessage().getFrom());
            //userSession.setUpdate(update);

            //userSessions.put(update.getMessage().getChat().getId().intValue(),userSession);

        }else {
            if(update.getCallbackQuery()!=null && userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue())!=null) {
                //System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBB " + userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getData());
            }
            UserSession userSession = new UserSession(update.getCallbackQuery().getMessage().getFrom().getId(), update.getCallbackQuery().getMessage().getFrom());
            //userSession.setUpdate(update);

            //userSessions.put(update.getCallbackQuery().getMessage().getChat().getId().intValue(),userSession);
        }


        if (update.getMessage() != null || update.getCallbackQuery() != null) {

            Message updateActionMessageTest;

            if(update.getMessage()!=null){
                updateActionMessageTest = update.getMessage();
            } else {
                updateActionMessageTest = update.getCallbackQuery().getMessage();
            }

            System.out.println("updateActionMessageTest updateActionMessageTest updateActionMessageTest "+ updateActionMessageTest);

            userSessions.put(updateActionMessageTest.getChat().getId().intValue(),new UserSession(updateActionMessageTest.getFrom().getId(), updateActionMessageTest.getFrom()));
            userSessions.get(updateActionMessageTest.getChat().getId().intValue()).oldReguestMessanger.add(updateActionMessageTest);

            if (update.getMessage() != null && update.getMessage().getText() != null) {


                if (!userSessions.containsKey(update.getMessage().getFrom().getId())) {

                    userSessions.put(update.getMessage().getFrom().getId(), new UserSession(update.getMessage().getFrom().getId(), update.getMessage().getFrom()));

                    // And From your main() method or any other method
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    System.out.println("created clean thread : " + threadProcessBot.time / 60000);


                    threadProcessBot.runProcessBot(update, userSessions);

                    System.out.println("ID " + update.getMessage().getFrom().getId());
                    System.out.println("HashMap " + userSessions.get(update.getMessage().getFrom().getId()));

                } else {

                    /*if (!userSessions.containsKey(update.getCallbackQuery().getMessage().getFrom().getId())) {

                        userSessions.put(update.getCallbackQuery().getFrom().getId(), new UserSession(update.getCallbackQuery().getMessage().getFrom().getId(), update.getMessage().getFrom()));

                    }*/


                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    System.out.println("updated timer clean chat : " + threadProcessBot.time / 60000);

                    ///////  //threadProcessBot.timer.cancel();

                    threadProcessBot.runProcessBot(update, userSessions);

                    System.out.println("ID " + update.getMessage().getFrom().getId());
                    System.out.println("HashMap " + userSessions.get(update.getMessage().getFrom().getId()));
                }


                userSessions.get(update.getMessage().getFrom().getId()).setReguest(update.getMessage());
                //userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setReguest(update.getMessage());


            }


            if (update.getMessage() != null && update.getMessage().getText() != null && update.getMessage().getText().equals("/start")) {

                Message updateActionMessage;

                if(update.getMessage()!=null){
                    updateActionMessage = update.getMessage();
                } else {
                    updateActionMessage = update.getCallbackQuery().getMessage();
                }

                messageAction.deleteMessage(userSessions.get(updateActionMessage.getChat().getId().intValue()));

                userSessions.remove(updateActionMessage.getChat().getId().intValue());

                userSessions.put(updateActionMessage.getChat().getId().intValue(), new UserSession(updateActionMessage.getFrom().getId(), updateActionMessage.getFrom()));

                template.generalMenu(update.getMessage(), userSessions);

            }


            try {

                List<Product> callMethod;
                BotRouter router = new BotRouter(productTest, productRepository);


                if (update.hasCallbackQuery() && update.getCallbackQuery().getData().equals("pageNext")) {

                    messageAction.deleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()));

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage() <= userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getTotalCountPage()) {
                        userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCurrentPage(userSessions.get(update.getCallbackQuery().getFrom().getId()).getCurrentPage() + 1);
                    }

                    int idUser = update.getCallbackQuery().getMessage().getChat().getId().intValue();

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getThisMessage()) {
                        callMethod = router.create(BotController.class, "/find", userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getReguest().getText(), userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCompany().split("\\s", 2)[1], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage(), 5, idUser);

                        messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getReguest(), callMethod, userSessions);
                    }

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getThisCallBackMessage()) {
                        callMethod = router.create(BotController.class, "/find", userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getData().split("\\s")[2], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCompany().split("\\s", 2)[1], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage(), 5, idUser);

                        messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getMessage(), callMethod, userSessions);
                    }


                } else if (update.hasCallbackQuery() && update.getCallbackQuery().getData().equals("pageBack")) {

                    messageAction.deleteMessage(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()));

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage() >= 1) {
                        userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCurrentPage(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage() - 1);
                    }

                    int idUser = update.getCallbackQuery().getMessage().getChat().getId().intValue();

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getThisMessage()) {
                        callMethod = router.create(BotController.class, "/find", userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getReguest().getText(), userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCompany().split("\\s", 2)[1], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage(), 5, idUser);

                        messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getReguest(), callMethod, userSessions);
                    }

                    if (userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getThisCallBackMessage()) {
                        callMethod = router.create(BotController.class, "/find", userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getData().split("\\s")[2], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCompany().split("\\s", 2)[1], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage(), 5, idUser);

                        messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getMessage(), callMethod, userSessions);
                    }

                } else if (update.getMessage() != null && update.getMessage().getText() != null && userSessions.get(update.getMessage().getFrom().getId()).getCompany() != null) {

                    userSessions.get(update.getMessage().getFrom().getId()).oldReguestMessanger.add(update.getMessage());
                    userSessions.get(update.getMessage().getFrom().getId()).setCurrentPage(0);

                    userSessions.get(update.getMessage().getFrom().getId()).setThisMessage(true);
                    userSessions.get(update.getMessage().getFrom().getId()).setThisCallBackMessage(false);

                    Integer idUser = update.getMessage().getFrom().getId();


//count users request and save in db;
                    requestUserBotServiceImpl.countUsersRequest(update);


                    callMethod = router.create(BotController.class, "/find", userSessions.get(update.getMessage().getFrom().getId()).getReguest().getText(), userSessions.get(update.getMessage().getFrom().getId()).getCompany(), userSessions.get(update.getMessage().getFrom().getId()).getCurrentPage(), 5, idUser);

                    messageAction.sendMsg(userSessions.get(update.getMessage().getFrom().getId()).getReguest(), callMethod, userSessions);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        if (update.getMessage() != null && update.getMessage().getText() != null && update.getMessage().getText().equals("/findByProduct") || update.hasCallbackQuery() && update.getCallbackQuery().getData().equals("/findByProduct")) {

            //DeleteMessage deleteMessage = new DeleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation.getChatId().toString(), userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation.getMessageId());

            if (update.getMessage() != null) {

                messageAction.deleteMessage(userSessions.get(update.getMessage().getFrom().getId()));

            } else {

                messageAction.deleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()));
            }

            /*try {
                execute(deleteMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }*/

            threadProcessBot.hello.setState(false);

            if (update.getMessage() != null) {
                /////////userSessions.get(update.getMessage().getFrom().getId()).setCompany(null);
                messageAction.deleteMessage(userSessions.get(update.getMessage().getFrom().getId()));
            } else {
                ///////////userSessions.get(update.getCallbackQuery().getFrom().getId()).setCompany(null);
                messageAction.deleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()));
            }
            // testCompany=null;
            // testRequest=null;


            Message message1 = update.getMessage();

            Integer messageId = null;
            Long chatID = null;

            System.out.println("call SELECT market");

            if (message1 == null) {
                message1 = update.getCallbackQuery().getMessage();
                messageId = update.getCallbackQuery().getFrom().getId();
                chatID = update.getCallbackQuery().getMessage().getChatId();

            } else {
                userSessions.get(update.getMessage().getFrom().getId()).oldReguestMessanger.add(message1);
                messageId = message1.getFrom().getId();
                chatID = message1.getChatId();
                //oldReguestMessanger.add(message1);
            }


            template.listOfMarket(chatID, messageId, userSessions);

        }


        if (update.getMessage() != null && userSessions.get(update.getMessage().getFrom().getId()).getCallBackRequest() != null &&
                (userSessions.get(update.getMessage().getFrom().getId()).getCallBackRequest().getData().equals("/getGoeLocation")
                || userSessions.get(update.getMessage().getFrom().getId()).getCallBackRequest().getData().split("\\s")[0].equals("/findNearByShopByName")
                )) {

            if(userSessions.get(update.getMessage().getFrom().getId()).getCallBackRequest().getData().split("\\s")[0].equals("/findNearByShopByName"))
            userSessions.get(update.getMessage().getFrom().getId()).setCompany(userSessions.get(update.getMessage().getFrom().getId()).getCallBackRequest().getData().split("\\s")[1]);

            template.distanceToShop(update, userSessions);

        }


        if (update.getCallbackQuery() != null && update.getCallbackQuery().getData().split("\\s")[0].equals("/findNearByShopByName")) {

            userSessions.put(update.getCallbackQuery().getMessage().getChat().getId().intValue(), new UserSession(update.getCallbackQuery().getMessage().getChat().getId().intValue(), update.getCallbackQuery().getFrom()));

            template.geoLocation(update, userSessions);

            userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCallBackRequest(update.getCallbackQuery());

        }


        if (update.hasCallbackQuery()) {

            String request = update.getCallbackQuery().getData();

            if (update.getCallbackQuery().getData().equals("/getGoeLocation")) {

                template.geoLocation(update, userSessions);

                userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCallBackRequest(update.getCallbackQuery());

            }


            if (userSessions.get(+update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest() != null)
                System.out.println(userSessions.get(+update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCallBackRequest().getData());


            if (update.getCallbackQuery().getData().equals(request) && request.split("\\s")[0].equals("/find") && request.split("\\s")[1].equals("All") && usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getRemind() != null && usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getRemind()) {

                UsersBot usersBotRemind = usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue());
                usersBotRemind.setRemind(false);
                usersBotRepository.save(usersBotRemind);


                userSessions.put(update.getCallbackQuery().getMessage().getChat().getId().intValue(), new UserSession(update.getCallbackQuery().getMessage().getChat().getId().intValue(), update.getCallbackQuery().getFrom()));
                userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setThisMessage(false);
                userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setThisCallBackMessage(true);
                userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCallBackRequest(update.getCallbackQuery());
                userSessions.get(update.getCallbackQuery().getFrom().getId()).setCompany(request.split("\\s")[1]);
                userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).setCurrentPage(0);

                int idUser = update.getCallbackQuery().getMessage().getChat().getId().intValue();


                List<Product> callMethod;
                BotRouter router = new BotRouter(productTest, productRepository);

                //callMethod = router.create(BotController.class, "/find", userSessions.get(update.getMessage().getFrom().getId()).getReguest().getText(), userSessions.get(update.getMessage().getFrom().getId()).getCompany().split("\\s", 2)[1], userSessions.get(update.getMessage().getFrom().getId()).getCurrentPage(), 5,idUser);
                //callMethod = router.create(BotController.class, "/find", request.split("\\s")[2], request.split("\\s")[1], 0, 5,update.getCallbackQuery().getFrom().getId());
                callMethod = router.create(BotController.class, "/find", request.split("\\s")[2], request.split("\\s")[1], userSessions.get(update.getCallbackQuery().getMessage().getChat().getId().intValue()).getCurrentPage(), 5, idUser);

                //messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getFrom().getId()).getReguest(),callMethod,userSessions);
                messageAction.sendMsg(userSessions.get(update.getCallbackQuery().getFrom().getId()).getCallBackRequest().getMessage(), callMethod, userSessions);


            } else if ((update.getCallbackQuery().getData()).equals(request) && request.split("\\s")[0].equals("/find")) {

                Company company = companyRepository.findAllByName(update.getCallbackQuery().getData().replaceAll("/find ", ""));

                if (company != null && company.getPDF() != null && company.getPDF()) {

                    SendMessage message = new SendMessage() // Create a message object object
                            .setChatId(update.getCallbackQuery().getMessage().getChatId())
                            .setText("Набір акцій із журналу: ");

                    Company fora = companyRepository.findById(company.getId()).get();


                    JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                            unescapeJava(fora.getLinkPDF())).getAsJsonObject();


                    JsonArray arrayLinks = null;

                    if (jsonObject.getAsJsonObject().getAsJsonArray("paths") != null) {

                        arrayLinks = jsonObject.getAsJsonObject().getAsJsonArray("paths");

                    } else {

                        arrayLinks = jsonObject.getAsJsonObject().getAsJsonArray("links");

                    }

                    try {
                        Message messageInfo = execute(message);
                        userSessions.get(Integer.valueOf(message.getChatId())).oldReguestMessanger.add(messageInfo);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                    dowloadMenuThread.setChatID(Long.valueOf(message.getChatId()));
                    dowloadMenuThread.setDownload(true);

                    //dowloadMenuThread.run();
                    Thread ttt = new Thread(dowloadMenuThread);

                    ttt.start();

                    SendDocument linksOfPDF = null;

                    for (JsonElement link : arrayLinks) {

                        if (jsonObject.getAsJsonObject().getAsJsonArray("paths") != null) {

                            linksOfPDF = new SendDocument().setChatId(update.getCallbackQuery().getMessage().getChatId()).setDocument(new File(link.getAsString()));

                        } else {

                            linksOfPDF = new SendDocument().setChatId(update.getCallbackQuery().getMessage().getChatId()).setDocument(link.getAsString());

                        }

                        //SendDocument linksOfPDF = new SendDocument().setChatId(update.getCallbackQuery().getMessage().getChatId()).setDocument(new File(link.getAsString()));
                        try {
                            Message messagePDF = execute(linksOfPDF); // Sending our message object to user
                            userSessions.get(Integer.valueOf(message.getChatId())).oldReguestMessanger.add(messagePDF);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }

                    }


                    dowloadMenuThread.setDownload(false);
                    ttt.stop();
                    dowloadMenuThread.delete();


                    InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    List<InlineKeyboardButton> list = new ArrayList<>();


                    list.add(new InlineKeyboardButton().setText("Повернутись до вибору магазину").setCallbackData("/findByProduct"));
                    rowsInline.add(list);

                    markupInlineBackMenu.setKeyboard(rowsInline);

                    /*SendMessage backMenu = new SendMessage();
                    backMenu.setChatId(message.getChatId());
                    backMenu.setText("Можете продовжити пошук в даному магазин або ви можете повернутись до вибору нового");*/

                    SendMessage backMenu = new SendMessage();
                    backMenu.setChatId(message.getChatId());
                    backMenu.setText("Меню вибору:");

                    backMenu.setReplyMarkup(markupInlineBackMenu);

                    //ProductBot addMenuProduct = (ProductBot)context.getBean("productBot");

                    try {

                        //Message menu = addMenuProduct.execute(backMenu);
                        Message messageMenu = execute(backMenu);
                        //oldReguestMessanger.add(menu);
                        //userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(menu);
                        userSessions.get(Integer.valueOf(message.getChatId())).oldReguestMessanger.add(messageMenu);

                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }


                } else {

                    userSessions.get(update.getCallbackQuery().getFrom().getId()).setCompany(request.split("\\s")[1]);

                    Chat messageID = update.getCallbackQuery().getMessage().getChat();
                    SendMessage sendMessage = new SendMessage().setChatId(messageID.getId());
                    sendMessage.setText("Введіть продукт який хочете знайти");

                    try {
                        Message regProduct = execute(sendMessage);
                        userSessions.get(update.getCallbackQuery().getFrom().getId()).oldReguestMessanger.add(regProduct);
                        //oldReguestMessanger.add(regProduct);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                }

            }


            if ((update.getCallbackQuery().getData()).equals(request) && request.equals("/compare")) {

                //System.out.println("compare "+userSessions.get(update.getCallbackQuery().getFrom().getId()).getReguest().getText());
                compareReguest = userSessions.get(update.getCallbackQuery().getFrom().getId()).getReguest();


                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                markupInline.setKeyboard(template.menuShopTemplate(update, request));

                try {

                    //sendMsg(update.getCallbackQuery().getMessage().getChatId(),update.getCallbackQuery().getFrom().getId(), markupInline);
                    messageAction.sendMsg(update.getCallbackQuery().getMessage().getChatId(), update.getCallbackQuery().getFrom().getId(), markupInline, userSessions);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            if ((update.getCallbackQuery().getData()).equals(request) && compareReguest != null && request.split("\\s")[0].equals("/compare") && request.split("\\s").length == 2) {

                List<Product> callMethod;
                BotRouter router = new BotRouter(productTest, productRepository);

                callMethod = router.create(BotController.class, "/find", compareReguest.getText(), update.getCallbackQuery().getData().split("\\s")[1], 0, 0, update.getCallbackQuery().getFrom().getId());

                //compareTemplate(userSessions.get(update.getCallbackQuery().getFrom().getId()).getReguest(),callMethod,update.getCallbackQuery().getData().split("\\s")[1]);

                try {

                    Message menu = execute(template.compareTemplate(userSessions.get(update.getCallbackQuery().getFrom().getId()).getReguest(), callMethod, update.getCallbackQuery().getData().split("\\s")[1], userSessions));
                    userSessions.get(update.getCallbackQuery().getFrom().getId()).oldReguestMessanger.add(menu);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }


            if ((update.getCallbackQuery().getData()).equals(request) && request.split("\\s")[0].equals("/mapLocation")) {

                SendLocation sendLocation = new SendLocation();
                sendLocation.setLatitude(Float.valueOf(request.split("\\s")[1]));
                sendLocation.setLongitude(Float.valueOf(request.split("\\s")[2]));
                sendLocation.setChatId(update.getCallbackQuery().getMessage().getChatId());

                try {

                    if (userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation != null) {

                        DeleteMessage deleteMessage = new DeleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation.getChatId().toString(), userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation.getMessageId());

                        try {
                            execute(deleteMessage);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }

                    }

                    Message mapMessage = execute(sendLocation);

                    userSessions.get(update.getCallbackQuery().getFrom().getId()).mapShopLocation = mapMessage;

                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }

            }


            if (update.getCallbackQuery() != null && update.getCallbackQuery().getData().equals("/getGoeLocationRadius")) {

                template.geoLocation(update, userSessions);
                userSessions.get(update.getCallbackQuery().getMessage().getChatId().intValue()).setCallBackRequest(update.getCallbackQuery());

            }


        }


        if (update.getMessage() != null && update.getMessage().getText() != null && update.getMessage().getText().equals("/productRadius") ||
                update.getCallbackQuery() != null && update.getCallbackQuery().getData() != null && update.getCallbackQuery().getData().equals("/productRadius")) {

            if (update.getMessage() != null) {

                template.addMenuProductTest(update.getMessage(), userSessions);

            } else {

                template.addMenuProductTest(update.getCallbackQuery().getMessage(), userSessions);

            }

        }


        if (update.getCallbackQuery() != null && update.getCallbackQuery().getData().equals("/getGoeLocation")) {

            userSessions.get(update.getCallbackQuery().getFrom().getId()).setCallBackRequest(update.getCallbackQuery());

        }

        if (geoLocationForProduct != null && !geoSearch && update.getCallbackQuery() != null && update.getCallbackQuery().getData().equals("/repeatGeoSearch")) {

            geoSearch = true;

            SendMessage backMenu = new SendMessage();
            backMenu.setChatId(update.getCallbackQuery().getMessage().getChatId());
            backMenu.setText("Можете продовжити пошук, Введіть товар який хочете знайти в радіусі 15хв");


            try {
                execute(backMenu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }


        if (geoLocationForProduct != null && geoSearch) {

            HashMap<Product, List<Shops>> listOfProducts = template.distanceRadiusToProduct(userSessions.get(update.getMessage().getChatId().intValue()).getGeoListOfShops().getIdOfShops(), userSessions.get(update.getMessage().getChatId().intValue()).getGeoListOfShops().getListOfShops(), update.getMessage().getText());

            template.listImageProductAndShopsTemplate(update.getMessage(), listOfProducts, userSessions);

            geoSearch = false;

        }


        if (update.getMessage() != null && userSessions.get(update.getMessage().getChatId().intValue()).getCallBackRequest() != null && userSessions.get(update.getMessage().getChatId().intValue()).getCallBackRequest().getData().equals("/getGoeLocationRadius")) {

            geoLocationForProduct = update.getMessage().getLocation();

            template.distanceRadiusToShop(update, userSessions.get(update.getMessage().getChatId().intValue())).getGeoListOfShops();

            userSessions.get(update.getMessage().getChatId().intValue()).setCallBackRequest(null);

        }


        if (update.getCallbackQuery() != null && update.getCallbackQuery().getData().split(" ")[0].equals("/priorityOrderList")) {

            Product product = productRepository.findFirstById(Integer.valueOf(update.getCallbackQuery().getData().split(" ")[1]));
            Company company = companyRepository.findAllByName(product.getCompany());


            UsersBot usersBot = usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue());

            UserListProducts userListProducts = userListProductsRepository.findByUsersBotAndProductAndCompany(usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue()), product, company);

            if (userListProducts == null) {
                userListProducts = new UserListProducts();
            }

            userListProducts.setProduct(product);
            userListProducts.setCompany(company);
            userListProducts.setUsersBot(usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue()));
            userListProducts.setDateTime(new Date());

            userListProductsRepository.save(userListProducts);


            try {
                execute(new AnswerCallbackQuery()
                        .setCallbackQueryId(update.getCallbackQuery().getId())
                        .setText(product.getName() + " добавлений в список бажань")
                        //.url("telegram.me/pengrad_test_bot?game=pengrad_test_game")
                        .setShowAlert(true)
                        .setCacheTime(3));
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }


        }


        if (update.getCallbackQuery() != null && update.getCallbackQuery().getData().equals("/userOrderList")) {

            messageAction.deleteMessage(userSessions.get(update.getCallbackQuery().getFrom().getId()));


            List<Product> listProduct = new ArrayList<>();
            List<UserListProducts> arrListOrderProduct = userListProductsRepository.findAllByUsersBot(usersBotRepository.findByIdUserBot(update.getCallbackQuery().getMessage().getChat().getId().intValue()));

            for (UserListProducts products : arrListOrderProduct) {
                listProduct.add(products.getProduct());
            }

            ArrayList<SendPhoto> arrayImages = template.listImageProductTemplate(update.getCallbackQuery().getMessage(), listProduct);
            int count = 0;

            try {

                for (SendPhoto imageProduct : arrayImages) {

                    execute(imageProduct);

                    List<InlineKeyboardButton> block1 = new ArrayList<>();

                    InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    block1.add(new InlineKeyboardButton().setText("Знайти найближчий магазин").setCallbackData("/findNearByShopByName " + listProduct.get(count).getCompany()));

                    rowsInline.add(block1);

                    markupInlineBackMenu.setKeyboard(rowsInline);


                    SendMessage backMenu = new SendMessage();
                    backMenu.setChatId(update.getCallbackQuery().getMessage().getChatId().toString());

                    backMenu.setText("Виберіть магазин: ");

                    backMenu.setReplyMarkup(markupInlineBackMenu);


                    Message message = execute(backMenu);

                    userSessions.get(update.getCallbackQuery().getFrom().getId()).oldReguestMessanger.add(message);

                    count++;

                }


            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }


    }


    @Override
    public String getBotUsername() {
        return "Product_List_Bot";
    }

    @Override
    public String getBotToken() {
        return "610266631:AAG_IrlVUDJyMtKg9K0Ra5XPXt4BU-0lVec";
    }


}