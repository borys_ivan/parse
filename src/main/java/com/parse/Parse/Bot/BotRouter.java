package com.parse.Parse.Bot;

import com.parse.Parse.entity.Product;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.repository.ProductRepository;
import com.parse.Parse.service.ProductService;
import com.parse.Parse.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class BotRouter {

    private final ProductServiceImpl productTest;
    private final ProductRepository productRepository;

    @Autowired
    public BotRouter(ProductServiceImpl productTest, @Qualifier("productRepository") ProductRepository productRepository) {
        this.productTest = productTest;
        this.productRepository = productRepository;
    }


    public List<Product> create(Class<BotController> objectClass, String nameMethodAnnotation, String requestValue, String company, int pageNumber, int listCount,int idUser)  {

        //no paramater
        Class noparams[] = {};

        //String parameter
        //Class[] paramString = new Class[2];
        Class[] paramString = new Class[5];
        paramString[0] = String.class;
        paramString[1] = String.class;

        paramString[2] = Integer.class;
        paramString[3] = Integer.class;
        paramString[4] = Integer.class;


        Method[] objectMethod = objectClass.getMethods();
        Annotation annotation;
        BotRequestMapping valueAnnotation;

        List<Product> request=new ArrayList<>();

        for (Method method : objectMethod) {

            annotation=method.getAnnotation(BotRequestMapping.class);
            valueAnnotation=method.getAnnotation(BotRequestMapping.class);

            if (annotation != null && valueAnnotation.value()[0].equals(nameMethodAnnotation)) {

                BotController clazz = new BotController(productTest,productRepository);
                Class<?> c = clazz.getClass();

                //System.out.println("Method "+method.getName()+"|"+company+"|"+requestValue+"|"+pageNumber+"|"+listCount);

                Method m = null;
                try {
                    m = c.getDeclaredMethod(method.getName(),paramString);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }

                System.out.println(company+"|"+requestValue+"|"+pageNumber+"|"+listCount+"|"+idUser);
                //Object product = m.invoke(clazz,new Object[] {company,requestValue});
                Object product = null;
                try {
                    product = m.invoke(clazz,new Object[] {company,requestValue,pageNumber,listCount,idUser});
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                request = (List<Product>) product;

            }

        }

        return request;
    }

}
