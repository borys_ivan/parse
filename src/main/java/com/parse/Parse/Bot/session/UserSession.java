package com.parse.Parse.Bot.session;

import com.parse.Parse.entity.GeoListOfShops;
import com.parse.Parse.entity.Shops;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class UserSession{

    private int IdUser;
    private User user;
    private Message reguest;
    private String company;
    private CallbackQuery callBackRequest;
    private Update update;

    private Boolean thisMessage;
    private Boolean thisCallBackMessage;

    private int currentPage;
    private Integer totalCountPage=0;
    public ArrayList<Message> oldReguestMessanger=new ArrayList<>();

    public Message mapShopLocation;


    private GeoListOfShops geoListOfShops;


    public UserSession(int idUser, User user) {
        IdUser = idUser;
        this.user = user;
    }

    public int getIdUser() {
        return IdUser;
    }

    public void setIdUser(int idUser) {
        IdUser = idUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Message getReguest() {
        return reguest;
    }

    public void setReguest(Message reguest) {
        this.reguest = reguest;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalCountPage() {
        return totalCountPage;
    }

    public void setTotalCountPage(Integer totalCountPage) {
        this.totalCountPage = totalCountPage;
    }

    public CallbackQuery getCallBackRequest() {
        return callBackRequest;
    }

    public void setCallBackRequest(CallbackQuery callBackRequest) {
        this.callBackRequest = callBackRequest;
    }

    public Boolean getThisMessage() {
        return thisMessage;
    }

    public void setThisMessage(Boolean thisMessage) {
        this.thisMessage = thisMessage;
    }

    public Boolean getThisCallBackMessage() {
        return thisCallBackMessage;
    }

    public void setThisCallBackMessage(Boolean thisCallBackMessage) {
        this.thisCallBackMessage = thisCallBackMessage;
    }

    public Message getMapShopLocation() {
        return mapShopLocation;
    }

    public void setMapShopLocation(Message mapShopLocation) {
        this.mapShopLocation = mapShopLocation;
    }

    public GeoListOfShops getGeoListOfShops() {
        return geoListOfShops;
    }

    public void setGeoListOfShops(GeoListOfShops geoListOfShops) {
        this.geoListOfShops = geoListOfShops;
    }

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }

    public ArrayList<Message> getOldReguestMessanger() {
        return oldReguestMessanger;
    }

    public void setOldReguestMessanger(ArrayList<Message> oldReguestMessanger) {
        this.oldReguestMessanger = oldReguestMessanger;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "IdUser=" + IdUser +
                ", user=" + user +
                ", reguest=" + reguest +
                ", company='" + company + '\'' +
                ", callBackRequest=" + callBackRequest +
                ", thisMessage=" + thisMessage +
                ", thisCallBackMessage=" + thisCallBackMessage +
                ", currentPage=" + currentPage +
                ", totalCountPage=" + totalCountPage +
                ", oldReguestMessanger=" + oldReguestMessanger +
                '}';
    }
}
