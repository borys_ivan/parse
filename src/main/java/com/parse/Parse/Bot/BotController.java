package com.parse.Parse.Bot;

import com.parse.Parse.controller.ParseController;
import com.parse.Parse.entity.AjaxResponseBody;
import com.parse.Parse.entity.PaginateRequest;
import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.SearchCriteria;
import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.ProductRepository;
import com.parse.Parse.service.ProductService;
import com.parse.Parse.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.telegram.telegrambots.meta.api.objects.User;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


@BotRequestController
public class BotController {

    private final ProductServiceImpl productTest;
    private final ProductRepository productRepository;

    @Autowired
    public BotController(ProductServiceImpl productTest, @Qualifier("productRepository") ProductRepository productRepository) {
        this.productTest = productTest;
        this.productRepository = productRepository;
    }

    public ProductRepository getBar() { return this.productRepository; }


    @BotRequestMapping("/find")
    public List<Product> findProductByNameAndCompany(String company, String word,Integer pageNumber,Integer listCount,Integer idUder){

        List<Product> totalList;

        if(pageNumber==0 && listCount==0 && !company.equals("All")){

            totalList = productTest.findAllByNameContainingIgnoreCaseAndCompanyAndVisible(word, company, true, Sort.by("price").descending());

            return totalList;

        }

        if(company.equals("All")){
            //System.out.println(productTest.findAllByNameContainingIgnoreCaseAndVisible(word,true, Sort.by("price")));
            totalList = productTest.findAllByNameContainingIgnoreCaseAndVisible(word,true, Sort.by("price").descending());
        }else {
            totalList = productTest.findAllByNameContainingIgnoreCaseAndCompanyAndVisible(word, company, true, Sort.by("price").descending());
        }

        //totalList.removeIf(product -> product.getPrice() == 1);
        for (Iterator<Product> iterator = totalList.iterator(); iterator.hasNext(); ) {
            Product product = iterator.next();
            if (product.getPrice()==1) {
                iterator.remove();
            }

        }


        /*for(Product product:totalList) {
            try {
                if (ImageIO.read(new URL(product.getImage())) != null)
                System.out.println("true "+product.getName() + "|" + product.getCompany() + "|" + product.getImage());
                    totalListWithImage.add(product);

            } catch (IOException e) {
                //System.out.println("flase " + line.getName() + "|" + line.getCompany() + "|" + line.getImage());
                System.out.println("Not have image from server "+product.getImage());
            }
        }*/



        if (totalList.isEmpty()) {
            System.out.println("NULL");
            return null;
        }

        int totalCount = totalList.size();
        System.out.println("SIZE TOTAL "+totalCount);
        //int totalCount = totalListWithImage.size();

        int endPageCount;
        int startPageCount= pageNumber*listCount;
        int currentList = (startPageCount+listCount)-totalCount;
        System.out.println("CURRENT PAGE "+currentList);
        if(currentList>0){

            endPageCount  = startPageCount+listCount-currentList;

        } else {

            endPageCount = startPageCount+listCount;

        }
        System.out.println("END PAGE "+endPageCount);

System.out.println("USER USER USER "+ProductBot.userSessions.get(idUder));
        //System.out.println("CountL "+totalListWithImage.size());
        ProductBot.userSessions.get(idUder).setTotalCountPage((int) Math.ceil((float) totalList.size()/listCount));
        //ProductBot.userSessions.get(idUder).setTotalCountPage((int) Math.ceil((float) totalListWithImage.size()/listCount));

        return totalList.subList(startPageCount,endPageCount);
        //return totalListWithImage.subList(startPageCount,endPageCount);
    }

    @BotRequestMapping("/test22")
    public String test2(){
        return "TEST!!!!";
    }


}
