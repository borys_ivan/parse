package com.parse.Parse.Bot;

import org.jboss.logging.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;


import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TelegramUpdateHandlerBeanPostProcessor implements BeanPostProcessor, Ordered {

    private Map<String, Class> botReguestControllerMap = new HashMap<>();
    private static final Logger LOGGER = Logger.getLogger(TelegramUpdateHandlerBeanPostProcessor.class);

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(BotRequestController.class))
            botReguestControllerMap.put(beanName, beanClass);
        System.out.println(beanClass);
        System.out.println(bean);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(!botReguestControllerMap.containsKey(beanName)) return bean;

        Object original = botReguestControllerMap.get(beanName);
       /* Arrays.stream(original.getClass().getMethods())
                .filter(method -> method.isAnnotationPresent(BotRequestMapping.class))
                .forEach((Method method) -> generateController(bean, method));*/
        return bean;
    }
}
