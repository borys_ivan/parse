package com.parse.Parse.Bot.message;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.Bot.session.UserSession;
import com.parse.Parse.Bot.template.Template;
import com.parse.Parse.Bot.thead.DowloadMenuThread;
import com.parse.Parse.Bot.thead.ThreadProcessBot;
import com.parse.Parse.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.List;

import static java.awt.image.BufferedImage.TYPE_3BYTE_BGR;
import static java.awt.image.BufferedImage.TYPE_CUSTOM;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

@Component
public class MessageAction {

    @Autowired
    private ApplicationContext context;

    @Autowired
    public Template template;

    @Autowired
    private DowloadMenuThread dowloadMenuThread;


    public void deleteMessage(UserSession userSession){

        synchronized (this) {

            if (userSession.oldReguestMessanger.size()!=0) {

                for (org.telegram.telegrambots.meta.api.objects.Message message : userSession.oldReguestMessanger) {

                    DeleteMessage deleteMessage = new DeleteMessage(message.getChatId(), message.getMessageId());

                    ProductBot clean = (ProductBot)context.getBean("productBot");

                    try {
                        clean.execute(deleteMessage);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                }

                userSession.oldReguestMessanger.clear();

            }

        }

    }


    public void sendMsg(Long chatID, Integer messagaID, InlineKeyboardMarkup inlineKeyboardMarkup, Map<Integer, UserSession> userSessions) {

        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(chatID);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        sendMessage.setText("Виберіть магазин");

        try {

            ProductBot send = (ProductBot)context.getBean("productBot");

            Message selectMenu = send.execute(sendMessage);

            userSessions.get(messagaID).oldReguestMessanger.add(selectMenu);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    public void sendMsg(Message message, List<Product> text,Map<Integer, UserSession> userSessions) {
        /*SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());*/
        //sendMessage.setReplyToMessageId(message.getMessageId());

        boolean lock=false;

        //userSessions.get(message.getFrom().getId()).

       //////deleteMessage(userSessions.get(message.getFrom().getId()));
        deleteMessage(userSessions.get(message.getChat().getId().intValue()));

        if(text==null){

            template.notFoundProduct(message,userSessions);

            lock=false;

        }else{


            for (Product line:text) {


                //DowloadMenuThread test = new DowloadMenuThread();
                dowloadMenuThread.setChatID(message.getChat().getId());
                dowloadMenuThread.setDownload(true);

                //dowloadMenuThread.run();
                Thread ttt = new Thread(dowloadMenuThread);



                ttt.start();



               // (new Thread(new DowloadMenuThread())).start();
              //  dowloadMenuThread.setChatID(message.getChat().getId());
              //  dowloadMenuThread.setDownload(true);

                SendPhoto sendPhoto = new SendPhoto();

                sendPhoto.setChatId(message.getChatId());


                //DowloadMenuThread test=new DowloadMenuThread(message.getChat().getId());
                //Thread t1 =new Thread(test);
                //t1.start();

                try {
                   // dowloadMenuThread.setDownload(true);

                    URL url = new URL(line.getImage());
                System.out.println("Dowload Dowload Dowload Dowload Dowload");

                BufferedImage image = null;

                image = ImageIO.read(url);

                    String mimeType = URLConnection.guessContentTypeFromName(url.getFile());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();


                    Iterator<ImageWriter> writers=null;

                    System.out.println(mimeType);

                 if (mimeType.equals("image/jpg")) {

                     writers = ImageIO.getImageWritersByFormatName("jpg");

                 } else {

                     writers = ImageIO.getImageWritersByFormatName("png");

                    }

                ImageWriter writer = (ImageWriter) writers.next();

                ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
                writer.setOutput(ios);

                ImageWriteParam param = writer.getDefaultWriteParam();

                    if(param.canWriteCompressed()) {
                        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                        param.setCompressionQuality(0.1f);  // Change the quality value you prefer
                    }

                IIOImage outputImage = new IIOImage(image, null, null);

                writer.write(null, outputImage, param);

                    InputStream targetStream = new ByteArrayInputStream(baos.toByteArray());
                    System.out.println("SIZE SIZE SIZE SIZE SIZE "+baos.size());

                    sendPhoto.setPhoto(String.valueOf(line.getId()),targetStream);


                ios.close();
                writer.dispose();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                dowloadMenuThread.setDownload(false);
                ttt.stop();
                dowloadMenuThread.delete();


                if(userSessions.get(message.getChat().getId().intValue()).getCompany().equals("/find All"))
                    sendPhoto.setCaption(line.getName() + "\n нова ціна: " + line.getPrice() + "\n стара ціна: " + line.getOldPrice()+ "\nмагазин: " + line.getCompany());
                else
                    sendPhoto.setCaption(line.getName() + "\n нова ціна: " + line.getPrice() + "\n стара ціна: " + line.getOldPrice());

                try {
                    System.out.println(sendPhoto);

                    ProductBot send = (ProductBot)context.getBean("productBot");

                    Message bot = send.execute(sendPhoto);



                    System.out.println("getDownload getDownload getDownload "+dowloadMenuThread.getDownload());


                    //userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(bot);
                    userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(bot);

                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }

                lock=true;
            }


        }


        if (!message.getText().split("\\s",2)[0].equals("/start") && lock) {

            template.addMenuProduct(message,userSessions);

        }

    }

}
