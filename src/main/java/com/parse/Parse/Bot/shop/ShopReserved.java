package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service("ShopReserved")
public class ShopReserved extends AbstractParser implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    @Override
    public void parse() {

        LOGGER.info("start parsing Reserved shops");

        try {

            JsonArray arrayShops;
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://www.reserved.com/ua/ru/ajx/stores/get/");

            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("all_stores", new StringBody("0"));

            httppost.setEntity(reqEntity);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);


            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();


            arrayShops = jsonObject.getAsJsonObject("content").getAsJsonArray("stores");


            for (JsonElement shop : arrayShops) {

                /* System.out.println(shop.getAsJsonObject().get("name").toString().replaceAll("\"", ""));*/

                Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("street").toString().replaceAll("\"", ""));

                if (shops != null) {

                    shops.setAddress(shop.getAsJsonObject().get("street").toString().replaceAll("\"", ""));
                    shops.setLongitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lng").toString().replaceAll("\"", "")));
                    shops.setLatitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lat").toString().replaceAll("\"", "")));

                    shops.setIdOfCompany(8);

                    shopsRepository.save(shops);

                } else {

                    Shops shopsReserved = new Shops();

                    shopsReserved.setAddress(shop.getAsJsonObject().get("street").toString().replaceAll("\"", ""));
                    shopsReserved.setLongitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lng").toString().replaceAll("\"", "")));
                    shopsReserved.setLatitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lat").toString().replaceAll("\"", "")));

                    shopsReserved.setIdOfCompany(8);

                    shopsRepository.save(shopsReserved);

                }

            }

            LOGGER.info("Reserved shops parsed");


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
