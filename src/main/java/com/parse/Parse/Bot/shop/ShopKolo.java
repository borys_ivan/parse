package com.parse.Parse.Bot.shop;

import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.*;

@Service("ShopKolo")
public class ShopKolo extends AbstractParser implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 9;

    @Autowired
    @Qualifier("AddressGeoApi")
    private AddressGeoApi addressGeoApi;

    @Override
    public void parse() {

        LOGGER.info("start parsing Kolo shops");

        List<Document> page = new ArrayList<Document>();
        String Url = "https://kolomarket.com.ua/about/shops-addresses/";

        Element shops = null;
        try {
            shops = connectSetting(Url).get().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements allShop = shops.select("div.cols-container div.col ul.addresses-list").get(0).select("li.address");

        ArrayList<String> shopsAddress = new ArrayList<>();

        for (Element shop : allShop) {
            shopsAddress.add(shop.text().replaceAll("(.*?)(\\s\\(.*?\\))", "$1"));
        }

        addressGeoApi.request(shopsAddress,ID_COMPANY);

        LOGGER.info("Kolos shops parsed");

    }


}
