package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service("ShopFurshet")
public class ShopFurshet implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    @Override
    public void parse() {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        LOGGER.info("start parsing Furshet shops");


        try {

            HttpUriRequest request = RequestBuilder.get()
                    .setUri("https://furshet.ua/wcity")
                    .addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .addHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36")
                    .addHeader(HttpHeaders.REFERER, "https://furshet.ua/")
                    .addHeader("accept-language", "en-US,en;q=0.9")
                    .addHeader("x-requested-with", "XMLHttpRequest")
                    .addParameter("cid", "10")
                    .build();

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(request, responseHandler);

            JsonArray jsonArray = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonArray();

            for (JsonElement shop : jsonArray) {

               String fixStreet = shop.getAsJsonObject().get("name").toString().replaceAll("(.*?бул\\..*)(,\\s[\\d]+)","$1 бульвар$2");

                String [] array = fixStreet.replaceAll("\"", "").split("\\.\\s|,\\s");
                String strRequestApi = array[array.length - 1]+", "+array[array.length - 2].replaceAll("бул. |вул. |пр-т |«Гурман-Фуршет», |Метрополіта А.", "")+", Kyiv, Київ, Ukraine";

                HttpUriRequest requestMapApi  = RequestBuilder.get()
                        .setUri("https://api.opencagedata.com/geocode/v1/json")
                        .addParameter("q", strRequestApi)
                        .addParameter("key", "641c51bed8ab490184632ad8526e29ad")
                        .addParameter("language", "ua")
                        .addParameter("no_annotations", "1")
                        .build();

                ResponseHandler<String> responseHandlerApi = new BasicResponseHandler();
                String responseBodyApi = httpClient.execute(requestMapApi, responseHandlerApi);

                JsonObject jsonArrayApi = new JsonParser().parse(StringEscapeUtils.
                        unescapeJava(responseBodyApi)).getAsJsonObject();

                JsonElement geometry = jsonArrayApi.get("results").getAsJsonArray().get(0).getAsJsonObject().get("geometry");


                System.out.println(geometry.getAsJsonObject().get("lat"));
                System.out.println(geometry.getAsJsonObject().get("lng"));

                Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("name").toString().replaceAll("\"", ""));

                if (shops != null) {

                    shops.setAddress(shop.getAsJsonObject().get("name").toString().replaceAll("\"", ""));
                    shops.setLatitude(Double.valueOf(geometry.getAsJsonObject().get("lat").toString()));
                    shops.setLongitude(Double.valueOf(geometry.getAsJsonObject().get("lng").toString()));


                    if (shop.getAsJsonObject().get("tid") != null)
                        shops.setNumber(Integer.valueOf(shop.getAsJsonObject().get("tid").toString().replaceAll("\"", "")));


                    shops.setIdOfCompany(2);

                    shopsRepository.save(shops);

                }else {

                    Shops shopsFurshet = new Shops();

                    shopsFurshet.setAddress(shop.getAsJsonObject().get("name").toString().replaceAll("\"", ""));
                    shopsFurshet.setLatitude(Double.valueOf(geometry.getAsJsonObject().get("lat").toString()));
                    shopsFurshet.setLongitude(Double.valueOf(geometry.getAsJsonObject().get("lng").toString()));


                    if (shop.getAsJsonObject().get("tid") != null)
                        shopsFurshet.setNumber(Integer.valueOf(shop.getAsJsonObject().get("tid").toString().replaceAll("\"", "")));


                    shopsFurshet.setIdOfCompany(2);

                    shopsRepository.save(shopsFurshet);

                }

                        Thread.sleep(8000);


            }

            LOGGER.info("Furshet shops parsed");

        } catch(ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
