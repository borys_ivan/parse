package com.parse.Parse.Bot.shop;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.net.URISyntaxException;

@Service("ShopMetro")
public class ShopMetro implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 17;

    @Override
    public void parse() {

        LOGGER.info("start parsing Metro shops");

        CloseableHttpClient httpClient = HttpClients.createDefault();

        URIBuilder builder = null;
        try {

            builder = new URIBuilder("https://www.metro.ua/services/StoreLocator/StoreLocator.ashx");
            builder.setParameter("id", "{9BBBF4AA-50EF-40FF-9C93-961B9F96A0ED}");
            builder.setParameter("lat", "50.4501");
            builder.setParameter("lng", "30.5234");
            builder.setParameter("languag", "uk-UA");
            builder.setParameter("distance", "50000000");
            builder.setParameter("limit", "100");


            HttpGet httpGET = new HttpGet(builder.build());
            //httpGET.addHeader("Accept","*/*");
            /*httpGET.addHeader("Accept-Encoding:","gzip, deflate, br");
            httpGET.addHeader("Connection","keep-alive");
            httpGET.addHeader("Host","www.metro.ua");
            httpGET.addHeader("Referer","https://www.metro.ua/stores");
            httpGET.addHeader("Sec-Fetch-Dest","empty");
            httpGET.addHeader("Sec-Fetch-Mode","cors");
            httpGET.addHeader("Sec-Fetch-Site","same-origin");
            httpGET.addHeader("User-Agent","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36");
            httpGET.addHeader("X-Requested-With","XMLHttpRequest");
            httpGET.addHeader("Cookie","website_ua-metro#lang=uk-UA; ASP.NET_SessionId=zbb2tygl2rydunpuozdl2x3u; SRVNAME=DTC_3; BIGipServerwww-p-mc.metro-group.com-80=!LA0Ok++GtiXfyHeMmcB0XOhPdskaM1MVDdJGqzWe4EQ++TJ8eP0GpOneoU8OILKaRO6EVpEpie5szg==; _ga=GA1.2.409892796.1593038340; _fbp=fb.1.1593038340733.632193957; SC_ANALYTICS_GLOBAL_COOKIE=18bcb6cbd69f4caeb82b9a0f1717d84a|True; UserSettings=SelectedStore={A64B7300-8C1D-4C25-9D07-07510A53267F}; _gid=GA1.2.1200551810.1593199883; _gat_UA-64696032-3=1; _gat_UA-118296439-49=1; _gat=1");
*/

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(httpGET, responseHandler);

            JsonElement jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject().get("stores");


            String street = "";
            String lat = "";
            String lon = "";
            String schedule = "";


            for (JsonElement shop : jsonObject.getAsJsonArray()) {


                street = shop.getAsJsonObject().get("street").toString().replaceAll("\"", "") + " " +
                        shop.getAsJsonObject().get("hnumber").toString().replaceAll("\"", "");
                lat = shop.getAsJsonObject().get("lat").toString().replaceAll("\"", "");
                lon = shop.getAsJsonObject().get("lon").toString().replaceAll("\"", "");
                schedule = shop.getAsJsonObject().get("openinghours").getAsJsonObject().get("all").
                        getAsJsonArray().get(0).getAsJsonObject().get("title").toString().replaceAll("\"", "") + " "
                        + shop.getAsJsonObject().get("openinghours").getAsJsonObject().get("all").getAsJsonArray().get(0).getAsJsonObject().get("hours").toString().replaceAll("\"", "");

                if (shop.getAsJsonObject().get("city").toString().equals("\"Київ\"")) {

                    Shops shops = shopsRepository.findByAddress(street);

                    if (shops != null) {

                        System.out.println("This address is in BD");

                        if (!shops.getIdOfCompany().equals(ID_COMPANY)) {

                            Shops shopsMetro = new Shops();

                            shopsMetro.setAddress(street);
                            shopsMetro.setLatitude(Double.valueOf(lat));
                            shopsMetro.setLongitude(Double.valueOf(lon));
                            shopsMetro.setSchedule(schedule);
                            shopsMetro.setIdOfCompany(ID_COMPANY);

                            shopsRepository.save(shopsMetro);

                            System.out.println("Save new company shop in this Address");

                        }

                    } else {

                        Shops shopsMetro = new Shops();

                        shopsMetro.setAddress(street);
                        shopsMetro.setLatitude(Double.valueOf(lat));
                        shopsMetro.setLongitude(Double.valueOf(lon));
                        shopsMetro.setSchedule(schedule);
                        shopsMetro.setIdOfCompany(ID_COMPANY);

                        shopsRepository.save(shopsMetro);

                        System.out.println("Save shop in BD");

                    }

                }

            }

            LOGGER.info("Metro shops parsed");

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }

    }

}
