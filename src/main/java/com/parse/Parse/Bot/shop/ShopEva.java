package com.parse.Parse.Bot.shop;

import com.google.gson.*;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service("ShopEva")
public class ShopEva implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 15;

    @Override
    public void parse() {

        LOGGER.info("start parsing Eva shops");

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpUriRequest request = RequestBuilder.get()
                .setUri("https://eva.ua/ua/stockists/ajax/stores")
                .addHeader("accept", "application/json, text/javascript, */*; q=0.01")
                .addHeader(":authority", "eva.ua")
                .addHeader(":method", "GET")
                .addHeader(":scheme", "https")
                .addHeader(":path", "/ua/stockists/ajax/stores?state=%D0%9A%D0%B8%D1%97%D0%B2&city=%D0%9A%D0%B8%D1%97%D0%B2")
                .addHeader("accept-language", "en-US,en;q=0.9")
                .addHeader("sec-Fetch-Mode", "cors")
                .addHeader("sec-Fetch-Site", "same-origin")
                .addHeader("user-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")
                .addHeader("referer", "https://eva.ua/ua/stockists/")
                .addParameter("state","Київ")
                .addParameter("city","Київ")
                .build();

        try{

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(request, responseHandler);

            JsonArray arrayJson = new JsonParser().parse(responseBody).getAsJsonArray();

           for (JsonElement shop:arrayJson){

               Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("address").toString().replaceAll("\"", ""));

               if (shops != null) {

                   System.out.println("This address is in DB");

                   if(!shops.getIdOfCompany().equals(ID_COMPANY)) {

                       Shops shopsEva = new Shops();

                       shopsEva.setAddress(shop.getAsJsonObject().get("address").toString().replaceAll("\"", ""));
                       shopsEva.setLatitude(Double.valueOf(shop.getAsJsonObject().get("latitude").toString().replaceAll("\"", "")));
                       shopsEva.setLongitude(Double.valueOf(shop.getAsJsonObject().get("longitude").toString().replaceAll("\"", "")));

                       if (shop.getAsJsonObject().get("schedule1").toString() != null)
                           shopsEva.setSchedule(shop.getAsJsonObject().get("schedule1").toString().replaceAll("\"", ""));

                       shopsEva.setIdOfCompany(ID_COMPANY);

                       shopsRepository.save(shopsEva);

                       System.out.println("Save new company shop in this Address");

                   }

               } else {

                   Shops shopsEva = new Shops();

                   shopsEva.setAddress(shop.getAsJsonObject().get("address").toString().replaceAll("\"", ""));
                   shopsEva.setLatitude(Double.valueOf(shop.getAsJsonObject().get("latitude").toString().replaceAll("\"", "")));
                   shopsEva.setLongitude(Double.valueOf(shop.getAsJsonObject().get("longitude").toString().replaceAll("\"", "")));


                   if (shop.getAsJsonObject().get("schedule1").toString() != null)
                       shopsEva.setSchedule(shop.getAsJsonObject().get("schedule1").toString().replaceAll("\"", ""));

                   shopsEva.setIdOfCompany(ID_COMPANY);

                   shopsRepository.save(shopsEva);

                   System.out.println("Save shop in DB");

               }

           }

            LOGGER.info("Eva shops parsed");

        }catch (IOException e) {


        } finally {
            httpClient.getConnectionManager().shutdown();
        }

    }

}


