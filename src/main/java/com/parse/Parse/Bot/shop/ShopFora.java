package com.parse.Parse.Bot.shop;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

@Service("ShopFora")
public class ShopFora implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 11;

    @Override
    public void parse() {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://fora.ua/vasha-nayblyzhcha-fora/");

        LOGGER.info("start parsing Fora shops");

        try {
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("ajax", new StringBody("1"));
            reqEntity.addPart("action", new StringBody("getStoregesByCity"));
            reqEntity.addPart("city", new StringBody("6"));

            httppost.setEntity(reqEntity);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);

            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();


            Set<Map.Entry<String, JsonElement>> arrayShops = jsonObject.get("storages").getAsJsonObject().entrySet();

            for (Map.Entry shop : arrayShops) {

                JsonObject GEO = (JsonObject) shop.getValue();

                Shops shops = shopsRepository.findByAddress(GEO.get("title").toString().replaceAll("\"", ""));

                if (shops != null) {

                    System.out.println("This address is in BD");

                    if(!shops.getIdOfCompany().equals(ID_COMPANY)) {

                        Shops shopsFora = new Shops();

                        shopsFora.setAddress(GEO.get("title").toString().replaceAll("\"", ""));
                        String[] coordinate = GEO.get("coordinates").toString().replaceAll("\"", "").split(",\\s?");
                        shopsFora.setLatitude(Double.valueOf(coordinate[0]));
                        shopsFora.setLongitude(Double.valueOf(coordinate[1]));

                        shopsFora.setNumber(Integer.valueOf(shop.getKey().toString()));

                        if (GEO.get("worktime").toString() != null)
                            shopsFora.setSchedule(GEO.get("worktime").toString().replaceAll("\"", ""));

                        shopsFora.setIdOfCompany(ID_COMPANY);

                        shopsRepository.save(shopsFora);

                        System.out.println("Save new company shop in this Address");

                    }

                } else {

                    Shops shopsFora = new Shops();

                    shopsFora.setAddress(GEO.get("title").toString().replaceAll("\"", ""));
                    String[] coordinate = GEO.get("coordinates").toString().replaceAll("\"", "").split(",\\s?");
                    shopsFora.setLatitude(Double.valueOf(coordinate[0]));
                    shopsFora.setLongitude(Double.valueOf(coordinate[1]));

                    shopsFora.setNumber(Integer.valueOf(shop.getKey().toString()));

                    if (GEO.get("worktime").toString() != null)
                        shopsFora.setSchedule(GEO.get("worktime").toString().replaceAll("\"", ""));

                    shopsFora.setIdOfCompany(ID_COMPANY);

                    shopsRepository.save(shopsFora);

                    System.out.println("Save shop in BD");

                }

            }

            LOGGER.info("Fora shops parsed");

        } catch (IOException e) {


        } finally {
            httpclient.getConnectionManager().shutdown();
        }

    }
}
