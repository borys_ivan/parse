package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

@Service("ShopAuchan")
public class ShopAuchan implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    @Override
    public void parse() {

        JsonObject arrayProductAuchan;
        CloseableHttpClient httpClient = HttpClients.createDefault();

        LOGGER.info("start parsing Auchan shops");

        try {


            HttpClient client = HttpClients.custom().build();
            HttpUriRequest request = RequestBuilder.get()
                    .setUri("https://yep.auchan.com/corp/cms/v3/ua/template/stores")
                    .addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .addHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36")
                    .addHeader(HttpHeaders.REFERER, "https://brand.auchan.ua/ru/map")
                    .addHeader("X-Gravitee-Api-Key", "5951f1cd-62ad-44c2-82ab-8470bcbff6b5")
                    .addParameter("lang", "ua")
                    .build();

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(request, responseHandler);


            JsonArray jsonArray = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonArray();

            for (JsonElement shop : jsonArray) {

                if (shop.getAsJsonObject().get("city").toString().replaceAll("\"|\\,", "").equals("Київ")) {

                    Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("street_address").toString().replaceAll("\"", ""));

                    if (shops != null) {

                        shops.setAddress(shop.getAsJsonObject().get("street_address").toString().replaceAll("\"", ""));
                        shops.setLatitude(Double.valueOf(shop.getAsJsonObject().get("latitude").toString().replaceAll("\"", "")));
                        shops.setLongitude(Double.valueOf(shop.getAsJsonObject().get("longitude").toString().replaceAll("\"", "")));


                        if (shop.getAsJsonObject().get("store_id") != null)
                            shops.setNumber(Integer.valueOf(shop.getAsJsonObject().get("id").toString().replaceAll("\"", "")));


                        shops.setIdOfCompany(4);

                        shopsRepository.save(shops);

                    } else {

                        Shops shopsAuchan = new Shops();

                        shopsAuchan.setAddress(shop.getAsJsonObject().get("street_address").toString().replaceAll("\"", ""));
                        shopsAuchan.setLatitude(Double.valueOf(shop.getAsJsonObject().get("latitude").toString().replaceAll("\"", "")));
                        shopsAuchan.setLongitude(Double.valueOf(shop.getAsJsonObject().get("longitude").toString().replaceAll("\"", "")));


                        if (shop.getAsJsonObject().get("store_id") != null)
                            shopsAuchan.setNumber(Integer.valueOf(shop.getAsJsonObject().get("store_id").toString().replaceAll("\"", "")));


                        shopsAuchan.setIdOfCompany(4);

                        shopsRepository.save(shopsAuchan);

                    }

                }

            }

            LOGGER.info("Auchan shops parsed");

            } catch(ClientProtocolException e){
                e.printStackTrace();
            } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
