package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Service("ShopSilpo")
public class ShopSilpo implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Override
    public void parse() {

        JsonArray arrayShops;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://silpo.ua/graphql");

        LOGGER.info("start parsing Silpo shops");

        try {
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("operationName", new StringBody("stores"));
            reqEntity.addPart("variables", new StringBody("{\"filter\":{\"storeId\":null,\"cityId\":\"79\",\"start\":null,\"end\":null,\"hasCertificate\":null,\"servicesIds\":null},\"pagingInfo\":{\"limit\":0,\"offset\":0},\"ids\":[\"OPENED\"]}"));

            InputStream inputStream = getClass()
                    .getClassLoader().getResourceAsStream("config/queryShopSilpo.txt");
            String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);


            reqEntity.addPart("query",new StringBody(result));

            httppost.setEntity(reqEntity);
            //System.out.println("Requesting : " + httppost.getRequestLine());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);


            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();


            arrayShops = jsonObject.getAsJsonObject("data").getAsJsonObject("stores").
                    getAsJsonArray("items");


            for (JsonElement shop:arrayShops){

                Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("title").toString().replaceAll("\"", ""));

                if (shops!=null){

                    shops.setAddress(shop.getAsJsonObject().get("title").toString().replaceAll("\"", ""));
                    shops.setLatitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lat").toString().replaceAll("\"", "")));
                    shops.setLongitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lng").toString().replaceAll("\"", "")));


                    if (shop.getAsJsonObject().get("id")!= null)
                        shops.setNumber(Integer.valueOf(shop.getAsJsonObject().get("id").toString().replaceAll("\"", "")));

                    if (shop.getAsJsonObject().get("workingHours") != null)
                        shops.setSchedule(shop.getAsJsonObject().get("workingHours").getAsJsonObject().get("start").toString().replaceAll("\"", "")+" - "+shop.getAsJsonObject().get("workingHours").getAsJsonObject().get("end").toString().replaceAll("\"", ""));


                    shops.setIdOfCompany(3);

                    shopsRepository.save(shops);

                }else {

                    Shops shopsSilpo = new Shops();

                    shopsSilpo.setAddress(shop.getAsJsonObject().get("title").toString().replaceAll("\"", ""));
                    shopsSilpo.setLatitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lat").toString().replaceAll("\"", "")));
                    shopsSilpo.setLongitude(Double.valueOf(shop.getAsJsonObject().get("location").getAsJsonObject().get("lng").toString().replaceAll("\"", "")));


                    if (shop.getAsJsonObject().get("id")!= null)
                        shopsSilpo.setNumber(Integer.valueOf(shop.getAsJsonObject().get("id").toString().replaceAll("\"", "")));

                    if (shop.getAsJsonObject().get("workingHours") != null)
                        shopsSilpo.setSchedule(shop.getAsJsonObject().get("workingHours").getAsJsonObject().get("start").toString().replaceAll("\"", "")+" - "+shop.getAsJsonObject().get("workingHours").getAsJsonObject().get("end").toString().replaceAll("\"", ""));


                    shopsSilpo.setIdOfCompany(3);

                    shopsRepository.save(shopsSilpo);

                }


            }

            LOGGER.info("Silpo shops parsed");

        } catch (IOException e) {


        } finally {
            httpclient.getConnectionManager().shutdown();
        }


    }
}
