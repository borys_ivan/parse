package com.parse.Parse.Bot.shop;

import com.parse.Parse.entity.Shops;
import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.regex.Pattern;

@Service("ShopKishenya")
public class ShopKishenya extends AbstractParser implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    @Override
    public void parse() {

        try {

            LOGGER.info("start parsing Kishenya shops");

            String Url = "http://kishenya.ua/ua/kontakty/mahazyny-velyka-kyshenia.html";

            connectSetting(Url).get();
            Document docCustomConn = connectSetting(Url).get();

            Elements geometry = docCustomConn.select("div.addres.address-value > div.city-item.kyiv");

            String address = Pattern.compile(
                    "(?:<div class=\" city-item kyiv\">.*?<h2>Київ<\\/h2>.*?)?\\s?<div onclick.*?\\,\'.*?\'\\,\'.*?\'.*?<p>(.*?)<\\/p>.*?<div class=\"clear\"><\\/div>\\s*\\n\\s*<\\/div>(?:\\s?\\n<\\/div>)?\\s?",
                    Pattern.DOTALL)
                    .matcher(geometry.toString()).replaceAll("$1|");

            String lat = Pattern.compile(
                    "(?:<div class=\"\\s?city-item kyiv\">.*?<h2>Київ<\\/h2>.*?)?\\s?<div onclick.*?\\,\'(.*?)\'\\,\'.*?\'.*?<div class=\"clear\"><\\/div>\\s*\\n\\s*<\\/div>(?:\\s?\\n<\\/div>)?",
                    Pattern.DOTALL)
                    .matcher(geometry.toString()).replaceAll("$1");

            String lng = Pattern.compile(
                    "(?:<div class=\"\\s?city-item kyiv\">.*?<h2>Київ<\\/h2>.*?)?\\s?<div onclick.*?\\,\'.*?\'\\,\'(.*?)\'.*?<div class=\"clear\"><\\/div>\\s*\\n\\s*<\\/div>(?:\\s?\\n<\\/div>)?",
                    Pattern.DOTALL)
                    .matcher(geometry.toString()).replaceAll("$1");

            String[] addressShop = address.split("\\|(?:\\n)?");
            String[] latShop = lat.split("\\n");
            String[] lngShop = lng.split("\\n");

            for (int i =0;i<=addressShop.length-1;i++){

                System.out.println(addressShop[i]+" "+latShop[i]+" "+lngShop[i]);


                Shops shops = shopsRepository.findByAddress(addressShop[i]);

                if (shops!=null){

                    shops.setAddress(addressShop[i]);
                    shops.setLatitude(Double.valueOf(latShop[i]));
                    shops.setLongitude(Double.valueOf(lngShop[i]));

                    shops.setIdOfCompany(5);

                    shopsRepository.save(shops);

                }else {

                    Shops shopsKishenya = new Shops();

                    shopsKishenya.setAddress(addressShop[i]);
                    shopsKishenya.setLatitude(Double.valueOf(latShop[i]));
                    shopsKishenya.setLongitude(Double.valueOf(lngShop[i]));

                    shopsKishenya.setIdOfCompany(5);

                    shopsRepository.save(shopsKishenya);

                }

            }


            LOGGER.info("Kishenya shops parsed");

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
