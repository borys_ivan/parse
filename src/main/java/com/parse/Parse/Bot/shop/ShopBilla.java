package com.parse.Parse.Bot.shop;


import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.Set;


@Service("ShopBilla")
public class ShopBilla implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 12;

    @Override
    public void parse() {

        LOGGER.info("start parsing Billa shops");

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpUriRequest request = RequestBuilder.get()
                .setUri("https://www.billa.ua/api/stores/all")
                .addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
                .addHeader("Referer", "https://www.billa.ua/magazini")
                .addHeader("Accept-Language", "en-US,en;q=0.9")
                .addHeader("Accept-Encoding", "gzip, deflate, br")
                .addHeader("Host", "www.billa.ua")
                .addHeader("Sec-Fetch-Mode", "cors")
                .addHeader("Sec-Fetch-Site", "same-origin")
                .addHeader("lat", "50.476083")
                .addHeader("lon", "30.497538")
                .addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")
                .build();

        try {

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(request, responseHandler);

            JsonObject arrayShop = new JsonParser().parse(responseBody).getAsJsonObject();


            arrayShop.get("groupedStores").getAsJsonObject();

            Set<Map.Entry<String, JsonElement>> arrayShops = arrayShop.get("groupedStores").getAsJsonObject().entrySet();

            for (Map.Entry shop : arrayShops) {

                JsonArray innerArrShops = (JsonArray) shop.getValue();

                for (JsonElement shopInf : innerArrShops) {

                    if (shopInf.getAsJsonObject().get("city").toString().equals("\"Київ\"")) {

                        Shops shops = shopsRepository.findByAddress(shopInf.getAsJsonObject().get("street").toString().replaceAll("\"", ""));

                        if (shops != null) {

                            System.out.println("This address is in DB");

                            if(!shops.getIdOfCompany().equals(ID_COMPANY)) {

                                Shops shopsBillaInAddrs = new Shops();

                                shopsBillaInAddrs.setAddress(shopInf.getAsJsonObject().get("street").toString().replaceAll("\"", ""));
                                shopsBillaInAddrs.setLatitude(Double.valueOf(shopInf.getAsJsonObject().get("yCoordinates").toString()));
                                shopsBillaInAddrs.setLongitude(Double.valueOf(shopInf.getAsJsonObject().get("xCoordinates").toString()));

                                shopsBillaInAddrs.setNumber(Integer.valueOf(shop.getKey().toString()));

                                if (shopInf.getAsJsonObject().get("openingTimesHTMLSplit").toString() != null)
                                    shopsBillaInAddrs.setSchedule(shopInf.getAsJsonObject().get("openingTimesHTMLSplit").toString().replaceAll("\"", ""));

                                shopsBillaInAddrs.setIdOfCompany(ID_COMPANY);

                                shopsRepository.save(shopsBillaInAddrs);

                                System.out.println("Save new company shop in this Address");

                            }

                        } else {

                            Shops shopsBilla = new Shops();

                            shopsBilla.setAddress(shopInf.getAsJsonObject().get("street").toString().replaceAll("\"", ""));
                            shopsBilla.setLatitude(Double.valueOf(shopInf.getAsJsonObject().get("yCoordinates").toString()));
                            shopsBilla.setLongitude(Double.valueOf(shopInf.getAsJsonObject().get("xCoordinates").toString()));

                            shopsBilla.setNumber(Integer.valueOf(shop.getKey().toString()));

                            if (shopInf.getAsJsonObject().get("openingTimesHTMLSplit").toString() != null)
                                shopsBilla.setSchedule(shopInf.getAsJsonObject().get("openingTimesHTMLSplit").toString().replaceAll("\"", ""));

                            shopsBilla.setIdOfCompany(12);

                            shopsRepository.save(shopsBilla);

                            System.out.println("Save shop in DB");

                        }

                    }

                }

            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            httpClient.getConnectionManager().shutdown();
        }


    }
}
