package com.parse.Parse.Bot.shop;

import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service("ShopVelmart")
public class ShopVelmart extends AbstractParser implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 13;

    @Autowired
    @Qualifier("AddressGeoApi")
    private AddressGeoApi addressGeoApi;

    @Override
    public void parse() {

        LOGGER.info("start parsing Velmart shops");

        String Url = "http://velmart.ua/ru/stores";

        Element shops = null;
        try {
            shops = connectSetting(Url).get().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements allShop = shops.select("ul.mapList.first li");

        Elements shopsList = allShop.get(4).select("a");

        ArrayList<String> shopsAddress = new ArrayList<>();

        for (Element shop : shopsList) {
            shopsAddress.add(shop.attr("title").replaceAll("(.*?)(\\s\\(.*?\\))", "$1"));
        }

        addressGeoApi.request(shopsAddress,ID_COMPANY);

        LOGGER.info("Kolos shops parsed");
    }
}
