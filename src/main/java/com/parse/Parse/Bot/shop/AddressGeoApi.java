package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service("AddressGeoApi")
public class AddressGeoApi {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

     void request(ArrayList<String> shopsAddress,int ID_COMPANY){

        int count = 0;

        /*ArrayList<String> shopsAddress = new ArrayList<>();

        for (Element shop : allShop) {
            shopsAddress.add(shop.text().replaceAll("(.*?)(\\s\\(.*?\\))", "$1"));
        }*/


        JsonArray geometryAll;

        JsonElement GeoValueFinal = null;

        ArrayList<String> test = new ArrayList<>();
        //test=shopsAddress;
        //test.add(shopsAddress.get(45));

        //for (String shop : test) {
        for (String shop : shopsAddress) {

            geometryAll = makeRequestApi(shop, "([\\d\\/]+)\\-(.*?\\,.*)", false);

            System.out.println(shop);

            for (JsonElement valueGeometry : geometryAll) {


                if (valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("city") != null &&
                        valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("city").toString().equals("\"Київ\"") &&
                        valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("_category").toString().equals("\"building\"")) {

                    GeoValueFinal = valueGeometry.getAsJsonObject();

                    break;

                }


                if (valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("town") != null &&
                        valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("town").toString().equals("\"Київ\"") &&
                        valueGeometry.getAsJsonObject().get("components").getAsJsonObject().get("_category").toString().equals("\"building\"")) {

                    GeoValueFinal = valueGeometry.getAsJsonObject();

                    break;

                }


            }


            if (GeoValueFinal == null) {
                GeoValueFinal = makeRequestApi(shop, "([\\d\\/]+).*?(\\,.*)", false).get(0).getAsJsonObject();
            }


            if (GeoValueFinal != null && GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lat").toString().equals("50.45466") &&
                    GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lng").toString().equals("30.5238")) {

                JsonElement GeoValue = GeoValueFinal;

                while (GeoValue != null && GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lat").toString().equals("50.45466") &&
                        GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lng").toString().equals("30.5238")) {

                    System.out.println("FIRST");

                    try {
                        GeoValue = null;

                        GeoValue = makeRequestApi(shop, "([\\d\\/]+)\\-(.*?\\,.*)", false).get(0).getAsJsonObject();

                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (count == 3) {
                        break;
                    }

                    count++;

                    GeoValueFinal = GeoValue;

                }


                count = 0;


                while (GeoValue != null && GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lat").toString().equals("50.45466") &&
                        GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lng").toString().equals("30.5238")) {

                    System.out.println("SECOND");

                    try {
                        GeoValue = null;

                        GeoValue = makeRequestApi(shop, "([\\d\\/]+).*?(\\,.*)", false).get(0).getAsJsonObject();

                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (count == 3) {
                        GeoValueFinal = GeoValue;
                        break;
                    }

                    count++;

                    GeoValueFinal = GeoValue;

                }

                count = 0;

                JsonArray checkGeoValue = null;

                while (GeoValue != null && GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lat").toString().equals("50.45466") &&
                        GeoValue.getAsJsonObject().get("geometry").getAsJsonObject().get("lng").toString().equals("30.5238")) {

                    System.out.println("THIRD");

                    try {
                        GeoValue = null;

                        checkGeoValue = makeRequestApi(shop, "([\\d\\/]+).*?(\\,.*)", true);

                        List<JsonElement> arrGeo = new ArrayList<>();

                        for (JsonElement geo : checkGeoValue) {

                            if (geo.getAsJsonObject().get("components").getAsJsonObject().get("city") != null && geo.getAsJsonObject().get("components").getAsJsonObject().get("city").toString().equals("\"Київ\"") &&
                                    geo.getAsJsonObject().get("components").getAsJsonObject().get("_category").toString().equals("\"building\"")) {

                                arrGeo.add(geo);
                            }

                        }


                        if (arrGeo.size() != 0) {
                            GeoValue = arrGeo.get(0).getAsJsonObject();
                        } else {
                            GeoValue = checkGeoValue.get(0).getAsJsonObject();
                        }

                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (count == 3) {
                        GeoValueFinal = checkGeoValue.get(0).getAsJsonObject();
                        break;
                    }


                    GeoValueFinal = GeoValue;

                    count++;
                }

            }


            if (GeoValueFinal != null) {
System.out.println("FINAL TEST TEST "+ shop);
                System.out.println("FINAL TEST TEST 2222"+ shopsRepository.findByAddress(shop));
                Shops shopObject = shopsRepository.findByAddress(shop);

                Shops createdShops = new Shops();
                createdShops.setAddress(shop);
                createdShops.setLatitude(Double.valueOf(GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lat").toString()));
                createdShops.setLongitude(Double.valueOf(GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lng").toString()));
                createdShops.setIdOfCompany(ID_COMPANY);

                //System.out.println(createdShops);

                if (shopObject == null) {

                    System.out.println("Created");

                    shopsRepository.save(createdShops);

                } else {

                    if (shopObject.getIdOfCompany().equals(ID_COMPANY)) {

                        System.out.println("We have this shoh in DB");

                    } else {

                        shopsRepository.save(createdShops);

                        System.out.println("Created");
                    }


                }

                /*System.out.println("--------------------------------------------------");
                ttt++;
                System.out.println(ttt);
                System.out.println(shop);

                System.out.println(GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lat"));
                System.out.println(GeoValueFinal.getAsJsonObject().get("geometry").getAsJsonObject().get("lng"));

                System.out.println("--------------------------------------------------");*/

            }

            GeoValueFinal = null;

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private static JsonArray makeRequestApi(String shopAddress, String regex, boolean street) {

        CloseableHttpClient httpClient = HttpClients.createDefault();
        ResponseHandler<String> responseHandlerApi = new BasicResponseHandler();

        Pattern check = Pattern.compile(".*?\\,\\s[\\d\\/]+", Pattern.CASE_INSENSITIVE);
        if (!check.matcher(shopAddress).find()) {
            shopAddress = shopAddress.replaceAll("(.*?)(\\s[\\d\\/]+)", "$1,$2");
        }

        Pattern checkPl = Pattern.compile("пл..*?", Pattern.CASE_INSENSITIVE);
        if (checkPl.matcher(shopAddress).find()) {
            shopAddress = shopAddress.replaceAll("пл. ", "").replaceAll("(.*?)(\\,\\s.*)", "$1 площа$2");
        }

        Pattern checkBul = Pattern.compile("(?:Пр-т |пр-т|пр).*?", Pattern.CASE_INSENSITIVE);
        if (checkBul.matcher(shopAddress).find()) {
            shopAddress = shopAddress.replaceAll("Пр-т |пр-т |пр. ", "").replaceAll("(.*?)(\\,\\s.*)", "$1 проспект$2");
        }


        Pattern checkVul = Pattern.compile("(?:вул. |\\bул. ).*?", Pattern.CASE_INSENSITIVE);
        if (checkVul.matcher(shopAddress).find() && street) {
            shopAddress = shopAddress.replaceAll("вул. |^ул. ", "").replaceAll("(.*?)(\\,\\s.*)", "$1 вулиця$2");
            System.out.println("TEST " + shopAddress);
        }


        String fixStreet = shopAddress.replaceAll("(.*?бул\\..*)(,\\s[\\d]+)", "$1 бульвар$2").replaceAll("\\,? корп. [\\d]+| шт. [\\d]+| прим. [\\d]+", "");
        String[] array = fixStreet.replaceAll("\"", "").split("\\.\\s|,\\s|, ");

        String strRequestApi = array[array.length - 1] + ", " + array[array.length - 2].replaceAll("бул. |вул. |Пр-т |пр-т |«Гурман-Фуршет», |ул. |Метрополіта А.", "") + ",Kyiv";

        HttpUriRequest requestMapApi = RequestBuilder.get()
                .setUri("https://api.opencagedata.com/geocode/v1/json")
                //.addParameter("q", strRequestApi.replaceAll("([\\d\\/]+)\\-(.*?\\,.*)","$1$2"))
                .addParameter("q", strRequestApi.replaceAll(regex, "$1$2"))
                .addParameter("key", "641c51bed8ab490184632ad8526e29ad")
                .addParameter("language", "uk")
                .addParameter("no_annotations", "1")
                .build();

        String responseBodyApi = null;
        try {
            responseBodyApi = httpClient.execute(requestMapApi, responseHandlerApi);
        } catch (IOException e) {
            e.printStackTrace();
        }


        JsonObject jsonArrayApi = new JsonParser().parse(responseBodyApi).getAsJsonObject();

        JsonArray geometryAll = jsonArrayApi.get("results").getAsJsonArray();

        return geometryAll;
    }

}
