package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service("ShopVarus")
public class ShopVarus implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;


    @Override
    public void parse() {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        LOGGER.info("start parsing Varus shops");

        try {

            HttpUriRequest request = RequestBuilder.post()
                    .setUri("https://varus.ua/wcity")
                    .addHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36")
                    .addHeader(HttpHeaders.REFERER, "https://varus.ua/uk")
                    .addHeader("Orgin", "https://varus.ua")
                    .addHeader("accept-language", "en-US,en;q=0.9")
                    .addHeader("x-requested-with", "XMLHttpRequest")
                    .addHeader("accept", "*/*")
                    .addHeader("sec-fetch-site", "same-origin")
                    .addHeader("sec-fetch-mode", "cors")
                    //.addHeader("content-length: ", "7")
                    .addHeader("accept-encoding", "gzip, deflate, br")
                    .addParameter("cid", "108")
                    .build();

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(request, responseHandler);

            JsonArray arrayShop = new JsonParser().parse(responseBody).getAsJsonArray();

            for (JsonElement shop:arrayShop){

                JsonObject value = shop.getAsJsonObject().get("_field_data").getAsJsonObject().get("nid").getAsJsonObject().get("entity").getAsJsonObject().get("field_map").getAsJsonObject().get("und").getAsJsonArray().get(0).getAsJsonObject();

                Shops shops = shopsRepository.findByAddress(value.get("address").toString().replaceAll("\"|, Киев, город Киев|Киев,\\s|, Київ|, город Киев", ""));

                if (shops != null) {

                    shops.setAddress(value.get("address").toString().replaceAll("\"|, Киев, город Киев|Киев,\\s|, Київ|, город Киев", ""));
                    shops.setLatitude(Double.valueOf(value.get("latitude").toString().replaceAll("\"", "")));
                    shops.setLongitude(Double.valueOf(value.get("longitude").toString().replaceAll("\"", "")));

                    if (value.get("glid") != null)
                        shops.setNumber(Integer.valueOf(value.get("glid").toString().replaceAll("\"", "")));

                    shops.setIdOfCompany(6);

                    shopsRepository.save(shops);

                } else {

                    Shops shopsVarus = new Shops();

                    shopsVarus.setAddress(value.get("address").toString().replaceAll("\"|, Киев, город Киев|Киев,\\s|, Київ|, город Киев", ""));
                    shopsVarus.setLatitude(Double.valueOf(value.get("latitude").toString().replaceAll("\"", "")));
                    shopsVarus.setLongitude(Double.valueOf(value.get("longitude").toString().replaceAll("\"", "")));

                    if (value.get("glid") != null)
                        shopsVarus.setNumber(Integer.valueOf(value.get("glid").toString().replaceAll("\"", "")));


                    shopsVarus.setIdOfCompany(6);


                    shopsRepository.save(shopsVarus);

                }

            }

            LOGGER.info("Varus shops parsed");

        } catch(ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
