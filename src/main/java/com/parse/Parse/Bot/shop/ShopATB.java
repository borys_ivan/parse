package com.parse.Parse.Bot.shop;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Map;


@Service("ShopATB")
public class ShopATB implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    @Override
    public void parse() {

        JsonObject arrayProductATB;
        CloseableHttpClient httpClient = HttpClients.createDefault();

        LOGGER.info("start parsing ATB shops");

        try {

            URIBuilder builder = new URIBuilder("https://www.atbmarket.com/map/get-region?id=3");
            builder.setParameter("id", "3");

            HttpGet httpGET = new HttpGet(builder.build());

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(httpGET,responseHandler);

            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();

            arrayProductATB = jsonObject.getAsJsonObject("shops").getAsJsonObject("6");



            JsonObject shop;



            for(Map.Entry<String, JsonElement> entry:arrayProductATB.entrySet()){

                shop = entry.getValue().getAsJsonObject();

                Shops shops = shopsRepository.findByAddress(shop.get("address_uk").toString().replaceAll("\"", ""));

                if (shops!=null){

                    shops.setAddress(shop.get("address_uk").toString().replaceAll("\"", ""));
                    shops.setLatitude(Double.valueOf(shop.get("longitude").toString().replaceAll("\"", "")));
                    shops.setLongitude(Double.valueOf(shop.get("latitude").toString().replaceAll("\"", "")));


                    if (shop.get("num") != null)
                        shops.setNumber(Integer.valueOf(shop.get("num").toString().replaceAll("\"", "")));

                    if (shop.get("schedule") != null)
                        shops.setSchedule(shop.get("schedule").toString().replaceAll("\"", ""));


                    shops.setIdOfCompany(1);

                    shopsRepository.save(shops);

                }else {

                    Shops shopsATB = new Shops();

                    shopsATB.setAddress(shop.get("address_uk").toString().replaceAll("\"", ""));
                    shopsATB.setLatitude(Double.valueOf(shop.get("latitude").toString().replaceAll("\"", "")));
                    shopsATB.setLongitude(Double.valueOf(shop.get("longitude").toString().replaceAll("\"", "")));


                    if (shop.get("num") != null)
                        shopsATB.setNumber(Integer.valueOf(shop.get("num").toString().replaceAll("\"", "")));

                    if (shop.get("schedule") != null)
                        shopsATB.setSchedule(shop.get("schedule").toString().replaceAll("\"", ""));

                    shopsATB.setIdOfCompany(1);

                    shopsRepository.save(shopsATB);

                }
            }

            LOGGER.info("ATB shops parsed");

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }
}
