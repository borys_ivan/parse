package com.parse.Parse.Bot.shop;

import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service("ShopСhervonyi")
public class ShopСhervonyi implements Shop{

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 16;

    @Override
    public void parse() {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://chervonyi.com.ua/map/ajax0.base.php");

        LOGGER.info("start parsing Сhervonyi shops");

        try {

            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("action", "city_markers"));
            params.add(new BasicNameValuePair("wcity", "Київ"));
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String theString = IOUtils.toString(instream, StandardCharsets.UTF_8);

                Pattern shop = Pattern.compile(
                        "markers_city.addLayer\\(L.marker\\(\\[(?<lat>[\\d.]+)\\,\\s?(?<lon>[\\d.]+)\\].*?datasearch\\:\\s\\\"<b>.*?<\\/b>\\s?(?<address>.*?)\\\"\\}\\).*?aria-hidden.*?<\\/i>(?<timeS>[\\d\\:]+)\\-(?<timeE>[\\d\\:]+)<\\/a><\\/div>.*?class=\"closeb\"><\\/div>.*?customOptions\\)\\)\\;",
                        Pattern.DOTALL);

                String address = shop.matcher(theString).replaceAll("$3");

                String[] addressArr = address.split("\\n");

                String lat = shop.matcher(theString).replaceAll("$1");
                String[] latArr = lat.split("\\n");

                String lon = shop.matcher(theString).replaceAll("$2");
                String[] lonArr = lon.split("\\n");

                String timeS = shop.matcher(theString).replaceAll("$4");
                String timeE = shop.matcher(theString).replaceAll("$5");
                String[] timeSArr = timeS.split("\\n");
                String[] timeEArr = timeE.split("\\n");

                for (int i =0; i<addressArr.length;i++){

                    Shops shops = shopsRepository.findByAddress(addressArr[i]);

                    if (shops != null) {

                        System.out.println("This address is in BD");

                        if (!shops.getIdOfCompany().equals(ID_COMPANY)) {

                            Shops Сhervonyi = new Shops();

                            Сhervonyi.setAddress(addressArr[i]);
                            Сhervonyi.setLatitude(Double.valueOf(latArr[i]));
                            Сhervonyi.setLongitude(Double.valueOf(lonArr[i]));

                            if (timeSArr[i]!= null)
                                Сhervonyi.setSchedule(timeSArr[i]+" - "+timeEArr[i]);

                            Сhervonyi.setIdOfCompany(ID_COMPANY);

                            shopsRepository.save(Сhervonyi);

                            System.out.println("Save new company shop in this Address");
                        }

                    } else {

                        Shops Сhervonyi = new Shops();

                        Сhervonyi.setAddress(addressArr[i]);
                        Сhervonyi.setLatitude(Double.valueOf(latArr[i]));
                        Сhervonyi.setLongitude(Double.valueOf(lonArr[i]));

                        if (timeSArr[i]!= null)
                            Сhervonyi.setSchedule(timeSArr[i]+" - "+timeEArr[i]);

                        Сhervonyi.setIdOfCompany(ID_COMPANY);

                        shopsRepository.save(Сhervonyi);

                        System.out.println("Save shop in BD");

                    }

                }

            } else {
                System.out.println("Parse problem Сhervonyi ID:"+ID_COMPANY);
            }


        }catch (IOException e) {


            LOGGER.info("Сhervonyi shops parsed");

        } finally {
            httpclient.getConnectionManager().shutdown();
        }

    }
}
