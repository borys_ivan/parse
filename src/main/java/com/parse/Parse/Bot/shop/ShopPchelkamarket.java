package com.parse.Parse.Bot.shop;

import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("ShopPchelkamarket")
public class ShopPchelkamarket extends AbstractParser implements Shop {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 10;

    @Autowired
    @Qualifier("AddressGeoApi")
    private AddressGeoApi addressGeoApi;

    @Override
    public void parse() {

        ArrayList<String> shopsAddress = new ArrayList<>();

        LOGGER.info("start parsing Pchelkamarket shops");

        List<Document> page = new ArrayList<Document>();
        String Url = "https://pchelkamarket.kiev.ua/";

        Element shops = null;
        try {
            shops = connectSetting(Url).get().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements allShop = shops.select("div.adress.tabs__content");

        for (Element shop :allShop) {

            for (Element strShop : shop.select("li")) {

                String[] arrShopStr = strShop.toString().replaceAll("<\\/?li>", "").split(", |<br> ");

                if (arrShopStr[1].replaceAll("\\s","").equals("м.Київ")) {

                   if(arrShopStr.length==5) {
                       //System.out.println(arrShopStr[2] + " " + arrShopStr[3]);
                       shopsAddress.add(arrShopStr[2] + " " + arrShopStr[3]);
                   } else {
                       //System.out.println(arrShopStr[2]);
                       shopsAddress.add(arrShopStr[2]);
                   }

                }

            }
        }

        addressGeoApi.request(shopsAddress,ID_COMPANY);

        LOGGER.info("Pchelkamarket shops parsed");

    }
}
