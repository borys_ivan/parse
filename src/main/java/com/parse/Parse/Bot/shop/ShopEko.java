package com.parse.Parse.Bot.shop;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.regex.Pattern;

@Service("ShopEkoMarket")
public class ShopEko implements Shop {

    @Autowired
    @Qualifier("shopsRepository")
    private ShopsRepository shopsRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private final static int ID_COMPANY = 14;

    @Override
    public void parse() {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://www.eko.com.ua/ua/network/region/");

        LOGGER.info("start parsing EkoMarket shops");

        try {
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("rid", new StringBody("1"));
            reqEntity.addPart("city_id", new StringBody("3"));
            reqEntity.addPart("loc_id", new StringBody("0"));

            httppost.setEntity(reqEntity);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);

            String html = Pattern.compile(".*?var shops = (\\[.*?\\]);.*", Pattern.DOTALL).matcher(responseBody).replaceAll("$1");

            JsonArray jsonObject = new JsonParser().parse(html).getAsJsonArray();

            for (JsonElement shop : jsonObject) {

                if (shop.getAsJsonObject().get("city_name").toString().equals("\"Київ\"")) {

                    Shops shops = shopsRepository.findByAddress(shop.getAsJsonObject().get("shop_mode").toString().replaceAll("\"", ""));

                    if (shops != null) {

                        System.out.println("This address is in BD");

                        if (!shops.getIdOfCompany().equals(ID_COMPANY)) {

                            Shops shopsEko = new Shops();

                            shopsEko.setAddress(shop.getAsJsonObject().get("shop_address").toString().replaceAll("\"", ""));
                            shopsEko.setLatitude(Double.valueOf(shop.getAsJsonObject().get("shop_lat").toString().replaceAll("\"", "")));
                            shopsEko.setLongitude(Double.valueOf(shop.getAsJsonObject().get("shop_lng").toString().replaceAll("\"", "")));

                            if (shop.getAsJsonObject().get("shop_mode").toString() != null)
                                shopsEko.setSchedule(shop.getAsJsonObject().get("shop_mode").toString().replaceAll("\"", ""));

                            shopsEko.setIdOfCompany(ID_COMPANY);

                            shopsRepository.save(shopsEko);

                            System.out.println("Save new company shop in this Address");

                        }

                    } else {

                        Shops shopsEko = new Shops();

                        shopsEko.setAddress(shop.getAsJsonObject().get("shop_address").toString().replaceAll("\"", ""));
                        shopsEko.setLatitude(Double.valueOf(shop.getAsJsonObject().get("shop_lat").toString().replaceAll("\"", "")));
                        shopsEko.setLongitude(Double.valueOf(shop.getAsJsonObject().get("shop_lng").toString().replaceAll("\"", "")));

                        if (shop.getAsJsonObject().get("shop_mode").toString() != null)
                            shopsEko.setSchedule(shop.getAsJsonObject().get("shop_mode").toString().replaceAll("\"", ""));

                        shopsEko.setIdOfCompany(14);

                        shopsRepository.save(shopsEko);

                        System.out.println("Save shop in BD");

                    }

                }

            }

            LOGGER.info("EkoMarket shops parsed");

        } catch (IOException e) {


        } finally {
            httpclient.getConnectionManager().shutdown();
        }

    }
}
