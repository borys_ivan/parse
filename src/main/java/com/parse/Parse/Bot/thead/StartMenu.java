package com.parse.Parse.Bot.thead;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.Bot.message.MessageAction;
import com.parse.Parse.Bot.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.util.TimerTask;

@Component
@Scope("prototype")
public class StartMenu extends TimerTask {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private MessageAction messageAction;

    private Integer id;
    private Boolean state;
    private UserSession userSession;

    public UserSession getUserSession() {
        return userSession;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public void run() {

        if(getState())
            messageAction.deleteMessage(userSession);

        if (userSession.oldReguestMessanger.size()==0 && getState()) {

            setState(false);
            Integer messageID = getId();
            SendMessage sendMessage = new SendMessage().setChatId(messageID.toString());
            sendMessage.setText("/start почати пошук по магазинах");

            ProductBot startMenu = (ProductBot)context.getBean("productBot");

            try {
                Message regProduct = startMenu.execute(sendMessage);
                userSession.oldReguestMessanger.add(regProduct);

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }
    }

}
