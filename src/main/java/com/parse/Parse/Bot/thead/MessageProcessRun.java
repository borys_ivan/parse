package com.parse.Parse.Bot.thead;


import com.parse.Parse.Bot.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class MessageProcessRun {


    @Autowired
    private ApplicationContext context;

    public void runProcessBot(){

        RemindProductMessage remindProductMessage = context.getBean(RemindProductMessage.class);
        ScheduledExecutorService scheduleTask = Executors.newScheduledThreadPool(1);

        scheduleTask.scheduleAtFixedRate(remindProductMessage,1,1,TimeUnit.DAYS);

    }

}
