package com.parse.Parse.Bot.thead;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.entity.RequestUserBot;
import com.parse.Parse.entity.UsersBot;
import com.parse.Parse.repository.RequestUserBotRepository;
import com.parse.Parse.repository.UsersBotRepository;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RemindProductMessage extends TimerTask {

    @Autowired
    private ApplicationContext context;

    @Qualifier("requestUserBotRepository")
    @Autowired
    private RequestUserBotRepository requestUserBotRepository;


    @Qualifier("usersBotRepository")
    @Autowired
    private UsersBotRepository usersBotRepository;


    @Override
    public void run() {
        System.out.println("REMINDMESSAGE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


        List<UsersBot> usersBotList;
        List<String> sortRequest = new ArrayList<String>();
        HashMap<UsersBot, List<String>> usersPriorityRequest = new HashMap<>();


        try {

            usersBotList = usersBotRepository.findAll();


            DateTime nowDate = new DateTime();
            DateTime lastVisitUsers;
            int days;

            for (UsersBot usersBot : usersBotList) {


                List<RequestUserBot> requestUserBots = requestUserBotRepository.findByUsersBot(usersBot, Sort.by(Sort.Direction.DESC, "countRequest"));

                for (RequestUserBot ttt : requestUserBots) {

                    if (sortRequest.size() <= 2) {
                        sortRequest.add(ttt.getRequest());
                    }


                }

                lastVisitUsers = new DateTime(usersBot.getLastVisit());

                days = Days.daysBetween(lastVisitUsers, nowDate).getDays();


                if (days >= 3) {

                    usersPriorityRequest.put(usersBot, sortRequest);

                    UsersBot usersBotRemind = usersBotRepository.findByIdUserBot(usersBot.getIdUserBot());

//                    System.out.println(usersBotRemind);

                    usersBotRemind.setRemind(true);
                    usersBotRepository.save(usersBotRemind);

                    //sendRemindMessage(usersPriorityRequest);
                    sendRemindMessage(sortRequest,usersBot.getChatID());

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }




        public void sendRemindMessage(List<String> sortRequest, Long getChatID){

        //ProductBot sendRemind = (ProductBot)context.getBean("productBot");
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        SendMessage backMenu = new SendMessage();
        //Message message = new Message();

        //System.out.println("CHAT ID "+message.getChatId());



        backMenu.setChatId(getChatID);
        backMenu.setText("Ви давно не шукали акційних товарів, можливо зявилось щось нове по тим товарам що ви шукали найчастіше");




            //List<InlineKeyboardButton> lists = new ArrayList<>();
            int count = 0;

            for(String request :sortRequest){

                List<InlineKeyboardButton> lists = new ArrayList<>();

                lists.add(0, new InlineKeyboardButton().setText(request).setCallbackData("/find All "+request));


            //rowsInline.add(count, new InlineKeyboardButton().setText(request).setCallbackData("/find "+request));

                rowsInline.add(count,lists);

            count++;
        }

        //allShop.add(new InlineKeyboardButton().setText("Всі магазини").setCallbackData("/find All"));

        //rowsInline.add(allShop);

        markupInline.setKeyboard(rowsInline);

            backMenu.setReplyMarkup(markupInline);

            ProductBot remindMessage = (ProductBot)context.getBean("productBot");
        try {

            Message selectMenu = remindMessage.execute(backMenu);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }


}
