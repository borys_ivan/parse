package com.parse.Parse.Bot.thead;

import com.parse.Parse.Bot.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ThreadProcessBot {

    public StartMenu hello;
    public Timer timer;
    public Integer time=60000*15;


    @Autowired
    private ApplicationContext context;

    public void runProcessBot(Update update, Map<Integer, UserSession> userSessions){

        timer = new Timer();

        StartMenu startMenu = context.getBean(StartMenu.class);
        startMenu.setId(update.getMessage().getFrom().getId());
        startMenu.setUserSession(userSessions.get(update.getMessage().getFrom().getId()));

        timer.schedule(hello=startMenu, time, time);//15 Min
        hello.setState(true);



        RemindProductMessage remindProductMessage = context.getBean(RemindProductMessage.class);

        ScheduledExecutorService scheduleTask = Executors.newScheduledThreadPool(1);


        scheduleTask.scheduleAtFixedRate(remindProductMessage,1,1,TimeUnit.DAYS);

    }

}
