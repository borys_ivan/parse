package com.parse.Parse.Bot.thead;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.Bot.session.UserSession;
import com.parse.Parse.Bot.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Map;

@Component
public class DowloadMenuThread implements Runnable {

    @Autowired
    private ApplicationContext context;

    @Autowired
    public Template template;

    private Long chatID;

    private Message deleteID;

    /*public DowloadMenuThread(Long chatID,Boolean d){
        this.chatID=chatID;
        this.download=download;
    }*/

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    volatile Boolean download=true;

    public Boolean getDownload() {
        return download;
    }

    public void setDownload(Boolean download) {
        this.download = download;
    }

    public Message getDeleteID() {
        return deleteID;
    }

    public void setDeleteID(Message deleteID) {
        this.deleteID = deleteID;
    }

    @Override
    public void run() {

        while(download){

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            sendMsg(chatID);
            setDownload(false);
        }


    }


    public void delete() {

        if(getDeleteID()!=null) {
            System.out.println("11111111 DELETE !!!!!!!!!!!!!!!!!!!");

            DeleteMessage deleteMessage = new DeleteMessage(deleteID.getChatId(), deleteID.getMessageId());

            ProductBot clean = (ProductBot) context.getBean("productBot");

            try {
                clean.execute(deleteMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            setDeleteID(null);

        }

    }


    private void sendMsg(Long chatID) {

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatID);

        sendMessage.setText("Зачекайте йде загрузка запиту з сервера...");

        try {

            ProductBot send = (ProductBot)context.getBean("productBot");
            setDeleteID(send.execute(sendMessage));

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

}
