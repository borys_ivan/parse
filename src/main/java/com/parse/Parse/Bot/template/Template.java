package com.parse.Parse.Bot.template;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.Bot.message.MessageAction;
import com.parse.Parse.Bot.session.UserSession;
import com.parse.Parse.Bot.shop.ShopATB;
import com.parse.Parse.entity.Company;
import com.parse.Parse.entity.GeoListOfShops;
import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Shops;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.repository.ProductRepository;
import com.parse.Parse.repository.ShopsRepository;
import com.parse.Parse.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;

@Component
public class Template {

    @Qualifier("companyRepository")
    @Autowired
    private CompanyRepository companyRepository;

    @Qualifier("shopsRepository")
    @Autowired
    private ShopsRepository shopsRepository;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private MessageAction messageAction;

    @Autowired
    private ProductServiceImpl productServiceImpl;

    public ArrayList<SendPhoto> listImageProductTemplate(Message message, List<Product> listProduct){

        ArrayList<SendPhoto> list = new ArrayList<>();

        for (Product line:listProduct) {

            SendPhoto sendPhoto = new SendPhoto();

            sendPhoto.setChatId(message.getChatId());
            sendPhoto.setPhoto(line.getImage());


            sendPhoto.setCaption(line.getName() + "\n нова ціна: " + line.getPrice() + "\n стара ціна: " + line.getOldPrice()+ "\nмагазин: " + line.getCompany());
            /*try {

                Message bot = execute(sendPhoto);
                //oldReguestMessanger.add(bot);
                userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(bot);

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }*/


            list.add(sendPhoto);

            //return sendPhoto;
        }

        return list;

    }


    public void listImageProductAndShopsTemplate(Message message, HashMap <Product,List<Shops>> listOfProducts, Map<Integer, UserSession> userSessions){

        SendPhoto sendPhoto = new SendPhoto();
        SendMessage sendMessage = new SendMessage();
        ProductBot executeLocation = (ProductBot)context.getBean("productBot");

        for(Map.Entry<Product, List<Shops>> entry : listOfProducts.entrySet()) {

            sendPhoto.setChatId(message.getChatId());
            sendPhoto.setPhoto(entry.getKey().getImage());

            sendPhoto.setCaption(entry.getKey().getName() + "\n нова ціна: " + entry.getKey().getPrice() + "\n стара ціна: " + entry.getKey().getOldPrice()+ "\nмагазин: " + entry.getKey().getCompany());

            try {

                Message imageProduct;
                imageProduct = executeLocation.execute(sendPhoto);

                    userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(imageProduct);

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            StringBuilder strShop = new StringBuilder("");

            List<InlineKeyboardButton> block1 = new ArrayList<>();
            List<InlineKeyboardButton> block2 = new ArrayList<>();
            List<InlineKeyboardButton> block3 = new ArrayList<>();

            InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

            int count=1;

            for (Shops shops : entry.getValue()){

                strShop.append(count+") "+shops.getAddress()+"\n");
                block1.add(new InlineKeyboardButton().setText(Integer.toString(count)).setCallbackData("/mapLocation "+shops.getLatitude()+" "+shops.getLongitude()));

                count++;
            }

            block2.add(new InlineKeyboardButton().setText("Повернутись до пошуку").setCallbackData("/repeatGeoSearch"));
            block2.add(new InlineKeyboardButton().setText("Зберегти товар в список бажаних товарів").setCallbackData("/priorityOrderList "+entry.getKey().getId()));

            sendMessage.setChatId(message.getChatId().toString()).setText("Найближчі магазини:\n" + strShop);


            try {
                Message menuBack;
                menuBack = executeLocation.execute(sendMessage);

                userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(menuBack);


            rowsInline.add(block1);
            rowsInline.add(block2);
            rowsInline.add(block3);

            markupInlineBackMenu.setKeyboard(rowsInline);

            SendMessage backMenu = new SendMessage();
            backMenu.setChatId(message.getChatId().toString());

            backMenu.setText("Виберіть магазин: ");

            backMenu.setReplyMarkup(markupInlineBackMenu);

                Message shopMenu;
                shopMenu = executeLocation.execute(backMenu);

                userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(shopMenu);

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }


        List<InlineKeyboardButton> block4 = new ArrayList<>();
        InlineKeyboardMarkup markupInlineBackMenu2 = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline2 = new ArrayList<>();

        block4.add(new InlineKeyboardButton().setText("Зайти в список бажань").setCallbackData("/userOrderList"));


        try {

            rowsInline2.add(block4);

            markupInlineBackMenu2.setKeyboard(rowsInline2);

            SendMessage menuOrderList = new SendMessage();
            menuOrderList.setChatId(message.getChatId().toString());

            menuOrderList.setText("------------------------------------------");

            menuOrderList.setReplyMarkup(markupInlineBackMenu2);

            Message Menu;
            Menu = executeLocation.execute(menuOrderList);

            //userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(Menu);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    public List<List<InlineKeyboardButton>> menuShopTemplate(Update update, String reguest){

        List<Company> companyList = companyRepository.findAll();
        //InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> allShop = new ArrayList<>();
        int elementRow = 2;
        int elementCompany = companyList.size();

        double count = Math.ceil((float) elementCompany / elementRow);
        int totalCount = 0;

        for (int i = 0; i <= count - 1; i++) {

            List<InlineKeyboardButton> lists = new ArrayList<>();

            for (int n = 0; n <= elementRow - 1; n++) {

                if (companyList.size() - 1 >= totalCount) {

                    if (lists.size() < elementRow) {
                        lists.add(n, new InlineKeyboardButton().setText(companyList.get(totalCount).getName()).setCallbackData("/compare "+companyList.get(totalCount).getName()));
                    } else {
                        lists.set(n, new InlineKeyboardButton().setText(companyList.get(totalCount).getName()).setCallbackData("/compare "+companyList.get(totalCount).getName()));
                    }

                    totalCount++;

                }

            }

            rowsInline.add(i, lists);

        }


        return rowsInline;

        /*markupInline.setKeyboard(rowsInline);


        try {

            sendMsg(update.getCallbackQuery().getMessage().getChatId(),update.getCallbackQuery().getFrom().getId(), markupInline);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }


    public void generalMenu(Message message, Map<Integer, UserSession> userSessions){

        InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> lists1 = new ArrayList<>();
        List<InlineKeyboardButton> lists2 = new ArrayList<>();
        List<InlineKeyboardButton> lists3 = new ArrayList<>();

        lists1.add(new InlineKeyboardButton().setText("Пошук по продукту").setCallbackData("/findByProduct"));
        lists2.add(new InlineKeyboardButton().setText("Пошук продукту в радіусі 15 хв").setCallbackData("/productRadius"));
        lists3.add(new InlineKeyboardButton().setText("Зайти в список бажань").setCallbackData("/userOrderList"));

        rowsInline.add(lists1);
        rowsInline.add(lists2);
        rowsInline.add(lists3);

        markupInlineBackMenu.setKeyboard(rowsInline);

        SendMessage backMenu = new SendMessage();
        backMenu.setChatId(message.getChatId());
        backMenu.setText("Головне меню: ");

        backMenu.setReplyMarkup(markupInlineBackMenu);

        ProductBot addMenuProduct = (ProductBot)context.getBean("productBot");

        try {

            Message menu = addMenuProduct.execute(backMenu);

            userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(menu);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    public void addMenuProduct(Message message, Map<Integer, UserSession> userSessions){

        InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline2 = new ArrayList<>();
        List<InlineKeyboardButton> lists2 = new ArrayList<>();
        List<InlineKeyboardButton> lists3 = new ArrayList<>();
        List<InlineKeyboardButton> lists4 = new ArrayList<>();
        List<InlineKeyboardButton> lists5 = new ArrayList<>();


        //if(userSessions.get(message.getFrom().getId()).getCurrentPage()>0)
        if(userSessions.get(message.getChat().getId().intValue()).getCurrentPage()>0)
            lists2.add(new InlineKeyboardButton().setText("Попередні товари").setCallbackData("pageBack"));


        //if(userSessions.get(message.getFrom().getId()).getCurrentPage()<userSessions.get(message.getFrom().getId()).getTotalCountPage()-1)
        if(userSessions.get(message.getChat().getId().intValue()).getCurrentPage()<userSessions.get(message.getChat().getId().intValue()).getTotalCountPage()-1)
         lists2.add(new InlineKeyboardButton().setText("Наступні товари").setCallbackData("pageNext"));

        lists5.add(new InlineKeyboardButton().setText("Найближчі магазини").setCallbackData("/getGoeLocation"));


        lists3.add(new InlineKeyboardButton().setText("Повернутись до вибору магазину").setCallbackData("/findByProduct"));
        lists4.add(new InlineKeyboardButton().setText("Зрівняти продукти з іншим магазином").setCallbackData("/compare"));
        rowsInline2.add(lists2);
        rowsInline2.add(lists5);
        rowsInline2.add(lists3);
        rowsInline2.add(lists4);

        markupInlineBackMenu.setKeyboard(rowsInline2);

        SendMessage backMenu = new SendMessage();
        backMenu.setChatId(message.getChatId());
        backMenu.setText("Можете продовжити пошук в даному магазин або ви можете повернутись до вибору нового");

        backMenu.setReplyMarkup(markupInlineBackMenu);

        ProductBot addMenuProduct = (ProductBot)context.getBean("productBot");

        try {

            Message menu = addMenuProduct.execute(backMenu);
            //oldReguestMessanger.add(menu);
            //userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(menu);
            userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(menu);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    public void notFoundProduct(Message message, Map<Integer, UserSession> userSessions){

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId().toString()).setText("Такого акційного товару не виявлено, попробуйте вести щось інакше");

        ProductBot notFound = (ProductBot)context.getBean("productBot");

        try {
            Message bot = notFound.execute(sendMessage);
            userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(bot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    public void listOfMarket(Long chatID,Integer messageId ,Map<Integer, UserSession> userSessions){

        List<Company> companyList = companyRepository.findAll();
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> allShop = new ArrayList<>();
        int elementRow = 2;
        int elementCompany = companyList.size();

        double count = Math.ceil((float) elementCompany / elementRow);
        int totalCount = 0;

        for (int i = 0; i <= count - 1; i++) {

            List<InlineKeyboardButton> lists = new ArrayList<>();

            for (int n = 0; n <= elementRow - 1; n++) {

                if (companyList.size() - 1 >= totalCount) {

                    if (lists.size() < elementRow) {
                        lists.add(n, new InlineKeyboardButton().setText(companyList.get(totalCount).getName()).setCallbackData("/find "+companyList.get(totalCount).getName()));
                    } else {
                        lists.set(n, new InlineKeyboardButton().setText(companyList.get(totalCount).getName()).setCallbackData("/find "+companyList.get(totalCount).getName()));
                    }

                    totalCount++;

                }

            }

            rowsInline.add(i, lists);

        }

        allShop.add(new InlineKeyboardButton().setText("Всі магазини").setCallbackData("/find All"));

        rowsInline.add(allShop);

        markupInline.setKeyboard(rowsInline);


        try {

            messageAction.sendMsg(chatID,messageId, markupInline,userSessions);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public SendMessage compareTemplate(Message message,List<Product> text,String company ,Map<Integer, UserSession> userSessions){

        boolean lock=false;

        if(text==null){

            notFoundProduct(message,userSessions);

            lock=false;

        }else{

            try {

                Message bot;

                ProductBot compare = (ProductBot)context.getBean("productBot");

                for (SendPhoto sendPhoto : listImageProductTemplate(message,text)) {

                    bot = compare.execute(sendPhoto);
                    userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(bot);

                }

            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            lock=true;

        }

        InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline2 = new ArrayList<>();
        List<InlineKeyboardButton> lists = new ArrayList<>();


        lists.add(new InlineKeyboardButton().setText("Повернутись до вибору магазину").setCallbackData("/findByProduct"));

        rowsInline2.add(lists);

        markupInlineBackMenu.setKeyboard(rowsInline2);

        SendMessage backMenu = new SendMessage();
        backMenu.setChatId(message.getChatId());
        backMenu.setText("Продукти з магазину "+ company);

        backMenu.setReplyMarkup(markupInlineBackMenu);

        /*try {

            Message menu = execute(backMenu);
            userSessions.get(message.getFrom().getId()).oldReguestMessanger.add(menu);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }*/

        return backMenu;

    }

    public void geoLocation(Update update,Map<Integer, UserSession> userSessions){

        SendMessage sendMessage = new SendMessage();

        if(update.getCallbackQuery()!=null){

            sendMessage
                    .setChatId(update.getCallbackQuery().getMessage().getChat().getId())
                    .setText("Необхідна Ваша геолокацію щоб знайти наближчі магазини");

        } else {

            sendMessage
                    .setChatId(update.getMessage().getChat().getId())
                    .setText("Необхідна Ваша геолокацію щоб знайти наближчі магазини");

        }

        // create keyboard
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        // new list
        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        KeyboardButton keyboardButton = new KeyboardButton();
        keyboardButton.setText("Поділитись своєю локацією >").setRequestLocation(true);
        keyboardFirstRow.add(keyboardButton);

        // add array to list
        keyboard.add(keyboardFirstRow);

        // add list to our keyboard
        replyKeyboardMarkup.setKeyboard(keyboard);

        try {
           ProductBot location = (ProductBot)context.getBean("productBot");

           Message message = location.execute(sendMessage);

           userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(message);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }


    }


    public void distanceToShop(Update update,Map<Integer, UserSession> userSessions){

        Message updateActionMessage;

        if(update.getMessage()!=null){
            updateActionMessage = update.getMessage();
        } else {
            updateActionMessage = update.getCallbackQuery().getMessage();
        }

        System.out.println("companyRepository.findAllByName(userSessions.get(updateActionMessage.getChat().getId().intValue()).getCompany()) "+userSessions.get(updateActionMessage.getChat().getId().intValue()).getCompany());

        Company company = companyRepository.findAllByName(userSessions.get(updateActionMessage.getChat().getId().intValue()).getCompany());
        List<Shops> shops = shopsRepository.findAllByIdOfCompany(company.getId());

        LinkedHashMap<Double,Shops> requestShops = new LinkedHashMap<>();

        for (Shops shop : shops) {

            requestShops.put(distance(updateActionMessage.getLocation().getLatitude(),updateActionMessage.getLocation().getLongitude(), shop.getLatitude(),shop.getLongitude(),"K"),shop);

        }

        InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline2 = new ArrayList<>();
        List<InlineKeyboardButton> lists2 = new ArrayList<>();


        Map<Double, Shops> treeMap = new TreeMap<Double, Shops>(requestShops);
        boolean longDistance = false;

        requestShops.clear();

        for(Map.Entry<Double, Shops> entry : treeMap.entrySet()) {

            longDistance=false;

            Double key = entry.getKey();
            Shops value = entry.getValue();

            if(key<=2.200) {

                requestShops.put(key,value);

            }
        }

        if(requestShops.isEmpty()){

            longDistance=true;

            treeMap.get(treeMap.keySet().toArray()[0]);
            requestShops.put(Double.valueOf(treeMap.keySet().toArray()[0].toString()),treeMap.get(treeMap.keySet().toArray()[0]));

        }


        SendMessage sendMessage = new SendMessage();
        StringBuilder strShop= new StringBuilder(new String());
        Integer count=1;

        for(Map.Entry<Double, Shops> entry : requestShops.entrySet()) {
            Double key = entry.getKey();
            Shops value = entry.getValue();

            strShop.append(count+") "+value.getAddress()).append("\n~ ").append(String.format("%.3f", key)).append(" км\n\n");
            lists2.add(new InlineKeyboardButton().setText(count.toString()).setCallbackData("/mapLocation "+value.getLatitude()+" "+value.getLongitude()));

            count++;
        }


        if (longDistance){

            sendMessage.setChatId(updateActionMessage.getChatId().toString()).setText("Поблизу з вами нема даного магазину, найближий магазин:\n" + strShop);

        } else {

            sendMessage.setChatId(updateActionMessage.getChatId().toString()).setText("Найближчі магазини:\n" + strShop);
        }

         ProductBot executeLocation = (ProductBot)context.getBean("productBot");

        try {
            Message message = executeLocation.execute(sendMessage);
            userSessions.get(updateActionMessage.getChat().getId().intValue()).oldReguestMessanger.add(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }


        rowsInline2.add(lists2);

        markupInlineBackMenu.setKeyboard(rowsInline2);

        SendMessage backMenu = new SendMessage();
        backMenu.setChatId(updateActionMessage.getChatId());
        backMenu.setText("Виберіть необхідний магазин, щоб показати його на карті: ");

        backMenu.setReplyMarkup(markupInlineBackMenu);

        try {
            executeLocation.execute(backMenu);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    public UserSession distanceRadiusToShop(Update update,UserSession userSessions){

        List<Shops> shops = shopsRepository.findAll();

        LinkedHashMap<Double,Shops> requestShops = new LinkedHashMap<>();

        for (Shops shop : shops) {

            requestShops.put(distance(update.getMessage().getLocation().getLatitude(),update.getMessage().getLocation().getLongitude(), shop.getLatitude(),shop.getLongitude(),"K"),shop);

        }

        Map<Double, Shops> treeMap = new TreeMap<Double, Shops>(requestShops);

        requestShops.clear();

        Double key;
        Shops value;

        for(Map.Entry<Double, Shops> entry : treeMap.entrySet()) {

             key = entry.getKey();
             value = entry.getValue();
//if(key<=4.200) {
            if(key<=1.200) {

                requestShops.put(key,value);

            }
        }

        Set<Integer> IdOfShops = new HashSet<>();

        Map<Double, Shops> listOfShops = new TreeMap<Double, Shops>(requestShops);
        for(Map.Entry<Double, Shops> entry : listOfShops.entrySet()) {

            IdOfShops.add(entry.getValue().getIdOfCompany());

        }


        ProductBot executeMessage = (ProductBot)context.getBean("productBot");

        SendMessage question = new SendMessage();

        question.setChatId(update.getMessage().getChatId());
        question.setText("Введіть товар який хочете знайти в радіусі 15хв");

        try {

           Message message = executeMessage.execute(question);

            ProductBot.geoSearch = true;

            userSessions.oldReguestMessanger.add(message);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        userSessions.setGeoListOfShops(new GeoListOfShops(IdOfShops,listOfShops));

        return userSessions;

        //return new GeoListOfShops(IdOfShops,listOfShops);
    }



    public HashMap<Product,List<Shops>> distanceRadiusToProduct(Set<Integer> IdOfShops,Map<Double, Shops> listOfShops,String stringOfSearch){

        //-----------------------------------------------------------------------
        Company company = null;
        List<Product> listOfProduct = new ArrayList<>();
        List<Product> test = new ArrayList<>();

        HashMap<Product,List<Shops>> productWithShpos = new HashMap<>();


        for (Integer id:IdOfShops){
            company = companyRepository.findById(id).get();
            //System.out.println(company.getName());
            listOfProduct = productServiceImpl.findAllByNameContainingIgnoreCaseAndCompanyAndVisible(stringOfSearch,company.getName(),true, Sort.by("price"));

            if(listOfProduct!=null){

                test.addAll(listOfProduct);


            }

        }


        //HashMap<Product,List<Shops>> productWithShpos = new HashMap<>();

        for(Integer id:IdOfShops){
            company = companyRepository.findById(id).get();

            for (Product t:test){

                if(t.getCompany().equals(company.getName())){

                    productWithShpos.put(t,null);
                }

                System.out.println("----------------------------------------------------------11111");
                List<Shops> qqq = new  ArrayList<>();

                for(Map.Entry<Double, Shops> entry : listOfShops.entrySet()) {

                    //System.out.println(entry.getValue().getAddress());

                    //company = companyRepository.findById(id).get();
                    company = companyRepository.findAllByName(t.getCompany());

                    //if(productWithShpos.containsKey(t) && entry.getValue().getIdOfCompany().equals(id)){
                    if(productWithShpos.containsKey(t) && entry.getValue().getIdOfCompany().equals(company.getId())){

                        System.out.println(entry.getKey()+" "+entry.getValue());

                        //List<Shops> shopsByProduct

                        qqq.addAll(Collections.singleton(entry.getValue()));
                        productWithShpos.put(t,qqq);
                        // productWithShpos.put(t, Collections.singletonList(productWithShpos.get(t),entry.getValue()));

                    }

                }
                System.out.println("----------------------------------------------------------11111");

            }


        }

        return productWithShpos;

    }


    public void addMenuProductTest(Message message, Map<Integer, UserSession> userSessions){

        InlineKeyboardMarkup markupInlineBackMenu = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline2 = new ArrayList<>();
        List<InlineKeyboardButton> lists5 = new ArrayList<>();

        lists5.add(new InlineKeyboardButton().setText("Гео пошуку").setCallbackData("/getGoeLocationRadius"));

        rowsInline2.add(lists5);

        markupInlineBackMenu.setKeyboard(rowsInline2);

        SendMessage backMenu = new SendMessage();
        backMenu.setChatId(message.getChatId());
        backMenu.setText("Введіть товар який ви хочете знайти в радіусі 15хв");

        backMenu.setReplyMarkup(markupInlineBackMenu);

        ProductBot addMenuProduct = (ProductBot)context.getBean("productBot");

        try {

            Message menu = addMenuProduct.execute(backMenu);

            userSessions.get(message.getChat().getId().intValue()).oldReguestMessanger.add(menu);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (unit.equals("K")) {
                dist = dist * 1.609344;
            } else if (unit.equals("N")) {
                dist = dist * 0.8684;
            }

            return (dist);
        }
    }

}
