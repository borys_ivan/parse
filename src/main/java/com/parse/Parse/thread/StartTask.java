package com.parse.Parse.thread;

import com.parse.Parse.Bot.ProductBot;
import com.parse.Parse.entity.Company;
import com.parse.Parse.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@Component
@Scope("prototype")
public class StartTask extends TimerTask {
    private Timer timer;

    private Date date;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run() {

        try {

          /*  ApiContextInitializer.init();

		TelegramBotsApi botsApi = new TelegramBotsApi();

		try {
			botsApi.registerBot(new ProductBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}*/

            System.out.println("Wake up!!!");
            //parseThread.run();
            ParseThread ParseThread = applicationContext.getBean(ParseThread.class);
            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
            service.schedule(ParseThread, 10, TimeUnit.SECONDS);
            timer.cancel();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }


    }


}


