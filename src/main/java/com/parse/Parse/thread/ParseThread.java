package com.parse.Parse.thread;

import com.parse.Parse.entity.Company;
import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.Product;
import com.parse.Parse.parse.*;
import com.parse.Parse.recognizeImage.ConnectToApi;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope("prototype")
public class ParseThread implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);

    @Autowired
    private ProductService productService;


    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;

    @Autowired
    @Qualifier("companyRepository")
    private CompanyRepository companyRepository;

    @Autowired
    @Qualifier("ATB")
    private Parse parseATB;

    @Autowired
    @Qualifier("Furshet")
    private Parse parseFurshet;

    @Autowired
    @Qualifier("Silpo")
    private ParseJson parseSilpo;

    @Autowired
    @Qualifier("Auchan")
    private Parse parseAuchan;

    @Autowired
    private ApplicationContext companyParse;

    /*@Autowired
    private RecognizeImage recognizeImage;*/

    @Autowired
    private ConnectToApi connectToApi;



    @Override
    public void run() {

        List<Company> companyList = companyRepository.findAllByParse(true);

        parseProcessRepository.deleteAll();


        LOGGER.info("Parsing products from site");

        ArrayList<EntityProduct> parsedProducts;

        for(Company company:companyList) {

            LOGGER.info(company.getName());


            try {

                Class<?> act = Class.forName("com.parse.Parse.parse.Parse"+company.getName());

                if(act.getGenericSuperclass().toString().equals("class com.parse.Parse.parse.AbstractParser")){
                    AbstractParser com = (AbstractParser) companyParse.getBean(company.getName());

                    parsedProducts = com.getInform();

                    LOGGER.info(productService.saveProduct(parsedProducts));
                    productService.checkProduct(parsedProducts,company.getName());
                    /*update price and img src in product*/
                    productService.updateProduct(parsedProducts);


                    System.out.println("total : "+Runtime.getRuntime().totalMemory());
                    System.out.println("free : "+Runtime.getRuntime().freeMemory());
                    System.out.println("-----------------------------------------------");

                }


                if(act.getGenericSuperclass().toString().equals("class com.parse.Parse.parse.AbstractParserImage")){
                //if(act.getInterfaces()[0].toString().equals("interface com.parse.Parse.parse.ParseImage")){
                    AbstractParserImage com = (AbstractParserImage) companyParse.getBean(company.getName());

                    //com.connect();
                    com.connectImage();

                    //ArrayList<EntityProduct> image = recognizeImage.recognize();

                    ArrayList<EntityProduct> image = connectToApi.connect();

                    for(EntityProduct test:image){
                        System.out.println(test.getName() +" | "+test.getImage());
                        //System.out.println(test.getImage());
                    }


                    //EntityProduct[] array = new EntityProduct[image.size()];
                    ArrayList<EntityProduct> array = new ArrayList<>();
                    int count=0;

                    for(EntityProduct value :image){

                        /*array[count] = new EntityProduct(count, value.getName(), 0.0, 0.0,
                                new Date(), null, "Kishenya", value.getImage());*/
                        array.add(new EntityProduct(count, value.getName(), 0.0, 0.0,
                                new Date(), null, "Kishenya", value.getImage()));

                        count++;
                    }


                    productService.saveProduct(array);
                    productService.checkProduct(array,company.getName());


                    //LOGGER.info(productService.saveProduct(com.getInform()));
                    //productService.checkProduct(com.getInform(),company.getName());

                    System.out.println("total : "+Runtime.getRuntime().totalMemory());
                    System.out.println("free : "+Runtime.getRuntime().freeMemory());
                    System.out.println("-----------------------------------------------");

                }


                System.out.println("total : "+Runtime.getRuntime().totalMemory());
                System.out.println("free : "+Runtime.getRuntime().freeMemory());


            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }

        LOGGER.info("Finished parse from markets");
    }
}
