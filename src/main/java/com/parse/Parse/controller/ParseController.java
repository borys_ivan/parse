package com.parse.Parse.controller;



//import com.parse.Parse.Bot.ProductBot;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.Bot.shop.*;
import com.parse.Parse.Bot.shopAdsPDF.MakePdfFile;
import com.parse.Parse.Bot.shopAdsPDF.ShopAdsPDF;
import com.parse.Parse.entity.*;
import com.parse.Parse.parse.*;
import com.parse.Parse.parseTimer.StartParseATB;
import com.parse.Parse.parseTimer.StartParseSilpo;
import com.parse.Parse.recognizeImage.ConnectToApi;
import com.parse.Parse.repository.*;
import com.parse.Parse.service.CategoryListProductService;
import com.parse.Parse.service.CategoryProductService;
import com.parse.Parse.service.ProductService;
import com.parse.Parse.service.UserService;
import com.parse.Parse.thread.ParseThread;
import com.parse.Parse.thread.StartTask;
/*import org.bytedeco.javacpp.*;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.tesseract.TessBaseAPI;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;*/
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
/*import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;*/

import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*import static javax.swing.text.StyleConstants.Size;
import static org.bytedeco.leptonica.global.lept.pixRead;*/


@Controller
public class ParseController {

    @Autowired
    @Qualifier("ATB")
    private Parse parseATB;

    @Autowired
    @Qualifier("Furshet")
    private Parse parseFurshet;

    @Autowired
    @Qualifier("Silpo")
    private ParseJson parseSilpo;

    @Autowired
    @Qualifier("Auchan")
    private Parse parseAuchan;


    @Autowired
    private ProductService productService;


    @Autowired
    @Qualifier("productRepository")
    private ProductRepository productRepository;


    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;


    @Autowired
    @Qualifier("companyRepository")
    private CompanyRepository companyRepository;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext applicationContext;


    @Autowired
    private ApplicationContext companyParse;

    @Autowired
    private StartTask startTask;


    @Autowired
    private StartParseATB startParseATB;

    @Autowired
    private StartParseSilpo startParseSilpo;

    @Autowired
    private ShopATB shopATB;

    @Autowired
    private ShopSilpo shopSilpo;

    @Autowired
    private ShopAuchan shopAuchan;

    @Autowired
    private ShopFurshet shopFurshet;

    @Autowired
    private ShopVarus shopVarus;

    @Autowired
    private ShopKishenya shopKishenya;

    @Autowired
    private ShopReserved shopReserved;

    /*@Autowired
    private ProductBot productBot;*/

    //@Autowired
    //private OffsetLimitRequest offsetLimitRequest;


    @Autowired
    private CategoryProductService categoryProductService;

    @Autowired
    private CategoryListProductService categoryListProductService;

    @Autowired
    @Qualifier("categoryRequestRepository")
    private CategoryRequestRepository categoryRequestRepository;

    @Autowired
    @Qualifier("categoryProductRepository")
    private CategoryProductRepository categoryProductRepository;


    @Autowired
    @Qualifier("categoryListProductRepository")
    private CategoryListProductRepository categoryListProductRepository;

    @Autowired
    @Qualifier("Kishenya")
    private ParseKishenya parseImagePocket;

    @Autowired
    @Qualifier("Watsons")
    private ParseWatsons parseWatsons;

    @Autowired
    @Qualifier("Eva")
    private ParseEva parseEva;

    @Autowired
    @Qualifier("Reserved")
    private ParseReserved parseReserved;

    @Autowired
    @Qualifier("Сhervonyi")
    private ParseСhervonyi parseСhervonyi;

    @Autowired
    @Qualifier("Kolo")
    private ParseKolo parseKolo;

    @Autowired
    @Qualifier("ShopKolo")
    private ShopKolo shopKolo;

    @Autowired
    @Qualifier("ShopPchelkamarket")
    private ShopPchelkamarket shopPchelkamarket;

    @Autowired
    @Qualifier("ShopFora")
    private ShopFora shopFora;

    @Autowired
    @Qualifier("ShopBilla")
    private ShopBilla shopBilla;

    @Autowired
    @Qualifier("ShopVelmart")
    private ShopVelmart shopVelmart;

    @Autowired
    @Qualifier("ShopEkoMarket")
    private ShopEko shopEko;

    @Autowired
    @Qualifier("ShopEva")
    private ShopEva shopEva;

    @Autowired
    @Qualifier("ShopСhervonyi")
    private ShopСhervonyi shopСhervonyi;
    //ShopMetro

    @Autowired
    @Qualifier("ShopMetro")
    private ShopMetro shopMetro;


    @Autowired
    @Qualifier("ShopAdsPDF")
    private ShopAdsPDF shopAdsPDF;

    @Autowired
    @Qualifier("MakePdfFile")
    private MakePdfFile makePdfFile;

    /*@Autowired
    private RecognizeImage recognizeImage;*/

    @Autowired
    private ConnectToApi connectToApi;


    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);


    @GetMapping("/test")
    public String test() {

        shopAdsPDF.parsePDF();

        //shopKishenya.parse();


        //parseReserved.connect(0,null);
        //parseReserved.getInform();

        //productService.saveProduct(parseReserved.getInform());

        //parseKolo.connectJson();
        //parseKolo.getInform();


        //shopMetro.parse();

        //parseСhervonyi.getInform();

        //productService.saveProduct(parseСhervonyi.getInform());
       //List<Product> test = productService.findAllByNameContainingIgnoreCase("Масло",Sort.by("price"));


       //System.out.println(test.get);
        /*for(Product qqq:test){
            System.out.println(qqq.getName());
        }*/


        ///https://www.atbmarket.com/map/get-region?id=3



        /*JsonObject arrayProductATB;
        CloseableHttpClient httpClient = HttpClients.createDefault();


        try {

            URIBuilder builder = new URIBuilder("https://www.atbmarket.com/map/get-region?id=3");
            builder.setParameter("id", "3");

            HttpGet httpGET = new HttpGet(builder.build());

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(httpGET,responseHandler);

            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();

            arrayProductATB = jsonObject.getAsJsonObject("shops").getAsJsonObject("6");


            //System.out.println(arrayProductATB.getAsJsonObject("6").getAsJsonObject("34"));

           // System.out.println(arrayProductATB.entrySet());


           /*for(Map.Entry<String, JsonElement> entry:arrayProductATB.entrySet()){



               JsonObject ttt = entry.getValue().getAsJsonObject();

               System.out.println( ttt.get("address")+" | "+ttt.get("longitude")+" | "+ttt.get("latitude"));
               //System.out.println( ttt.get("longitude"));
               //System.out.println( ttt.get("latitude"));

           }*/





            /*String[] array = arrayProduct.get(0).toString().split(",");

            String lat = Pattern.compile(
                    ".*?\"geometry\"\\:\\{.*?lat.*?:([\\d.]+).*?lng.*?}.*",
                    Pattern.DOTALL)
                    .matcher(arrayProduct.get(0).toString()).replaceAll("$1");

            String lng = Pattern.compile(
                    ".*?\"geometry\"\\:\\{.*?lat.*?:.*?lng.*?([\\d.]+)}.*",
                    Pattern.DOTALL)
                    .matcher(arrayProduct.get(0).toString()).replaceAll("$1");

            System.out.println("lat "+lat);
            System.out.println("lng "+lng);*/


        /*} catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }*/














        /*JsonArray arrayProduct;
        CloseableHttpClient httpClient = HttpClients.createDefault();


        try {

            URIBuilder builder = new URIBuilder("https://api.opencagedata.com/geocode/v1/json");
            builder.setParameter("q", "Євгенія Харченка, 39, Kyiv, Київ, Ukraine")
                    .setParameter("key", "641c51bed8ab490184632ad8526e29ad")
                    .setParameter("language", "en")
                    .setParameter("no_annotations", "1");

            HttpGet httpGET = new HttpGet(builder.build());

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpClient.execute(httpGET,responseHandler);

            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();

            arrayProduct = jsonObject.getAsJsonArray("results").getAsJsonArray();


            String[] array = arrayProduct.get(0).toString().split(",");

            String lat = Pattern.compile(
                    ".*?\"geometry\"\\:\\{.*?lat.*?:([\\d.]+).*?lng.*?}.*",
                    Pattern.DOTALL)
                    .matcher(arrayProduct.get(0).toString()).replaceAll("$1");

            String lng = Pattern.compile(
                    ".*?\"geometry\"\\:\\{.*?lat.*?:.*?lng.*?([\\d.]+)}.*",
                    Pattern.DOTALL)
                    .matcher(arrayProduct.get(0).toString()).replaceAll("$1");

System.out.println("lat "+lat);
System.out.println("lng "+lng);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }*/







       /* try {
            botsApi.registerBot(productBot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }*/

       // parseATB.getInform();

        //connectToApi.getToken();

       // productService.findGeneralReguestProduct("Серветки|Підгузники|Сік|дитяча|дитячий|дитини|дитяче|Суміш|Каша|Пюре|Каша",true,Sort.by("name"));

        return null;

    }

    @GetMapping("/general-reguest")
    public ResponseEntity<?> general(Model theModel, @RequestParam(name = "category", required = true, defaultValue = "all") String category) {
        AjaxResponseBody result = new AjaxResponseBody();
        //@GetMapping("/sortCategory")
        //public ResponseEntity<?> sortCategory(Model theModel, @RequestParam(name = "category", required = true, defaultValue = "all") String category) {
        CategoryRequest categoryRequest = categoryRequestRepository.findFirstByName(category);



      //  System.out.println(productService.findGeneralReguestProduct(categoryRequest.getRequest(),true,Sort.by("name")));

        result.setResult(productService.findGeneralReguestProduct(categoryRequest.getRequest(),true,Sort.by("name")));



        return ResponseEntity.ok(result);

    }


    @GetMapping("/")
    public String main() {

        return "redirect:/search";

    }


    @GetMapping("admin/save")
    public String save() {

        ParseThread ParseThread = applicationContext.getBean(ParseThread.class);
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(ParseThread, 10, TimeUnit.SECONDS);

        return "redirect:/admin/setting-process";
    }


    @GetMapping("/update")
    public void update() {


        productService.updateProduct(parseATB.getInform());
        //productService.updateProduct(parseATB);


    }


    Product findFirstByNameAndCompany(String name, String company) {

        Product findByName = productRepository.
                findFirstByNameAndCompany(name, company);

        if (findByName == null) {
            return null;
        } else {
            return findByName;
        }

    }

    @GetMapping("/check")
    public void check() {


        Product updateProduct;
        Product findByName;

        EntityProduct testProduct =new EntityProduct(1,"Дезодорант-стік Nivea Fresh «Ефект пудри» антиперспірант",169,119.99,new Date(2010,10,20),null,"Silpo","http://simpleicon.com/wp-content/uploads/basket-41.png");

        findByName = findFirstByNameAndCompany("Дезодорант-стік Nivea Fresh «Ефект пудри» антиперспірант", "Silpo");

        System.out.println(findByName.getName());
        System.out.println(findByName.getPeriod());
        System.out.println(findByName.getPrice());
        System.out.println(findByName.getOldPrice());
        System.out.println(findByName.getImage());
        System.out.println("UPDATED!!!!!!!!!UPDATED!!!!!!");
        System.out.println(testProduct.getName());
        System.out.println(testProduct.getPeriod());
        System.out.println(testProduct.getPrice());
        System.out.println(testProduct.getOldPrice());
        System.out.println(testProduct.getImage());

        //if (findByName.getImage().equals("http://simpleicon.com/wp-content/uploads/basket-4.png")){
        //    System.out.println(findByName.getName());}

        if (findByName.getPeriod()!=testProduct.getPeriod() || findByName.getPrice() != testProduct.getPrice() || !findByName.getImage().equals(testProduct.getImage())) {
        //if (findByName != null && (!findByName.getPeriod().equals(testProduct.getPeriod()) || findByName.getPrice() != testProduct.getPrice() || !findByName.getImage().equals(testProduct.getImage()))) {
            // if (findByName.getImage().equals("http://simpleicon.com/wp-content/uploads/basket-4.png")){
            //     System.out.println(object.getName()+ "| Not updated!!!!!!");}
            //updateProduct = productRepository.getOne(findByName.getId());



            System.out.println("222222222222222222222222222222222222");

            /*updateProduct = productRepository.findFirstById(findByName.getId());
            System.out.println("UPDATED!!!!!!!!!UPDATED!!!!!!");
            updateProduct.setPrice(testProduct.getPrice());
            updateProduct.setPeriod(testProduct.getPeriod());
            updateProduct.setImage(testProduct.getImage());
            updateProduct.setDateUpdate(new Date());

            System.out.println(updateProduct.getName());
            System.out.println(updateProduct.getImage());*/



            //productRepository.save(updateProduct);
            //System.out.println("UPDATED!!!!!!!!!UPDATED!!!!!!");
            //relevanceService.updateProposition(findByName);
            //System.out.println("UPDATED!!!!!!!!!UPDATED!!!!!!");
        }


    }

    @PostMapping("/search/products")
    public ResponseEntity<?> searchProducts(@Valid @RequestBody SearchCriteria search, BindingResult bindingResult, Errors errors) {

        AjaxResponseBody result = new AjaxResponseBody();

        int page = 0;

        if (search.getCompany().equals("All")) {
            search.setCompany("");
        }


        if (!search.getName().isEmpty() && search.getCompany().isEmpty()) {
            //List<Product> messages = productService.findAllByNameContainingIgnoreCase(search.getName(), Sort.by("price"));
            List<Product> messages = productService.findAllByNameContainingIgnoreCaseAndVisible(search.getName(), true, Sort.by("price"));
            if (messages.isEmpty()) {
                result.setMsg("no product found!");
            } else {
                result.setMsg("success");
            }
            result.setResult(messages);
        }


        if (!search.getName().isEmpty() && !search.getCompany().isEmpty()) {
            List<Product> messages = productService.findAllByNameContainingIgnoreCaseAndCompanyAndVisible(search.getName(), search.getCompany(), true, Sort.by("price"));
            if (messages.isEmpty()) {
                result.setMsg("no product found!");
            } else {
                result.setMsg("success");
            }
            result.setResult(messages);

        }

        if (search.getName().isEmpty() && !search.getCompany().isEmpty()) {

            //List<Product> theProductListWithCompany = productService.findAllByCompany(search.getCompany());
            List<Product> theProductListWithCompany = productService.findAllByCompanyAndVisible(search.getCompany(), true);

            //List<Product> theProductListWithCompany = productRepository.findAll(Sort.by("company"));

            PaginateRequest paginateRequest = new PaginateRequest(page, 32, theProductListWithCompany);


            Pageable pageable = new PageRequest(page, 32, Sort.by("price"));
            //Page<Product> companyProduct = productRepository.findAllByCompany(search.getCompany(),pageable);
            Page<Product> companyProduct = productRepository.findAllByCompanyAndVisible(search.getCompany(), true, pageable);


            result.setCompany(search.getCompany());
            result.setTotalPage(paginateRequest.countPageProcess());


            if (companyProduct.isEmpty()) {
                result.setMsg("no product found!");
            } else {
                result.setMsg("success");
            }
            result.setPageResult(companyProduct);

        }

        if (search.getName().isEmpty() && search.getCompany().isEmpty()) {

            //List<Product> allProductCount = productService.findAllByCompany(search.getCompany());
            //List<Product> allProductCount = productService.findAllByCompanyAndVisible(search.getCompany(),true);
            List<Product> allProductCount = productRepository.findAllByVisible(true);

            Pageable pageable = new PageRequest(page, 32, Sort.by("company"));
            //Page<Product> allProduct = productRepository.findAll(pageable);
            Page<Product> allProduct = productRepository.findAllByVisible(true, pageable);

            PaginateRequest paginateRequest = new PaginateRequest(page, 32, allProductCount);

            result.setCompany(search.getCompany());
            result.setTotalPage(paginateRequest.countPageProcess());

            result.setPageResult(allProduct);

        }

        return ResponseEntity.ok(result);

    }


    @GetMapping("/list/product")
    public String listProducts(Model theModel) {
        List<Product> theMessage = productRepository.findAll();

        theModel.addAttribute("message", theMessage);


        return "productList";
    }


    @GetMapping("/search/ajax")
    public ResponseEntity<?> searchAjax(Model theModel, @RequestParam(name = "page", required = false, defaultValue = "0") int page, @RequestParam(name = "company", required = false, defaultValue = "All") String company) {


        AjaxResponseBody result = new AjaxResponseBody();

        if (company.equals("All")) {

            Pageable pageable = new PageRequest(page - 1, 32, Sort.by("price"));
            Page<Product> allProduct = productRepository.findAllByVisible(true, pageable);


            result.setPageResult(allProduct);

        } else {

            Pageable pageable = new PageRequest(page - 1, 32, Sort.by("price"));
            Page<Product> testProduct = productRepository.findAllByCompanyAndVisible(company, true, pageable);

            result.setPageResult(testProduct);

        }


        return ResponseEntity.ok(result);
    }


    @GetMapping("/search")
    public String search(Model theModel, @RequestParam(name = "page", required = false, defaultValue = "0") int page) {

        HashMap<String, List<CategoryListProduct>> array = new HashMap<>();

        for (int i = 1; i <= categoryProductService.listCategory().size(); i++) {

            array.put(categoryProductRepository.getOne(i).getNameCategory(), categoryListProductService.listProductFromCategory(categoryProductRepository.getOne(i)));

        }

        HashMap<String, List<CategoryListProduct>> theListCategory = array;


        List<CategoryProduct> theCategory = categoryProductRepository.findAll();
        List<Company> theCompanyList = companyRepository.findAll();
        List<Product> theProductList = productRepository.findAllByVisible(true);

        PaginateRequest paginateRequest = new PaginateRequest(page, 32, theProductList);


        Pageable pageable = new PageRequest(page, 32, Sort.by("company"));
        Page<Product> testProduct = productRepository.findAllByVisible(true, pageable);


        theModel.addAttribute("totalPageCount", paginateRequest.countPageProcess());
        theModel.addAttribute("companyList", theCompanyList);
        theModel.addAttribute("productList", testProduct);
        theModel.addAttribute("category", theCategory);
        theModel.addAttribute("listCategory", theListCategory);

        return "searchProducts";
    }


    @GetMapping("/admin/process")
    public String adminParseList(Model theModel) {

        List<ParseProcess> theProcessParse = parseProcessRepository.findAll();

        theModel.addAttribute("processParse", theProcessParse);

        return "admin";
    }


    @GetMapping("/admin/setting-process")
    public String adminsSettingProcess(Model theModel) {

        List<Company> theSettingProcess = companyRepository.findAll(Sort.by("id"));

        theModel.addAttribute("settingProcess", theSettingProcess);

        return "settingProcess";
    }


    @PostMapping("/admin/saveProcess")
    public String saveMessage(@Valid Company company, BindingResult bindingResult) {

        Company companyResult = companyRepository.findAllByName(company.getName());
        // validateProcessValue.validate(theCompany, bindingResult);

        if (bindingResult.hasErrors()) {

            company.setStartTime(companyResult.getStartTime());

            return "editSettingProcess";
        }

        companyResult.setStartTime(company.getStartTime());
        companyResult.setParse(company.getParse());

        companyRepository.save(companyResult);


        return "redirect:/admin/setting-process";
    }


    @Autowired
    private UserService userService;

    // @Autowired
    // private SecurityService securityService;

    //@Autowired
    //private UserValidator userValidator;


    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new Users());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") Users userForm, BindingResult bindingResult) {
        //userValidator.validate(userForm, bindingResult);

        System.out.println(userForm);

        System.out.println(bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        //  securityService.autoLogin(userForm.getName(), userForm.getPassword());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");


        System.out.println(model);

        return "login";
    }


    @GetMapping("/sortCategory")
    public ResponseEntity<?> sortCategory(Model theModel, @RequestParam(name = "category", required = true, defaultValue = "all") String category) {

        AjaxResponseBody result = new AjaxResponseBody();

        if (category == "all") {

            Pageable pageable = new PageRequest(0, 32, Sort.by("company"));
            Page<Product> testProduct = productRepository.findAll(pageable);

            result.setPageResult(testProduct);

        } else {

            //List<Product> listProduct = productService.findAllByNameContainingIgnoreCase(category, Sort.by("price"));
            List<Product> listProduct = productService.findAllByNameContainingIgnoreCaseAndVisible(category, true, Sort.by("price"));
            CategoryListProduct productInfrom = categoryListProductRepository.findFirstByName(category);
            ArrayList<Product> arraySearchProduct = new ArrayList<>();

            for (Product product : listProduct) {

                Matcher matchName = Pattern.compile(
                        ".*?(" + productInfrom.getNotMatch() + ").*?", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE).matcher(product.getName());

                if (!matchName.matches()) {

                    arraySearchProduct.add(product);

                }

            }

            result.setResult(arraySearchProduct);
        }


        return ResponseEntity.ok(result);
    }

}
