package com.parse.Parse.controller;

import com.parse.Parse.entity.Company;
import com.parse.Parse.entity.Product;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {


    @Qualifier("companyRepository")
    @Autowired
    private CompanyRepository companyRepository;

    @Qualifier("productRepository")
    @Autowired
    private ProductRepository productRepository;

    @CrossOrigin
    @GetMapping(path = "/listProduct", produces = "application/json")
    public ResponseEntity<Page<Product>> greeting() {
    //public ResponseEntity<List<Company>> greeting() {
        //return new Greeting(counter.incrementAndGet(), String.format(template, name));
        //return companyRepository.findAll();

        //return ResponseEntity.ok().body(companyRepository.findAll());
        return ResponseEntity.ok().body(productRepository.findAll(new PageRequest(1, 10)));
    }

    @CrossOrigin
    @GetMapping(path = "/product/{id}", produces = "application/json")
    public ResponseEntity<Product> getProductById(@PathVariable Integer id) {
        //public ResponseEntity<List<Company>> greeting() {
        //return new Greeting(counter.incrementAndGet(), String.format(template, name));
        //return companyRepository.findAll();

        //return ResponseEntity.ok().body(companyRepository.findAll());
        return ResponseEntity.ok().body(productRepository.findFirstById(id));
    }
}
//23038