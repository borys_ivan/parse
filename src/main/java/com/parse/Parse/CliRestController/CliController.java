package com.parse.Parse.CliRestController;


import com.parse.Parse.entity.Company;
import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.parse.AbstractParser;
import com.parse.Parse.repository.CompanyRepository;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.service.ProductService;
import com.parse.Parse.system.Restart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.ArrayList;
import java.util.List;


@ShellComponent
public class CliController {


    @Qualifier("parseProcessRepository")
    @Autowired
    private ParseProcessRepository parseProcessRepository;

    @Qualifier("companyRepository")
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ApplicationContext companyParse;

    @Autowired
    private ProductService productService;

    @Autowired
    private Restart restart;


    private static final Logger LOGGER = LoggerFactory.getLogger(CliController.class);


    @ShellMethod("Parsing products from market")
    public String parseCompanyProducts(@ShellOption({"-C", "--company"}) String company) {

        Company findCompany =  companyRepository.findAllByName(company);

        if(findCompany != null) {

            AbstractParser com = (AbstractParser) companyParse.getBean(company);

            LOGGER.info("Parsing company: " + company);

            ArrayList<EntityProduct> parsedProducts;

            parsedProducts = com.getInform();

            LOGGER.info(productService.saveProduct(parsedProducts));
            productService.checkProduct(parsedProducts, company);

            /*update price and img src in product*/
            productService.updateProduct(parsedProducts);

            return "Parsed products from :" + company;

        } else {

            return "I don't have this company in db :"+ company;

        }

    }

    @ShellMethod("Restart server")
    public String restart() {

        restart.restartApp();

       return "I am going restart application right now";
    }


    @ShellMethod("Restart server")
    public String showCompany() {

        List<Company> allCompany =  companyRepository.findAll(Sort.by("id"));


        String format = "|%1$-4s|%2$-10s|%3$-30s|%4$-5s|\n";

        StringBuilder table= new StringBuilder();

        for(Company company:allCompany){

            table.append(String.format(format, company.getId(), company.getName(), company.getStartTime(), company.getParse()));
        }


        return table.toString();
    }
}
