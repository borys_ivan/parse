package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

@Service("Watsons")
public class ParseWatsons extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Watsons", new Date(), true, null);


    @Override
    public List<Document> connect(int count, List<Document> documents) {

        Document docCustomConn;
        String Url = "https://www.watsons.ua/ru/aktsii/vse-nominanty-hwb/c/hwb?page=1&startPage=0";
        List<Document> page = new ArrayList<Document>();
        Elements pageCount;


        try {

            docCustomConn = connectSetting(Url).get();

            if (docCustomConn.select(".row").hasText()) {
             //   System.out.println(docCustomConn.select(".row"));
                //page.add(docCustomConn);
            }

            pageCount = docCustomConn.select(".pagination__item.js-pagination-item.pagination__item > .pagination__text");

            LinkedList<Element> listCount = new LinkedList<Element>(pageCount);


           //  do {

           // for (int countP = 0; countP <= Integer.parseInt(listCount.getLast().getElementsByClass("pagination__text").text()); countP++) {
            for (int countP = 0; countP <= 5; countP++) {

                docCustomConn = connectSetting("https://www.watsons.ua/ru/aktsii/vse-nominanty-hwb/c/hwb?page="+countP+"&startPage=0").get();

                LOGGER.info("parsing page: " + countP + " total: " + listCount.getLast());

                Thread.sleep(6000);

                page.add(docCustomConn);

            }


            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);

            }


            return page;

        } catch (HttpStatusException ex) {


            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("SocketTimeoutException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("IOException");

            return null;

        } catch (InterruptedException e) {
            e.printStackTrace();

            return null;
        }


    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        List<Document> docCustomConn = connect(0,null);

        if (docCustomConn == null) {
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }


        String fixPrice,fixPriceSemicolon,fixOldPriceSemicolon = "";
        Elements getName = null;
        String fixOldPrice = null;
        Elements getPrice = null;
        Elements getOldPrice = null;
        Elements getImage = null;

        List<Elements> arrayName =new ArrayList<>();
        List<Elements> arrayPrice =new ArrayList<Elements>();
        List<Elements> arrayOldPrice =new ArrayList<Elements>();
        List<Elements> arrayImage =new ArrayList<Elements>();

        String price;
        String oldPrice;
        String image;

        for (Document page : docCustomConn) {

            getName = page.body().select("div.product-tile__horizontal-info-container > div.product-tile__product-name");
            getPrice = page.body().select("div.product-tile__horizontal-info-container > div.product-tile__price-rating-block > div.product-tile__price-wrapper");
            getOldPrice = page.body().select("div.product-tile__horizontal-info-container > div.product-tile__price-rating-block > div.product-tile__price-wrapper");
            getImage = page.body().select("div.product-tile__content > a.product-tile__product-link.js-product-tile-link");

            arrayName.add(getName);
            arrayPrice.add(getPrice);
            arrayOldPrice.add(getOldPrice);
            arrayImage.add(getImage);

        }


        ArrayList<EntityProduct> array = new ArrayList<>();

        if (getName.size() == getPrice.size()) {
            state.setState(true);
            state.setError(null);
            parseProcessRepository.save(state);
        } else {
            state.setState(false);
            state.setError("no equally value");
            parseProcessRepository.save(state);
        }

        try {

            for (int a = 0; a <= arrayName.size()-1; a++) {


                for (int i = 0; i <= arrayName.get(a).size()-1; i++) {


                    if(arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--discounted js-variant-markdown-price").hasText()) {

                        price = arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--discounted js-variant-markdown-price").text();

                    }else if(arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--original js-variant-price").hasText()){

                        price = arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--original js-variant-price").text();

                    } else if(arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--tpr js-variant-markdown-price").hasText()) {

                        price = arrayPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--tpr js-variant-markdown-price").text();

                    }else{

                        price = "0.0";
                    }


                    if(arrayOldPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--old js-variant-old-price ").hasText()) {

                        oldPrice = arrayOldPrice.get(a).get(i).getElementsByClass("product-tile__price product-tile__price--old js-variant-old-price ").text();

                    } else {
                        oldPrice = "0.0";
                    }

                    fixPrice = Pattern.compile(
                            "\\sгрн",
                            Pattern.DOTALL)
                            .matcher(price).replaceAll("");


                    fixPriceSemicolon = Pattern.compile(
                            ",",
                            Pattern.DOTALL)
                            .matcher(fixPrice.replaceAll(" ", "")).replaceAll(".");


                    fixOldPrice = Pattern.compile(
                            "\\sгрн",
                            Pattern.DOTALL)
                            .matcher(oldPrice).replaceAll("");

                    fixOldPriceSemicolon = Pattern.compile(
                            ",",
                            Pattern.DOTALL)
                            .matcher(fixOldPrice.replaceAll(" ", "")).replaceAll(".");

                    image = "https://www.watsons.ua/"+arrayImage.get(a).get(i).getElementsByClass("js-img-lazy-default product-tile__product-img js-variant-image ").attr("data-src");

                    array.add(new EntityProduct(i, arrayName.get(a).get(i).getElementsByTag("a").text(), Double.valueOf(fixPriceSemicolon), Double.valueOf(fixOldPriceSemicolon),
                            new Date(), null, "Watsons", image));

                }

            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            System.out.println("go out from array in during parse market, please check value");
        }

        System.out.println("Total : "+array.size());

        return array;
    }

}
