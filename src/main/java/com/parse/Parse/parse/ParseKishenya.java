package com.parse.Parse.parse;

/*Велика кишення*/

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/*import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;*/
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;


@Service("Kishenya")
public class ParseKishenya extends AbstractParserImage {
    @Override
    public void connectImage() {

        ArrayList<String> arrayLink = new ArrayList<String>();

        String strImageURL;
        Elements imageElements;
        Document document = null;

        //replace it with your URL
        arrayLink.addAll(Arrays.asList("http://kishenya.ua/ua/tovar-tyzhnia.html"));

        try {

            for (String strURL : arrayLink) {

                //connect to the website and get the document

                document = connectSetting(strURL).get();


                //select all img tags
                imageElements = document.select("p > img");

                //iterate over each image
                for (Element imageElement : imageElements) {

                    //make sure to get the absolute URL using abs: prefix
                    strImageURL = imageElement.attr("abs:src");
                    //download image one by one
                    saveImage(strImageURL);
                }

                System.out.println("The image is successfully parsed");

            }

        } catch (HttpStatusException c) {
            System.out.println("check server or link");
        } catch (SocketTimeoutException t) {
            System.out.println("long time connection to website");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void saveImage(String strImageURL) {

        //get file name from image path
        String strImageName = strImageURL.substring(strImageURL.lastIndexOf("/") + 1);
        System.out.println("SaveImage: " + strImageName);

        Path createdFolder = Paths.get("src/main/resources/image");

        if (!Files.exists(createdFolder)) {

            // try {
            //Files.createDirectory(createdFolder);

            File f = null;
            boolean bool = false;
            f = new File("src/main/resources/image");
            // create directories
            bool = f.mkdirs();
            //} catch (IOException e) {
            //    e.printStackTrace();
            //}

        }


        try {

            //open the stream from URL
            java.net.URL urlImage = new java.net.URL(strImageURL);
            InputStream in = urlImage.openStream();

            String path = new File("src/main/resources/image")
                    .getAbsolutePath();
            OutputStream out = new BufferedOutputStream(new FileOutputStream(path + "/" + strImageName));

            for (int b; (b = in.read()) != -1; ) {
                out.write(b);
            }

            out.close();
            in.close();


            /*nu.pattern.OpenCV.loadShared();
            System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);

            Mat gray = new Mat();
            Mat blur = new Mat();


            Stream<Path> paths = Files.walk(Paths.get(path));
            List<Path> pathsImage =  paths.filter(Files::isRegularFile).collect(Collectors.toList());


            Mat source;

            for(Path pathImage : pathsImage){


                source = Imgcodecs.imread(pathImage.toString());

                Imgproc.cvtColor(source, gray, Imgproc.COLOR_RGB2GRAY, 0);
                Imgproc.GaussianBlur(gray, blur, new org.opencv.core.Size(0, 0), 3);

                Imgcodecs.imwrite(pathImage.toString(), gray);

            }*/

            //System.out.println("The image is successfully changed");


        } catch (FileNotFoundException f) {

            System.out.println("folder not found for save image");

        } catch (IOException e) {

            e.printStackTrace();
        }

    }
}
