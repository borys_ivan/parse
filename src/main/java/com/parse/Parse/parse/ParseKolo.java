package com.parse.Parse.parse;


import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("Kolo")
public class ParseKolo extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Kolo", new Date(), true, null);

    @Override
    public List<Document> connect(int count, List<Document> documents) {

        List<Document> page = new ArrayList<Document>();
        String Url = "https://kolomarket.com.ua/ru/promo/cini-znizheno/";

        try {

            page.add(connectSetting(Url).get());

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);
            }

            return page;

        } catch (HttpStatusException ex) {

            LOGGER.info("Page not found");

            state.setError("HttpStatusException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            state.setError("IOException");
            return null;
        }

    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        List<Document> docCustomConn = new ArrayList<>();

        docCustomConn.add(connect(0,null).get(0));
        docCustomConn.addAll(additionalProduct());


        if (docCustomConn.isEmpty()) {
            parseProcessRepository.save(state);
            return null;
        }

        ArrayList<EntityProduct> array = new ArrayList<>();

        String getName, getPriceInteger, getPriceCent,getOldPriceInteger,getOldPriceCent, getImage = null;
        double getPrice,getOldPrice = 0.0;
        int count = 0;

        for (Document test :docCustomConn) {

            Elements general = test.select("div.product-card-container div.product-card.yellow");


            for (Element product :general) {

                getName = product.select("div.title-container div.title").text();
                getImage = product.select("div.illustration-container img").attr("data-src");

                getPriceInteger = product.select("div.price-container div.price span.integer").text();
                getPriceCent = product.select("div.price-container div.price span.cent").text();
                getPrice = Double.valueOf(getPriceInteger + "." + getPriceCent);


                if(product.select("div.price-container div.old-price span.integer").hasText()) {
                    getOldPriceInteger = product.select("div.price-container div.old-price span.integer").text();
                    getOldPriceCent = product.select("div.price-container div.old-price span.cent").text();
                    getOldPrice = Double.valueOf(getOldPriceInteger + "." + getOldPriceCent);
                } else {
                    getOldPrice = 0.0;
                }


                if (product.select("div.title-container div.title").hasText() && product.select("div.price-container div.price span.integer").hasText()) {

                    array.add(new EntityProduct(count, getName, getPrice, getOldPrice,
                            new Date(), null, "Kolo", getImage));
System.out.println(array.get(count));
                    count++;

                }

            }
        }


        System.out.println("Kolo-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");


        return array;
    }


    public List<Document> additionalProduct() {

        List<Document> arrayProduct = new ArrayList<>();

        try {

            boolean pageContent = false ;
            int count = 0;

            while (!pageContent){

                Thread.sleep(7000);

                String jsonBody = "{\"postType\": \"yellow-price-tag\", \"page\": "+count+", \"postsPerPage\": 12}";

                String url = "https://kolomarket.com.ua/wp-content/themes/kolo/ajax/get-posts-items.php";

                Connection.Response execute = Jsoup.connect(url)
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
                        .followRedirects(true)
                        .ignoreHttpErrors(true)
                        .ignoreContentType(true)
                        .userAgent("Mozilla/5.0 AppleWebKit/537.36 (KHTML," +
                                " like Gecko) Chrome/45.0.2454.4 Safari/537.36")
                        .method(Connection.Method.POST)
                        .requestBody(jsonBody)
                        .maxBodySize(1_000_000 * 30) // 30 mb ~
                        .timeout(0) // infinite timeout
                        .execute();

                pageContent = execute.body().isEmpty();

                if (!execute.body().isEmpty()) {
                    arrayProduct.add(execute.parse());

                }

                count++;
            }

            return arrayProduct;

        } catch (IOException e) {

            e.printStackTrace();
            //LOGGER.info("time out for website");

            state.setState(false);
            state.setError("IOException");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
