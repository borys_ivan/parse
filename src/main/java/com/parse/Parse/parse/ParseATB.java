package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service("ATB")
public class ParseATB extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("ATB", new Date(), true, null);

    @Override
    public List<Document> connect(int count,List<Document> documents) {

        List<Document> page = new ArrayList<Document>();
        String Url = "https://www.atbmarket.com/hot/akcii/economy";

        try {

            page.add(connectSetting(Url).get());

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);
            }

            return page;

        } catch (HttpStatusException ex) {

            LOGGER.info("Page not found");

            state.setError("HttpStatusException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            state.setError("IOException");
            return null;
        }

    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        Document docCustomConn = connect(0,null).get(0);

        if (docCustomConn == null) {
            parseProcessRepository.save(state);
            return null;
        }

        Elements getName = docCustomConn.select("li > div.promo_info > span.promo_info_text");

        String fixName = Pattern.compile(
                "(<span class=\"promo_info_text\">\\s?|<span>\\s?|</span>)",
                Pattern.DOTALL)
                .matcher(getName.toString()).replaceAll("");

        Elements getPrice = docCustomConn.select("li > div.promo_info > " +
                "div.price_box.small_box.red_box.floated_right > div.promo_price");

        String fixPrice = Pattern.compile(
                "<div class=\"promo_price\">.*?([\\d]+).*?<span>([\\d]+)</span>.*?<span.*?</span>.*?</div>",
                Pattern.DOTALL)
                .matcher(getPrice.toString()).replaceAll("$1.$2");

        Elements getOldPrice = docCustomConn.select("li > div.promo_info > " +
                "div.price_box.small_box.red_box.floated_right > span.promo_old_price");

        String deletebadPrice = Pattern.compile(
                "<span class=\"promo_old_price\"></span>",
                Pattern.DOTALL)
                .matcher(getOldPrice.toString()).replaceAll("null");

        String fixOldPrice = Pattern.compile(
                "<span class=\"promo_old_price\">([\\d.,]+)</span>",
                Pattern.DOTALL)
                .matcher(deletebadPrice).replaceAll("$1");

        Elements image = docCustomConn.select("li > div.promo_image_wrap > " +
                "a.promo_image_link");

        String fixImage = Pattern.compile(
                "<a class.*?img.*?src=\"([\\w\\.\\/]+)\".*?</a>",
                Pattern.DOTALL)
                .matcher(image.toString()).replaceAll("$1");


        String[] arrayName = fixName.split("\\n");
        String[] arrayPrice = fixPrice.split("\\n");
        String[] arrayOldPrice = fixOldPrice.split("\\n");
        String[] arrayImage = fixImage.split("\\n");


        //EntityProduct[] array = new EntityProduct[arrayName.length];
        ArrayList<EntityProduct> array = new ArrayList<>();

        if (arrayName.length == arrayPrice.length) {
            state.setState(true);
            state.setError(null);
            parseProcessRepository.save(state);
        } else {
            state.setState(false);
            state.setError("no equally value");
            parseProcessRepository.save(state);
        }

        for (int i = 0; i < arrayName.length; i++) {

            if (arrayOldPrice[i] == null || arrayOldPrice[i].equals("null")) {
                arrayOldPrice[i] = "0.0";
            }

            //System.out.println(arrayName[i]+"|"+arrayPrice[i]+"|"+arrayOldPrice[i]);

            /*array[i] = new EntityProduct(i, arrayName[i], Double.valueOf(arrayPrice[i]), Double.valueOf(arrayOldPrice[i]),
                    new Date(), null, "ATB","http://www.atbmarket.com/"+arrayImage[i]);*/

            arrayName[i] = arrayName[i].replaceAll("amp;|/","");

            array.add(new EntityProduct(i, arrayName[i], Double.valueOf(arrayPrice[i]), Double.valueOf(arrayOldPrice[i]),
                    new Date(), null, "ATB", "http://www.atbmarket.com/" + arrayImage[i]));

        }


        System.out.println("ATB-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");


        return array;
    }

}

