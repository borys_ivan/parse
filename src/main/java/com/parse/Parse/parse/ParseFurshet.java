package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("Furshet")
public class ParseFurshet extends AbstractParser {

    int count = 0;

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Furshet", new Date(), true, null);

    @Override
    public List<Document> connect(int count,List<Document> documents) {
        Document docCustomConn;
        String Url = "https://furshet.ua/actions?page=";
        List<Document> page = new ArrayList<Document>();
        //int count = 0;

        try {

            do {

                Thread.sleep(3000);

                docCustomConn = connectSetting(Url + count).get();

                if (docCustomConn.select(".desc").hasText()) {
                    page.add(docCustomConn);
                }

                count++;

            } while (docCustomConn.select(".desc").hasText());

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);

            }

            return page;

        } catch (HttpStatusException ex) {


            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("SocketTimeoutException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("IOException");

            return null;

        } catch (InterruptedException e) {
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        List<String> name = new ArrayList<>();
        List<String> price = new ArrayList<>();
        List<String> proposition = new ArrayList<>();
        List<String> oldPrice = new ArrayList<>();
        List<String> image = new ArrayList<>();

        List<Document> docCustomConn = connect(0,null);

        if (docCustomConn == null) {
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }


        Elements getName;
        String fixName;
        Elements getPrice;
        String fixPrice;
        Elements dateProposition;
        String fixProposition;
        Elements getOldPrice;
        String fixDatePrice;
        String fixEmptyOldPrice;
        String fixOldPrice;
        Elements getImage;
        String fixImage;

        Matcher correctOldPrice;
        String correct;

        for (Document page : docCustomConn) {

            getName = page.select(".swiper-slide > .item > .desc");
            fixName = Pattern.compile(
                    "(<div class=\"desc\">\\n?\\s?|</div>\\s?\\n?|(<p>|</p>)|<br>)",
                    Pattern.DOTALL)
                    .matcher(getName.toString()).replaceAll("");


            getPrice = page.select(".item > .actions-list__price > .cost");
            fixPrice = Pattern.compile(
                    "<div class=\"cost\">.*?([\\d]+).*?<sup>([\\d]+)</sup>.*?</div>",
                    Pattern.DOTALL)
                    .matcher(getPrice.toString()).replaceAll("$1.$2");

            dateProposition = page.select(".item > .actions-list__price > .date-cost");
            fixProposition = Pattern.compile(
                    "<div class=\"date-cost\">.*?<span.*?\">([\\d.,]+)</span>.*?\">([\\d.,]+)</span>.*?</div>.*?</div>",
                    Pattern.DOTALL)
                    .matcher(dateProposition.toString()).replaceAll("$1-$2");


            getOldPrice = page.select(".item > .actions-list__price");

            fixDatePrice = Pattern.compile(
                    "<div class=\"actions-list__price\">\\s\\n\\s<div class=\"cost\">" +
                            "\\n\\s?\\s?\\s?[\\d]+\\n\\s?\\s?<sup>[\\d]+</sup>\\s\\n\\s</div>\\s\\n\\s<div class=\"date-cost\">" +
                            ".*?<div>\\n\\s?\\s?\\s?\\s?<span class=\"date-display-single\">.*?</span>\\n\\s?\\s?</div>\\s\\n\\s</div>\\s\\n</div>",
                    Pattern.DOTALL)
                    .matcher(getOldPrice.toString()).replaceAll("null");

            fixEmptyOldPrice = Pattern.compile(
                    "<div class=\"actions-list__price\">\\s\\n\\s<div class=\"cost\">\\n\\s?\\s?\\s?[\\d]+" +
                            "\\n\\s?\\s?<sup>[\\d]+</sup>\\s\\n\\s</div>\\s\\n</div>",
                    Pattern.DOTALL)
                    .matcher(fixDatePrice).replaceAll("null");

            fixOldPrice = Pattern.compile(
                    "<div class=\"actions-list__price\">.*?<div class=\"cost\">.*?" +
                            "<div class=\"del-cost\">.*?([\\d]+(?:[.,,][\\d]{2})?).*?<sup>([\\d]+)?</sup>.*?</div>.*?</div>.*?</div>",
                    Pattern.DOTALL)
                    .matcher(fixEmptyOldPrice).replaceAll("$1.$2");

            correctOldPrice = Pattern.compile(
                    "([\\d]+)[\\,,]([\\d]+)\\.?",
                    Pattern.DOTALL).matcher(fixOldPrice);

            if (correctOldPrice.find()) {
                correct = correctOldPrice.replaceAll("$1.$2");
                fixOldPrice = correct;
            }


            getImage = page.select(".swiper-slide > .item > .img");
            fixImage = Pattern.compile(
                    "<div class=\"img\">.*?src=\"([\\w\\:\\/\\.\\-]+)\".*?alt=\".*?</div>\\s?\\n?(?:</div>)?",
                    Pattern.DOTALL)
                    .matcher(getImage.toString()).replaceAll("$1,");


            name.addAll(Arrays.asList(fixName.split("\\n")));
            price.addAll(Arrays.asList(fixPrice.split("\\n")));
            proposition.addAll(Arrays.asList(fixProposition.split("\\n")));
            oldPrice.addAll(Arrays.asList(fixOldPrice.split("\\n")));
            image.addAll(Arrays.asList(fixImage.split(",\\n|,")));

        }

        //EntityProduct[] array = new EntityProduct[name.size()];
        ArrayList<EntityProduct> array = new ArrayList<>();

        if (name.size() == price.size()) {
            state.setState(true);
            state.setError(null);
            parseProcessRepository.save(state);
        } else {
            state.setState(false);
            state.setError("no equally value");
            parseProcessRepository.save(state);
        }

        try {

            for (int i = 0; i <= name.size() - 1; i++) {

                if (proposition.get(i).isEmpty()) {
                    proposition.add(i, null);
                }

                if (oldPrice.get(i).equals("null")) {
                    oldPrice.set(i, "0.0");
                }


                array.add(new EntityProduct(i, name.get(i), Double.valueOf(price.get(i)), Double.valueOf(oldPrice.get(i)),
                        new Date(), proposition.get(i), "Furshet", image.get(i)));

            }

        } catch (IndexOutOfBoundsException ex) {
            System.out.println("go out from array in during parse market, please check value");
        }


        name.clear();
        price.clear();
        proposition.clear();
        oldPrice.clear();
        image.clear();


        System.out.println("Furchet-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        return array;
    }
}
