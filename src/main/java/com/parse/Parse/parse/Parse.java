package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public interface Parse {

    List<Document> connect(int count,List<Document> documents);

    ArrayList<EntityProduct> getInform();

}
