package com.parse.Parse.parse;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.parse.Parse.entity.EntityProduct;

import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javax.imageio.ImageIO.getCacheDirectory;

@Service("Silpo")
public class ParseSilpo extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Silpo", new Date(), true, null);



    @Override
    public JsonArray connectJson() {

        JsonArray arrayProduct;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://silpo.ua/graphql");

        try {
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("operationName", new StringBody("offers"));
            //reqEntity.addPart("variables", new StringBody("{\"categoryId\":null,\"storeIds\":null,\"pagingInfo\":{\"offset\":0,\"limit\":999999},\"pageSlug\":\"actions\",\"random\":false,\"fetchPolicy\":\"network-only\"}"));
            reqEntity.addPart("variables", new StringBody("{\"categoryId\":null,\"storeIds\":null,\"pagingInfo\":{\"offset\":0,\"limit\":999999},\"pageSlug\":\"actions\",\"random\":true}"));

            //File file = ResourceUtils.getFile("classpath:config/query.txt");
            InputStream inputStream = getClass()
                    .getClassLoader().getResourceAsStream("config/query2.txt");
            String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

            //reqEntity.addPart("query",new StringBody(new String(Files.readAllBytes(file.toPath()))));
            reqEntity.addPart("query",new StringBody(result));
            //reqEntity.addPart("query",new StringBody(new String(Files.readAllBytes(aChar))));

            httppost.setEntity(reqEntity);
            System.out.println("Requesting : " + httppost.getRequestLine());
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);
            JsonObject jsonObject = new JsonParser().parse(StringEscapeUtils.
                    unescapeJava(responseBody)).getAsJsonObject();

             arrayProduct = jsonObject.getAsJsonObject("data").getAsJsonObject("offersSplited").
                    getAsJsonObject("products").getAsJsonArray("items");


            if(arrayProduct.size()==0) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);
            }

            return arrayProduct;

        } catch (IOException e) {

            e.printStackTrace();
            //LOGGER.info("time out for website");

            state.setState(false);
            state.setError("IOException");

        } finally {
            httpclient.getConnectionManager().shutdown();
        }

       return null;
    }

    @Override
    public ArrayList<EntityProduct> getInform() {


        JsonArray array = connectJson();

        int count=0;
        List <String> arrayName = new ArrayList<>();
        List <String> arrayPrice = new ArrayList<>();
        List <String> arrayOldPrice = new ArrayList<>();
        List <String> arrayImage = new ArrayList<>();
        //EntityProduct[] arrayEntity = new EntityProduct[array.size()];
        ArrayList<EntityProduct> arrayEntity = new ArrayList<>();

        if(array.size()==0){
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }


        //for (JsonElement item:array){
        for (JsonElement item:array){

            if(item.getAsJsonObject().get("title")!=null) {
                arrayName.add(item.getAsJsonObject().get("title").toString().replaceAll("\"",""));
                //System.out.println(arrayName.add(item.getAsJsonObject().get("title").toString().replaceAll("\"","")));
            } else {
                arrayName.set(count,null);
            }

            if(item.getAsJsonObject().get("price")!=null) {
                arrayPrice.add(item.getAsJsonObject().get("price").toString().replaceAll("\"",""));
                //System.out.println(item.getAsJsonObject().get("price").toString().replaceAll("\"",""));
            } else {
                arrayPrice.set(count,null);
            }

            //item.getAsJsonObject().get("weight");


            if(item.getAsJsonObject().get("oldPrice")!=null) {
                arrayOldPrice.add(item.getAsJsonObject().get("oldPrice").toString().replaceAll("\"",""));
            } else {
                arrayOldPrice.set(count,null);
            }


            if(item.getAsJsonObject().get("imageUrl")!=null) {
                if(!item.getAsJsonObject().get("imageUrl").isJsonNull()) {
                    arrayImage.add(item.getAsJsonObject().get("imageUrl").toString().replaceAll("\"", ""));
                } else {
                    arrayImage.add("http://simpleicon.com/wp-content/uploads/basket-4.png");
                }
            } else {
                arrayImage.set(count,null);
            }

            /*arrayEntity[count] = new EntityProduct(count, arrayName.get(count), Double.valueOf(arrayPrice.get(count))
                    ,Double.valueOf(arrayOldPrice.get(count)), new Date(), null, "Silpo",arrayImage.get(count));*/
            arrayEntity.add(new EntityProduct(count, arrayName.get(count), Double.valueOf(arrayPrice.get(count))
                    ,Double.valueOf(arrayOldPrice.get(count)), new Date(), null, "Silpo",arrayImage.get(count)));

            count++;

        }

        //if(arrayEntity.length!=0) {
        if(arrayEntity.size()!=0) {

            state.setState(true);
            parseProcessRepository.save(state);
        }

        arrayName.clear();
        arrayPrice.clear();
        arrayOldPrice.clear();
        arrayImage.clear();

        System.out.println("Silpo-----------------------------------------");
        System.out.println("total : "+Runtime.getRuntime().totalMemory());
        System.out.println("free : "+Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        return arrayEntity;
    }
}
