package com.parse.Parse.parse;

import com.google.gson.JsonArray;
import com.parse.Parse.entity.EntityProduct;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractParser implements Parse,ParseJson {

    @Override
    public List<Document> connect(int count,List<Document> documents) {return new ArrayList<>();}

    @Override
    public JsonArray connectJson() {return new JsonArray();}

    @Override
    public ArrayList<EntityProduct> getInform() {return new ArrayList<>(); }

    public Connection connectSetting(String Url) {

        Connection docCustomConn;

        docCustomConn = Jsoup.connect(Url)
                .userAgent("Mozilla")
                .timeout(5000)
                .cookie("cookiename", "val234")
                .cookie("anothercookie", "ilovejsoup")
                .referrer("http://google.com")
                .header("headersecurity", "xyz123");

        return docCustomConn;
    }
}
