package com.parse.Parse.parse;

public abstract class AbstractParserImage extends AbstractParser implements ParseImage {
    @Override
    public void connectImage() {}

    @Override
    public void saveImage(String url){}
}
