package com.parse.Parse.parse;


import java.io.IOException;

public interface ParseImage {

    void connectImage();

    void saveImage(String url) throws IOException;


}
