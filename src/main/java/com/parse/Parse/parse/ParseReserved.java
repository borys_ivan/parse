package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("Reserved")
public class ParseReserved extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Reserved", new Date(), true, null);

    @Override
    public List<Document> connect(int count, List<Document> documents) {
        Document docCustomConn;

        HashMap<String, String> Urls = new HashMap<>();

        Urls.put("woman", "https://www.reserved.com/ua/uk/sale2-ua/woman/bestsellers-ua");
        Urls.put("men", "https://www.reserved.com/ua/uk/sale2-ua/men/bestsellers-ua");
        Urls.put("kids-girl", "https://www.reserved.com/ua/uk/sale2-ua/kids-girl/bestsellers-ua");
        Urls.put("kids-boy", "https://www.reserved.com/ua/uk/sale2-ua/kids-boy/bestsellers-ua");

        List<Document> page = new ArrayList<Document>();

        try {

            for (Map.Entry<String, String> url : Urls.entrySet()) {

                docCustomConn = connectSetting(url.getValue()).get();

                if (docCustomConn.select("section#categoryProducts").hasText()) {
                    page.add(docCustomConn);
                }
            }

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);

            }

            return page;

        } catch (HttpStatusException ex) {


            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("SocketTimeoutException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("IOException");

            return null;

        }


    }


    @Override
    public ArrayList<EntityProduct> getInform() {

        List<Document> docCustomConn = connect(0, null);

        if (docCustomConn == null) {
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }

        Elements getProduct;

        List<String> nameProduct = new ArrayList<>();
        List<String> oldPriceProduct = new ArrayList<>();
        List<String> priceProduct = new ArrayList<>();
        List<String> imageProduct = new ArrayList<>();
        List<String> refLink = new ArrayList<>();
        List<String> periodProduct = new ArrayList<>();

        ArrayList<EntityProduct> array = new ArrayList<>();

        for (Document page : docCustomConn) {

            Elements pageElement = page.select("section#categoryProducts");

            getProduct = pageElement.select(".es-product figure");

            for (Element product : getProduct) {
                nameProduct.add(Pattern.compile(
                        ".*?alt=\"(.*?)\">.*",
                        Pattern.DOTALL)
                        .matcher(product.select("a.es-product-photo img").toString()).replaceAll("$1"));
                imageProduct.add(Pattern.compile(
                        ".*?data-back-src=\"(.*?)\".*",
                        Pattern.DOTALL)
                        .matcher(product.select("a.es-product-photo img").toString()).replaceAll("$1"));


                if (product.select("section.es-product-price p.es-regular-price").hasText()) {
                    oldPriceProduct.add(product.select("section.es-product-price p.es-regular-price").text());
                } else {
                    oldPriceProduct.add("0");
                }

                if (product.select("section.es-product-price p.es-discount-price").hasText()) {
                    priceProduct.add(product.select("section.es-product-price p.es-discount-price").text());
                } else if (product.select("section.es-product-price p.es-final-price").hasText()) {
                    priceProduct.add(product.select("section.es-product-price p.es-final-price").text());
                } else {
                    priceProduct.add("0");
                }

                refLink.add(product.select("a.es-product-photo").attr("href"));

            }

            int count = nameProduct.size();

            int countProductTotal = 0;

            for (int countProduct = 0; countProduct < count; countProduct++) {

                array.add(countProductTotal, new EntityProduct(countProductTotal, nameProduct.get(countProductTotal),
                        Double.valueOf(priceProduct.get(countProductTotal).replaceAll("[ UAH]+", "")),
                        Double.valueOf(oldPriceProduct.get(countProductTotal).replaceAll("[ UAH]+", "")),
                        new Date(), null, "Reserved", imageProduct.get(countProductTotal), null,
                        refLink.get(countProductTotal)));

                countProductTotal++;

            }

            if (nameProduct.size() == priceProduct.size()) {
                state.setState(true);
                state.setError(null);
                parseProcessRepository.save(state);
            } else {
                state.setState(false);
                state.setError("no equally value");
                parseProcessRepository.save(state);
            }

        }

        System.out.println("Reserved-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        return array;
    }

}
