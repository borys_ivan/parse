package com.parse.Parse.parse;

import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service("Eva")
public class ParseEva extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Eva", new Date(), true, null);

    @Override
    public List<Document> connect(int count, List<Document> documents) {

        Document docCustomConn;
        List<Document> page = new ArrayList<Document>();
        Elements pageCount;


        List<String> arrayURL = new ArrayList<>();
        arrayURL.add("https://eva.ua/ua/299-300/dlja-lica/?product_list_dir=asc");
        /*arrayURL.add("https://eva.ua/ua/299-302/dlja-glaz-brovej/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/ua/299-315/dlja-gub/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/ua/299-321/sredstva-uhoda-nogtjami/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/217-324/parfjumerija-zhenskaja/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/217-297/parfjumerija-muzhskaja/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/217-12609/parfjumirovannyj-uhod/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/217-408/parfjumerija-uniseks/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/12536-12540-12541/podguzniki/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/ua/12536-12540-12548/trusiki-podguzniki/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/ua/12536-12537/sredstva-uhoda-malyshami/?product_list_dir=asc");
        arrayURL.add("https://eva.ua/ua/12536-12542/aksessuary-uhodu-malyshami/?product_list_dir=asc");*/

String name=null;
String newPrice=null;
String oldPrice=null;
String image=null;

List<Elements> arrayName= new ArrayList<>();
        List<Elements> arrayNewPrice= new ArrayList<>();
        List<Elements> arrayOldPrice= new ArrayList<>();
        List<Elements> arrayImage= new ArrayList<>();

        try {

            for (String url :arrayURL) {

                for (int i = 1; i<=1;i++) {

                    docCustomConn = connectSetting(url+"&p="+i).get();


                    arrayName.add(docCustomConn.body().getElementsByClass("product name product-item-name"));

                    arrayNewPrice.add(docCustomConn.body().getElementsByClass("price-box price-final_price"));

                    arrayOldPrice.add(docCustomConn.body().getElementsByClass("price-box price-final_price"));

                    arrayImage.add(docCustomConn.body().getElementsByClass("product-image-container"));


                    for (int a = 0; a <= 0; a++) {



                        for (int s = 0; s <= arrayName.get(a).size()-1; s++) {


                            if(arrayName.get(a).get(s).hasText()) {

                                name = arrayName.get(a).get(s).text();
                            }

                            if(arrayNewPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.special-price").hasText()){
                                newPrice=arrayNewPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.special-price").text();
                            }else if(arrayNewPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.price-container.price-final_price.tax.weee").hasText()){
                                newPrice=arrayNewPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.price-container.price-final_price.tax.weee").text();

                            }
                            //      System.out.println(arrayOldPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.old-price.sly-old-price.no-display > span.price-container.price-final_price.tax.weee"));
                            if(arrayOldPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.old-price.sly-old-price.no-display > span.price-container.price-final_price.tax.weee > span.price-wrapper > span.price").hasText()){

                                oldPrice=arrayOldPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.old-price.sly-old-price.no-display > span.price-container.price-final_price.tax.weee > span.price-wrapper > span.price").text();
                            } else {
                                oldPrice=arrayOldPrice.get(a).get(s).getElementsByClass("price-box price-final_price").select("span.price-container.price-final_price.tax.weee > span.price-wrapper > span.price").text();
                            }

                            //System.out.println(arrayImage.get(a).get(s).select("span.product-image-wrapper > img.photo.image").attr("data-src"));

                            if(arrayImage.get(a).get(s).hasText()) {

                                System.out.println(arrayImage.get(a).get(s));
                                image = arrayImage.get(a).get(s).select("span.product-image-wrapper > img.photo.image").attr("src");
                            }

                            System.out.println(name+" | "+ newPrice+" | "+oldPrice +" | "+image);


                        }


                        }






                    /*if(docCustomConn.body().getElementsByClass("product name product-item-name").hasText()){
                        name=docCustomConn.body().getElementsByClass("product name product-item-name").text();
                    }


                    if(docCustomConn.body().getElementsByClass("price-box price-final_price").select("span.special-price").hasText()){
                        newPrice=docCustomConn.body().getElementsByClass("price-box price-final_price").select("span.special-price").text();
                    }



                    if(docCustomConn.body().getElementsByClass("price-box price-final_price").select("span.old-price.sly-old-price.no-display").hasText()){
                        oldPrice=docCustomConn.body().getElementsByClass("price-box price-final_price").select("span.old-price.sly-old-price.no-display").text();
                    } else {
                        oldPrice=docCustomConn.body().getElementsByClass("price-box price-final_price").select("span.special-price").text();
                    }



                    System.out.println(name+" | "+ newPrice+" | "+oldPrice);*/

                    //products list items product-items

                }

            /*if (docCustomConn.select(".row").hasText()) {
                //   System.out.println(docCustomConn.select(".row"));
                //page.add(docCustomConn);
            }*/

                //pageCount = docCustomConn.body().select("div.row");

                //LinkedList<Element> listCount = new LinkedList<Element>(pageCount);product name product-item-namew_4081d32cc51805c5041b619032d6d579b49971b8


                //System.out.println(docCustomConn.body().select("div.container"));


            }

//Elements test = docCustomConn.body().select("div.container");
//System.out.println(test.);
            //  do {

            // for (int countP = 0; countP <= Integer.parseInt(listCount.getLast().getElementsByClass("pagination__text").text()); countP++) {
           /* for (int countP = 0; countP <= 5; countP++) {

                docCustomConn = connectSetting("https://www.watsons.ua/ru/aktsii/vse-nominanty-hwb/c/hwb?page="+countP+"&startPage=0").get();

                LOGGER.info("parsing page: " + countP + " total: " + listCount.getLast());

                Thread.sleep(6000);

                page.add(docCustomConn);

            }


            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);

            }*/


            return page;

        } catch (HttpStatusException ex) {


            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("SocketTimeoutException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("IOException");

            return null;

        } /*catch (InterruptedException e) {
            e.printStackTrace();

            return null;
        }*/


    }

}
