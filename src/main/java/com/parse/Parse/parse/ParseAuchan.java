package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service("Auchan")
public class ParseAuchan extends AbstractParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Auchan", new Date(), true, null);

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;

    int count = 0;
    List<Document> page = new ArrayList<Document>();

    @Override
    public List<Document> connect(int count,List<Document> documents) {
        Document docCustomConn;


        int del = 0;
        String pageCount;


        if(documents!=null){
             page = new ArrayList<>(documents);
             System.out.println(page.size());
        }

        try {
            do {

                Thread.sleep(15000);

                docCustomConn = connectSetting("https://shop.auchan.ua/superceny/page-" + count + "/?&limit=96&limit=96").get();

                pageCount = Pattern.compile(
                        "Товаров\\s[\\d]+\\sдо\\s[\\d]+\\sиз\\s([\\d]+)",
                        Pattern.DOTALL)
                        .matcher(docCustomConn.select(".amount.amount--no-pages").text()).replaceAll("$1");

                del = (int) Math.ceil(Double.valueOf(pageCount) / 96);

                LOGGER.info("parsing page: " + count + " total: " + del);

                if (docCustomConn.select(".product-info > .product-name").hasText()) {
                    page.add(docCustomConn);

                    count++;

                }

            } while (count <= del);//while (count <= del);

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);
            }

            return page;


        } catch (SocketTimeoutException e) {

            LOGGER.info("repeat Parse work");
            LOGGER.info("parsing page: " + count + " total: " + del);

            connect(count,page);

            return null;

        } catch (HttpStatusException ex) {

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("HttpStatusException");

            return null;

        } catch (InterruptedException in) {

            in.printStackTrace();

            LOGGER.info("InterruptedException");

            state.setState(false);
            state.setError("InterruptedException");

            return null;

        } catch (IOException io) {

            LOGGER.info("IOException");

            state.setState(false);
            state.setError("IOException");

            io.printStackTrace();


            return null;
        }


    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        List<Document> docCustomConn = connect(1,null);

        if (docCustomConn == null) {
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }


        String fixPrice,fixPriceSemicolon,fixOldPriceSemicolon = "";
        Elements getName = null;
        String fixOldPrice = null;
        Elements getPrice = null;
        Elements getOldPrice = null;
        Elements getImage = null;

        List<Elements> arrayName =new ArrayList<>();
        List<Elements> arrayPrice =new ArrayList<Elements>();
        List<Elements> arrayOldPrice =new ArrayList<Elements>();
        List<Elements> arrayImage =new ArrayList<Elements>();

        Double price= null;
        Double oldPrice= null;

        for (Document page : docCustomConn) {

            getName = page.body().getElementsByClass("product-name");
            getPrice = page.body().getElementsByClass("price-box");
            getOldPrice = page.body().getElementsByClass("price-box");
            getImage = page.body().getElementsByClass("product-image");

            arrayName.add(getName);
            arrayPrice.add(getPrice);
            arrayOldPrice.add(getOldPrice);
            arrayImage.add(getImage);

        }


        ArrayList<EntityProduct> array = new ArrayList<>();

        if (getName.size() == getPrice.size()) {
            state.setState(true);
            state.setError(null);
            parseProcessRepository.save(state);
        } else {
            state.setState(false);
            state.setError("no equally value");
            parseProcessRepository.save(state);
        }

        try {

            for (int a = 0; a <= arrayName.size()-1; a++) {


                for (int i = 0; i <= arrayName.get(a).size()-1; i++) {


                    if(arrayPrice.get(a).get(i).getElementsByClass("regular-price").hasText()){

                        fixPrice = Pattern.compile(
                                "\\sгрн",
                                Pattern.DOTALL)
                                .matcher(arrayPrice.get(a).get(i).getElementsByClass("regular-price").select("span.price").text()).replaceAll("");


                        fixPriceSemicolon = Pattern.compile(
                                ",",
                                Pattern.DOTALL)
                                .matcher(fixPrice.replaceAll(" ", "")).replaceAll(".");


                        price = Double.valueOf(fixPriceSemicolon);




                    }else if (arrayPrice.get(a).get(i).getElementsByClass("special-price").hasText()) {

                        fixPrice = Pattern.compile(
                                "\\sгрн",
                                Pattern.DOTALL)
                                .matcher(arrayPrice.get(a).get(i).getElementsByClass("special-price").select("span.price").text()).replaceAll("");


                        fixPriceSemicolon = Pattern.compile(
                                ",",
                                Pattern.DOTALL)
                                .matcher(fixPrice.replaceAll(" ", "")).replaceAll(".");

                        price = Double.valueOf(fixPriceSemicolon);

                    }


                    if(arrayOldPrice.get(a).get(i).getElementsByClass("old-price").hasText()){

                        fixOldPrice = Pattern.compile(
                                "\\sгрн",
                                Pattern.DOTALL)
                                .matcher(arrayOldPrice.get(a).get(i).getElementsByClass("old-price").select("span.price").text()).replaceAll("");

                        fixOldPriceSemicolon = Pattern.compile(
                                ",",
                                Pattern.DOTALL)
                                .matcher(fixOldPrice.replaceAll(" ", "")).replaceAll(".");

                        oldPrice = Double.valueOf(fixOldPriceSemicolon);

                    }else{

                        oldPrice=0.0;

                    }


                    array.add(new EntityProduct(i, arrayName.get(a).get(i).getElementsByTag("a").attr("title"), price, oldPrice,
                            new Date(), null, "Auchan", arrayImage.get(a).get(i).getElementsByTag("img").attr("src")));

                }

            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            System.out.println("go out from array in during parse market, please check value");
        }

        System.out.println("Total : "+array.size());

        return array;
    }

}



