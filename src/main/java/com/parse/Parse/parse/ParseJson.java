package com.parse.Parse.parse;

import com.google.gson.JsonArray;
import com.parse.Parse.entity.EntityProduct;


import java.util.ArrayList;
import java.util.List;

public interface ParseJson {

    JsonArray connectJson();

    ArrayList<EntityProduct> getInform();

}
