package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.xml.stream.events.EndElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("Сhervonyi")
public class ParseСhervonyi extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Сhervonyi Market", new Date(), true, null);

    @Override
    public List<Document> connect(int count, List<Document> documents) {

        List<Document> page = new ArrayList<Document>();
        String Url = "https://chervonyi.com.ua/aktsii?limitstart=0";

        try {

            ArrayList<Elements> elements = new ArrayList<>();
            ArrayList<Document> products = new ArrayList<>();

            int countPage =Integer.parseInt(connectSetting(Url).get().body().select("p.counter").text().replaceAll("Сторінка [\\d]+ із ([\\d]+)","$1"));

            for (int i = 0;i<=countPage;i++){

                Thread.sleep(4000);

                System.out.println("https://chervonyi.com.ua/aktsii?start="+6*i);

               if (connectSetting("https://chervonyi.com.ua/aktsii?start="+6*i).get().body().hasText()) {
                    elements.add(connectSetting("https://chervonyi.com.ua/aktsii?start=" + 6 * i).get().body().select("div.entry-image.intro-image a"));
                }


            }

            for(Elements pageElm :elements){


                for (Element productP:pageElm){

                    Thread.sleep(4000);

                    System.out.println(productP.attr("href"));

                    products.add(connectSetting("https://chervonyi.com.ua/aktsii"+productP.attr("href")).get());
                }

            }

            page.add(connectSetting(Url).get());

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);
            }

           return products;

        } catch (HttpStatusException ex) {

            LOGGER.info("Page not found");

            state.setError("HttpStatusException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();
            state.setError("IOException");
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        ArrayList<EntityProduct> array = new ArrayList<>();
        String name = null;
        String price = "";
        String oldPrice= "";
        String image = null;
        boolean checkName = false;
        boolean checkPrice = false;

        int count = 0;

        List<Document> products = connect(0,null);

        for(Document product : products){

            checkName = product.body().select("div.article-flex article.item.item-page.item-featured div p span strong span span span span span span").hasText();
            checkPrice = product.body().select("div.article-flex article.item.item-page.item-featured div p span strong").hasText();

            if (product.body().select("div.article-flex article.item.item-page.item-featured div p span strong span span span span span span").hasText()) {

                image = product.body().select("div.article-flex div.entry-image.full-image img").toString().replaceAll(".*?src=\"(.*?)\".*","$1");
                name = product.body().select("div.article-flex article.item.item-page.item-featured div p span strong span span span span span span").text();
                price = product.body().select("div.article-flex article.item.item-page.item-featured div p span strong").text().replaceAll(".*?\\-\\s?(?:від|лише)?\\s([\\d,.]+)\\sгрн.*", "$1");

            }

            if(checkName && checkPrice) {

                array.add(new EntityProduct(count, name, Double.valueOf(price.replaceAll("\\,",".")), Double.valueOf("0"),
                        new Date(), null, "Сhervonyi market", "https://chervonyi.com.ua"+image));

                count++;
            }

        }

       return array;
    }

}
