package com.parse.Parse.parse;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.ParseProcess;
import com.parse.Parse.repository.ParseProcessRepository;
import com.parse.Parse.thread.ParseThread;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("Varus")
public class ParseVarus extends AbstractParser {

    @Autowired
    @Qualifier("parseProcessRepository")
    private ParseProcessRepository parseProcessRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(ParseThread.class);
    private ParseProcess state = new ParseProcess("Varus", new Date(), true, null);

    @Override
    public List<Document> connect(int count,List<Document> documents) {
        Document docCustomConn;
        String Url = "https://varus.ua/uk/actions_list?page=";
        List<Document> page = new ArrayList<Document>();
        Elements pageCount;


        try {

            docCustomConn = connectSetting(Url + 0).get();

            if (docCustomConn.select(".desc").hasText()) {
                page.add(docCustomConn);
            }

            pageCount = docCustomConn.select(".item-list > .pager > .pager-last");

            String getPagesCount = Pattern.compile(
                    "<li class=\"pager-last last\">.*?page=([\\d]+)\">»</a></li>",
                    Pattern.DOTALL)
                    .matcher(pageCount.toString()).replaceAll("$1");


            // do {

            for (int countP = 0; countP <= Integer.parseInt(getPagesCount); countP++) {
            //    System.out.println("Page " + countP);
                docCustomConn = connectSetting(Url + countP).get();

                Thread.sleep(6000);

                page.add(docCustomConn);
            }


            //} while (docCustomConn.select(".desc").hasText());

            if (!page.isEmpty()) {
                state.setState(true);
                state.setError(null);
            } else {
                state.setState(false);

            }


            return page;

        } catch (HttpStatusException ex) {


            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("SocketTimeoutException");

            return null;

        } catch (IOException e) {
            e.printStackTrace();

            LOGGER.info("Page not found");

            state.setState(false);
            state.setError("IOException");

            return null;

        } catch (InterruptedException e) {
            e.printStackTrace();

            return null;
        }


    }

    @Override
    public ArrayList<EntityProduct> getInform() {

        List<Document> docCustomConn = connect(0,null);

        if (docCustomConn == null) {
            state.setState(false);
            parseProcessRepository.save(state);
            return null;
        }

        Elements getProduct;


        Matcher getName, getOldPrice, getPrice, getImageProduct, getPeriodProduct;

        List<String> nameProduct = new ArrayList<>();
        List<String> oldPriceProduct = new ArrayList<>();
        List<String> priceProduct = new ArrayList<>();
        List<String> imageProduct = new ArrayList<>();
        List<String> periodProduct = new ArrayList<>();


        int i = 0;
        ArrayList<EntityProduct> array = new ArrayList<>();

        for (Document page : docCustomConn) {

            getProduct = page.select(".action_item > .action_block");
            List<String> products = new ArrayList<>(Arrays.asList(getProduct.toString().split("\\s?\\n?<\\/div>\\n<div class=\"action_block  \">\\s?\\n?\\s?")));


            System.out.println("Count: " + products.size());


            for (String product : products) {

                getName = Pattern.compile(
                        "<div.*?class=\"sale_block\">.*?<div class=\"description\">.*?<h4>(.*?)<\\/h4>.*?<h5>(?:\\s<p>|\\s)(.*?)(?:\\s?<\\/p>\\s|\\s)<\\/h5>.*",
                        Pattern.DOTALL)
                        .matcher(product);

                getOldPrice = Pattern.compile(
                        ".*?<span class=\"old\">\\s?([\\d]+)\\s?<span>([\\d]+)<\\/span>\\s?<\\/span>.*",
                        Pattern.DOTALL)
                        .matcher(product);

                getPrice = Pattern.compile(
                        ".*?<span class=\"new\">\\s?(?:<sub>від<\\/sub>\\s)?([\\d]+)\\s?<span>([\\d]+)<\\/span>\\s?<\\/span>.*",
                        Pattern.DOTALL)
                        .matcher(product);

                getImageProduct = Pattern.compile(
                        ".*?<div class=\"img\">.*?<img src=\"(.*?)\".*?alt=\"\">.*",
                        Pattern.DOTALL)
                        .matcher(product);

                getPeriodProduct = Pattern.compile(
                        ".*?<div class=\"sales_day\">\\n\\s*(.*?)\\n.*?<\\/div>.*?<\\/div>.*",
                        Pattern.DOTALL)
                        .matcher(product);



                if (getOldPrice.find()) {
                    oldPriceProduct.add(i,getOldPrice.replaceAll("$1.$2"));
                } else {
                    oldPriceProduct.add(i,"0");
                }

                if (getPrice.find()) {
                    priceProduct.add(i,getPrice.replaceAll("$1.$2"));
                } else {
                    priceProduct.add(i,"0");
                }

                if (getImageProduct.find()) {
                    imageProduct.add(i,getImageProduct.replaceAll("$1"));
                }

                if (getPeriodProduct.find()) {
                    periodProduct.add(i,getPeriodProduct.replaceAll("$1"));
                } else {
                    periodProduct.add(i,null);
                }

                if (getName.find()) {
                    nameProduct.add(i,getName.replaceAll("$1 $2"));

                    if( !priceProduct.get(i).equals("0") && (!oldPriceProduct.get(i).equals("0") || oldPriceProduct.get(i).equals("0"))) {

                        array.add(i,new EntityProduct(i, nameProduct.get(i), Double.valueOf(priceProduct.get(i)), Double.valueOf(oldPriceProduct.get(i)),
                                new Date(), null, "Varus", imageProduct.get(i)));
                        i++;
                    }
                }

            }

            if (nameProduct.size() == priceProduct.size()) {
                state.setState(true);
                state.setError(null);
                parseProcessRepository.save(state);
            } else {
                state.setState(false);
                state.setError("no equally value");
                parseProcessRepository.save(state);
            }


        }

        System.out.println("Varus-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        return array;
    }
}
