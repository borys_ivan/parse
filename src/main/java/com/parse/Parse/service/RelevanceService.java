package com.parse.Parse.service;

import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Relevance;

public interface RelevanceService {

    void saveState(Relevance state);

    Relevance FindIdProduct(Product product);

    void updateProposition(Product product);

    void checkProposition(Product product);

    void hideOldProposition(String company);

    void hideOldProduct(String company);

}
