package com.parse.Parse.service;

import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Relevance;
import com.parse.Parse.repository.ProductRepository;
import com.parse.Parse.repository.RelevanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelevanceServiceImpl implements RelevanceService {

    @Autowired
    @Qualifier("productRepository")
    private ProductRepository productRepository;

    @Autowired
    @Qualifier("relevanceRepository")
    private RelevanceRepository relevanceRepository;

    @Override
    public void saveState(Relevance state) {
        relevanceRepository.save(state);
    }

    @Override
    public Relevance FindIdProduct(Product product) {
        return relevanceRepository.findFirstByProduct(product);
    }


    @Override
    public void updateProposition(Product product) {

        if (product != null) {

            Relevance existProduct = FindIdProduct(product);
            existProduct.setState("Update");
            relevanceRepository.save(existProduct);

        }

    }


    @Override
    public void checkProposition(Product product) {


        Relevance existProduct = FindIdProduct(product);

            existProduct.setState("Checked");
            relevanceRepository.save(existProduct);

            Product visible = productRepository.findFirstById(product.getId());
            visible.setVisible(true);

            productRepository.save(visible);
    }


    @Override
    public void hideOldProposition(String company) {

        List<Relevance> states = relevanceRepository.findAllByState("Actually");

        for (Relevance state : states) {

            Product updateVisible = productRepository.
                    getOne(productRepository.findByRelevance(state).getId());
            updateVisible.setVisible(false);

            productRepository.save(updateVisible);


            Relevance hide = relevanceRepository.getOne(state.getId());
            hide.setState("Hided");
            relevanceRepository.save(hide);

        }




       //List<Object> oldProducts = relevanceRepository.oldProduct(company);

       //System.out.println("----------------------------------------");


      // for (Object product:oldProducts){
          //Relevance productState = relevanceRepository.getOne(product);
       //   System.out.println(product);


          //productState.setState("Hide");
   //    }


    }

    @Override
    public void hideOldProduct(String company) {
        /*List<Object[]> arrayOldproduct = relevanceRepository.findHide(company);


        for(Object[] oldProduct :arrayOldproduct){
            Product product = productRepository.findByNameAndCompany(oldProduct[5].toString(),company);

            product.setVisible(false);
            productRepository.save(product);
        }*/

        /*put hide relevance.state="Hide" when product don't in Object Parse*/
        relevanceRepository.oldProduct(company);

        /*hide product put product.visible=true*/
        relevanceRepository.hideProduct(company);
    }


}



