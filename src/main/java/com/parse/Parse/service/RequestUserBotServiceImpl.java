package com.parse.Parse.service;

import com.parse.Parse.entity.RequestUserBot;
import com.parse.Parse.repository.RequestUserBotRepository;
import com.parse.Parse.repository.UsersBotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;

@Service
public class RequestUserBotServiceImpl implements RequestUserBotService{

    @Qualifier("usersBotRepository")
    @Autowired
    private UsersBotRepository usersBotRepository;

    @Qualifier("requestUserBotRepository")
    @Autowired
    private RequestUserBotRepository requestUserBotRepository;

    public void countUsersRequest(Update update){

        UsersBotRepository checkUsersBot = usersBotRepository;
        RequestUserBotRepository checkRequestFromUsersBot = requestUserBotRepository;

        if(usersBotRepository.findByIdUserBot(update.getMessage().getFrom().getId()) !=null && requestUserBotRepository.findByRequestAndUsersBot(update.getMessage().getText(),checkUsersBot.findByIdUserBot(update.getMessage().getFrom().getId()))==null){

            RequestUserBot requestUserBot = new RequestUserBot(update.getMessage().getText(),1,checkUsersBot.findByIdUserBot(update.getMessage().getFrom().getId()));
            requestUserBotRepository.save(requestUserBot);

        } else {

            RequestUserBot imediatlyReguestUser = requestUserBotRepository.findByRequestAndUsersBot(update.getMessage().getText(),checkUsersBot.findByIdUserBot(update.getMessage().getFrom().getId()));
            imediatlyReguestUser.setCountRequest(imediatlyReguestUser.getCountRequest()+1);

            requestUserBotRepository.save(imediatlyReguestUser);
        }

    }

}
