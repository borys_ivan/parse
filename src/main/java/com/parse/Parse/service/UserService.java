package com.parse.Parse.service;

import com.parse.Parse.entity.Users;


public interface UserService {
    void save(Users user);

    Users findByUsername(String username);
}
