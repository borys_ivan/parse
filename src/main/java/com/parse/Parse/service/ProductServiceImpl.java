package com.parse.Parse.service;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Relevance;
import com.parse.Parse.parse.Parse;
import com.parse.Parse.parse.ParseJson;
import com.parse.Parse.repository.ProductRepository;
import com.parse.Parse.repository.RelevanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private RelevanceService relevanceService;

    @Autowired
    @Qualifier("relevanceRepository")
    private RelevanceRepository relevanceRepository;

    @Autowired
    @Qualifier("productRepository")
    private ProductRepository productRepository;


    private Product findFirstByNameAndCompany(String name, String company) {

        Product findByName = productRepository.
                findFirstByNameAndCompany(name, company);

        if (findByName == null) {
            return null;
        } else {
            return findByName;
        }

    }


    @Override
    public String saveProduct(ArrayList<EntityProduct> parse) {
        //public String saveProduct(EntityProduct[] parse) {


        //EntityProduct[] ArrayProduct = parse;

        // if (ArrayProduct == null) {
        if (parse == null) {

            return "Don't parse this market";
        }

        Product findByName;
        Relevance relevance;

        //System.out.println(parse.getInform().length);
        //for (EntityProduct object : ArrayProduct) {
        for (EntityProduct object : parse) {
            //System.out.println(object.toString());

            findByName = productRepository.
                    findFirstByNameAndCompany(object.getName(), object.getCompany());


            if (findByName == null) {

                Product product = new Product();

                product.setName(object.getName());
                product.setPrice(object.getPrice());
                product.setOldPrice(object.getOldPrice());
                product.setPeriod(object.getPeriod());
                product.setImage(object.getImage());

                product.setGender(object.getGender());
                product.setReferLink(object.getReferLink());

                if (product.getDateAdd() == null) {
                    product.setDateAdd(object.getDateAdd());
                }

                product.setCompany(object.getCompany());
                product.setVisible(true);

                relevance = new Relevance("Added", product);
                relevanceService.saveState(relevance);


            }

        }

        System.out.println("saveProduct-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        return "Parse this market";
    }

    @Override
    public void updateProduct(ArrayList<EntityProduct> parse) {

        Product updateProduct;
        Product findByName;

        for (EntityProduct object : parse) {

            findByName = findFirstByNameAndCompany(object.getName(), object.getCompany());

            //if(object.getName().equals("Крем-фарба Palette WN-3 «Золотиста кава»"))
            //    System.out.println(object.getName() +" | "+object.getImage() );
           // System.out.println(findByName.toString());
           // System.out.println("UPDATED!!!!!!!!!UPDATED!!!!!!");

            //if (findByName.getImage().equals("http://simpleicon.com/wp-content/uploads/basket-4.png")){
            //    System.out.println(findByName.getName());}

            if (findByName != null && (findByName.getPeriod()!=object.getPeriod() || findByName.getPrice() != object.getPrice() || !findByName.getImage().equals(object.getImage()))) {
               // if (findByName.getImage().equals("http://simpleicon.com/wp-content/uploads/basket-4.png")){
               //     System.out.println(object.getName()+ "| Not updated!!!!!!");}
                //updateProduct = productRepository.getOne(findByName.getId());
                updateProduct = productRepository.findFirstById(findByName.getId());

                updateProduct.setPrice(object.getPrice());
                updateProduct.setPeriod(object.getPeriod());
                updateProduct.setImage(object.getImage());
                updateProduct.setDateUpdate(new Date());

                productRepository.save(updateProduct);

                relevanceService.updateProposition(findByName);
            }

        }

    }


    @Override
    public void checkProduct(ArrayList<EntityProduct> parse, String company) {

        Product findByName;

        for (EntityProduct object : parse) {

            findByName = findFirstByNameAndCompany(object.getName(), object.getCompany());

            if (findByName != null) {

                relevanceService.checkProposition(findByName);

            }

        }

        System.out.println("checkProduct-----------------------------------------");
        System.out.println("total : " + Runtime.getRuntime().totalMemory());
        System.out.println("free : " + Runtime.getRuntime().freeMemory());
        System.out.println("-----------------------------------------------");

        /*hide product*/
        relevanceService.hideOldProduct(company);

        /*put actually in relevance.state="Actually when product in Object Parse"*/
        relevanceRepository.actuallyProduct();


    }

    @Override
    public List<Product> findProductsByName(String name) {

        List<Product> products;

        if (!name.isEmpty()) {
            products = productRepository.findAllByNameContaining(name);
        } else {
            products = productRepository.findAll();
        }

        return products;
    }

    @Override
    public List<Product> findProductsByNameAndCompany(String name, String company) {

        if (!name.isEmpty()) {
            return priorityListProduct(name,productRepository.findAllByNameContainingAndCompany(name, company));
        } else {
            return productRepository.findAll();
        }

    }

    @Override
    public List<Product> findAllByCompany(String company) {
        if (!company.isEmpty()) {
            return productRepository.findAllByCompany(company);
        } else {
            return productRepository.findAll();
        }
    }

    @Override
    public List<Product> findAllByCompanyAndVisible(String company, Boolean visible) {
        if (!company.isEmpty()) {
            return productRepository.findAllByCompanyAndVisible(company, visible);
        } else {
            return productRepository.findAll();
        }
    }

    @Override
    public List<Product> findAllByNameContainingIgnoreCase(String name, Sort sort) {
        if (!name.isEmpty()) {
            return priorityListProduct(name,productRepository.findAllByNameContainingIgnoreCase(name, sort));
        } else {
            return productRepository.findAll();
        }
    }

    @Override
    public List<Product> findAllByNameContainingIgnoreCaseAndVisible(String name, Boolean visible, Sort sort) {
        if (!name.isEmpty()) {
            return priorityListProduct(name,productRepository.findAllByNameContainingIgnoreCaseAndVisible(name, true, sort));
        } else {
            return productRepository.findAll();
        }
    }


    private List<Product> priorityListProduct(String name,List<Product> listProduct){

        LinkedList<Product> sortProduct = new LinkedList<>();

        for (Product product:listProduct){

            if (product.getName().split("\\s",2)[0].equalsIgnoreCase(name)){
                sortProduct.addFirst(product);
            }else {
                sortProduct.addLast(product);
            }
        }

        return new ArrayList<>(sortProduct);
    }


    @Override
    public List<Product> findAllByNameContainingIgnoreCaseAndCompany(String name, String company, Sort sort) {

        if (!name.isEmpty()) {
            return priorityListProduct(name,productRepository.findAllByNameContainingIgnoreCaseAndCompany(name, company, sort));
        } else {
            return productRepository.findAll();
        }

    }

    @Override
    public List<Product> findAllByNameContainingIgnoreCaseAndCompanyAndVisible(String name, String company, Boolean visible, Sort sort) {
        if (!name.isEmpty()) {
           return priorityListProduct(name,productRepository.findAllByNameContainingIgnoreCaseAndCompanyAndVisible(name, company, true, sort));
        } else {
            return productRepository.findAll();
        }
    }

    @Override
    public List<Product> findGeneralReguestProduct(String request, Boolean visible, Sort sort) {

        List<Product> allReguestProduct;
        ArrayList<Product> totalReguestProduct = new ArrayList<>();

        if (!request.isEmpty()) {

            String[] arrayRequset = request.split("\\|");

            for (String req : arrayRequset) {

                allReguestProduct = productRepository.findAllByNameContainingIgnoreCaseAndVisible(req, true, sort);

                totalReguestProduct.addAll(allReguestProduct);

            }


            return totalReguestProduct;
        } else {
            return productRepository.findAll();
        }
    }


}
