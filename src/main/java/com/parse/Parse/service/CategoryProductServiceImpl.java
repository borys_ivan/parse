package com.parse.Parse.service;



import com.parse.Parse.entity.CategoryListProduct;
import com.parse.Parse.entity.CategoryProduct;
import com.parse.Parse.repository.CategoryListProductRepository;
import com.parse.Parse.repository.CategoryProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryProductServiceImpl implements CategoryProductService {

    @Autowired
    @Qualifier("categoryProductRepository")
    private CategoryProductRepository categoryProductRepository;

    @Autowired
    @Qualifier("categoryListProductRepository")
    private CategoryListProductRepository categoryListProductRepository;


    @Override
    public List<CategoryProduct> listCategory() {
        return categoryProductRepository.findAll();
    }


}
