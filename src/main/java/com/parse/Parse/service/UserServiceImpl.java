package com.parse.Parse.service;

import com.parse.Parse.entity.Users;
import com.parse.Parse.repository.RoleRepository;
import com.parse.Parse.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

   // @Autowired
   // private BCryptPasswordEncoder bCryptPasswordEncoder;
   @Bean
   public BCryptPasswordEncoder bCryptPasswordEncoder() {
       return new BCryptPasswordEncoder();
   }
    //@Autowired
   // private SecurityConfig securityConfig;

    @Override
    public void save(Users user) {
        user.setPassword(bCryptPasswordEncoder().encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public Users findByUsername(String username) {
        return userRepository.findByName(username);
    }
}
