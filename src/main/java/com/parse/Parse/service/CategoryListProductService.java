package com.parse.Parse.service;

import com.parse.Parse.entity.CategoryListProduct;
import com.parse.Parse.entity.CategoryProduct;

import java.util.List;

public interface CategoryListProductService {

    List<CategoryListProduct> listProductFromCategory(CategoryProduct cat);

    List<CategoryListProduct> test(int i);

}
