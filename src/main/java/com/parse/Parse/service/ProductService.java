package com.parse.Parse.service;

import com.parse.Parse.entity.EntityProduct;
import com.parse.Parse.entity.Product;
import com.parse.Parse.parse.Parse;
import com.parse.Parse.parse.ParseJson;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

public interface ProductService {

     String saveProduct(ArrayList<EntityProduct> parse);

     void updateProduct(ArrayList<EntityProduct> parse);

     void checkProduct(ArrayList<EntityProduct> parse,String company);

     List<Product> findProductsByName(String name);

     List<Product> findProductsByNameAndCompany(String name, String company);

     List<Product> findAllByCompany(String company);

     List<Product> findAllByCompanyAndVisible(String company, Boolean visible);

     List<Product> findAllByNameContainingIgnoreCase(String name, Sort sort);

     List<Product> findAllByNameContainingIgnoreCaseAndVisible(String name,Boolean visible , Sort sort);

     List<Product> findAllByNameContainingIgnoreCaseAndCompany(String name,String company, Sort sort);

     List<Product> findAllByNameContainingIgnoreCaseAndCompanyAndVisible(String name,String company,Boolean visible, Sort sort);

     List<Product> findGeneralReguestProduct(String request,Boolean visible,Sort sort);

}
