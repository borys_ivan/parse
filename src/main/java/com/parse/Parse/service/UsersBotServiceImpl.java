package com.parse.Parse.service;

import com.parse.Parse.entity.UsersBot;
import com.parse.Parse.repository.UsersBotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Date;

@Service
public class UsersBotServiceImpl implements UsersBotService {


    @Qualifier("usersBotRepository")
    @Autowired
    private UsersBotRepository usersBotRepository;


    @Override
    public void save(UsersBot userBot) {

    }


    public void createdOrUpdatedUserBot(Update update) {

        if (usersBotRepository.findByIdUserBot(update.getMessage().getFrom().getId()) == null) {

            UsersBot usersBot = new UsersBot();

            usersBot.setIdUserBot(update.getMessage().getFrom().getId());
            usersBot.setFirstName(update.getMessage().getFrom().getFirstName());
            usersBot.setLastName(update.getMessage().getFrom().getLastName());
            usersBot.setUsername(update.getMessage().getFrom().getUserName());
            usersBot.setLang(update.getMessage().getFrom().getLanguageCode());
            usersBot.setChatID(update.getMessage().getChatId());

            usersBot.setFirstVisit(new Date());

            usersBotRepository.save(usersBot);

        } else {

            UsersBot usersBotFind;
            UsersBot findUserByID = usersBotRepository.findByIdUserBot(update.getMessage().getFrom().getId());


            User user = update.getMessage().getFrom();
            UsersBot equalsUserBot = new UsersBot(user.getId(), user.getFirstName(), user.getLastName(), user.getUserName(), user.getLanguageCode());


            if (findUserByID.hashCode() != equalsUserBot.hashCode()) {
                usersBotFind = usersBotRepository.findByIdUserBot(update.getMessage().getFrom().getId());

                usersBotFind.setIdUserBot(update.getMessage().getFrom().getId());
                usersBotFind.setFirstName(update.getMessage().getFrom().getFirstName());
                usersBotFind.setLastName(update.getMessage().getFrom().getLastName());
                usersBotFind.setUsername(update.getMessage().getFrom().getUserName());
                usersBotFind.setLang(update.getMessage().getFrom().getLanguageCode());

                usersBotRepository.save(usersBotFind);

            }

            updatedLastVisit(update);


        }

    }


    private void updatedLastVisit(Update update){
System.out.println("UPDATED!!!!!!!!!!!!!!!!!!!!!!!!!");
        UsersBot usersBotFind;
        usersBotFind = usersBotRepository.findByIdUserBot(update.getMessage().getFrom().getId());
        System.out.println("UPDATED!!!!!!!!!!!!!!!!!!!!!!!!! "+new Date());
        usersBotFind.setLastVisit(new Date());

        usersBotRepository.save(usersBotFind);
    }

}

