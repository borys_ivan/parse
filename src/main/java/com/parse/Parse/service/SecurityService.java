package com.parse.Parse.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
