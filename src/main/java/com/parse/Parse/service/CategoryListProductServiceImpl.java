package com.parse.Parse.service;

import com.parse.Parse.entity.CategoryListProduct;
import com.parse.Parse.entity.CategoryProduct;
import com.parse.Parse.repository.CategoryListProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryListProductServiceImpl implements CategoryListProductService {

    @Autowired
    @Qualifier("categoryListProductRepository")
    private CategoryListProductRepository categoryListProductRepository;

    @Override
    public List<CategoryListProduct> listProductFromCategory(CategoryProduct cat) {
        return categoryListProductRepository.findByCategory(cat);
    }

    @Override
    public List<CategoryListProduct> test(int id) {
        return categoryListProductRepository.findById(id);
    }
}
