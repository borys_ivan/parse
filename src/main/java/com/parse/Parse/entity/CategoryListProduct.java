package com.parse.Parse.entity;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "CategoryListProduct")
public class CategoryListProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    //@Column(name = "categoryId")
    //private int categoryId;
    @Column(name = "name")
    private String name;

    @Column(name = "notMatch")
    private String notMatch;


    @Column(name = "nameCategory")
    private String nameCategory;

    //@Column(name = "notMatch")
    //private String[] notMatch;

    //@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    //@JoinColumn(name = "categoryId")
    //private List<СategoryProduct> сategoryProduct = new ArrayList<>();
    //private СategoryProduct сategoryProduct;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cat_id")
    private CategoryProduct category;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CategoryProduct getCategory() {
        return category;
    }

    public void setCategory(CategoryProduct category) {
        this.category = category;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    /*public List<СategoryProduct> getСategoryProduct() {
        return сategoryProduct;
    }

    public void setСategoryProduct(List<СategoryProduct> сategoryProduct) {
        this.сategoryProduct = сategoryProduct;
    }*/

    /*public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(String notMatch) {
        this.notMatch = notMatch;
    }
}
