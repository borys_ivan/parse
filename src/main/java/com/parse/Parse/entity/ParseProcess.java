package com.parse.Parse.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "parseProcess")
public class ParseProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "company")
    private String company;
    @Column(name = "date")
    private Date date;
    @Column(name = "state")
    private boolean state;
    @Column(name = "error")
    private String error;

    public ParseProcess() {
    }

    public ParseProcess(String company, Date date, Boolean state, String error) {
        this.company = company;
        this.date = date;
        this.state = state;
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    @Override
    public String toString() {
        return "ParseProcess{" +
                "id=" + id +
                ", company='" + company + '\'' +
                ", date='" + date + '\'' +
                ", state='" + state + '\'' +
                ", error='" + error + '\'' +
                '}';
    }
}

