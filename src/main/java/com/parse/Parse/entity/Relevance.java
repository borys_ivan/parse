package com.parse.Parse.entity;

import javax.persistence.*;

@Entity
@Table(name = "relevance")
public class Relevance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "state")
    private String state;


    public Relevance() {
    }

    public Relevance(String state) {
        this.state = state;
    }

    public Relevance(String state, Product product) {
        this.state = state;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product")
    private Product product;

    @Override
    public String toString() {
        return "Relevance{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", product=" + product +
                '}';
    }
}
