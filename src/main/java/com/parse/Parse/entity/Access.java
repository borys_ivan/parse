package com.parse.Parse.entity;

import javax.persistence.*;

@Entity
@Table(name = "Access")
public class Access {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "access_token")
    private String access_token;

    public Access() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    @Override
    public String toString() {
        return "Access{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", access_token='" + access_token + '\'' +
                '}';
    }
}


