package com.parse.Parse.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "usersBot")
public class UsersBot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "idUserBot")
    private int idUserBot;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "lang")
    private String lang;

    @Column(name = "firstVisit")
    private Date firstVisit;

    @Column(name = "lastVisit")
    private Date lastVisit;

    @Column(name = "chatID")
    private Long chatID;

    @Column(name = "remind")
    private Boolean remind;

    public UsersBot(int idUserBot, String firstName, String lastName, String username, String lang) {
        this.idUserBot = idUserBot;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.lang = lang;
    }

    public UsersBot() {
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usersBot")
    private List<RequestUserBot> requestUserBotList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUserBot() {
        return idUserBot;
    }

    public void setIdUserBot(int idUserBot) {
        this.idUserBot = idUserBot;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<RequestUserBot> getRequestUserBotList() {
        return requestUserBotList;
    }

    public void setRequestUserBotList(List<RequestUserBot> requestUserBotList) {
        this.requestUserBotList = requestUserBotList;
    }

    public Date getFirstVisit() {
        return firstVisit;
    }

    public void setFirstVisit(Date firstVisit) {
        this.firstVisit = firstVisit;
    }

    public Date getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    public Boolean getRemind() {
        return remind;
    }

    public void setRemind(Boolean remind) {
        this.remind = remind;
    }

    @Override
    public String toString() {
        return "UsersBot{" +
                "id=" + id +
                ", idUserBot=" + idUserBot +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", lang='" + lang + '\'' +
                ", firstVisit=" + firstVisit +
                ", lastVisit=" + lastVisit +
                ", chatID=" + chatID +
                ", remind=" + remind +
                //", requestUserBotList=" + requestUserBotList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsersBot)) return false;
        UsersBot usersBot = (UsersBot) o;
        return getIdUserBot() == usersBot.getIdUserBot() &&
                Objects.equals(getFirstName(), usersBot.getFirstName()) &&
                Objects.equals(getLastName(), usersBot.getLastName()) &&
                Objects.equals(getUsername(), usersBot.getUsername()) &&
                Objects.equals(getLang(), usersBot.getLang());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdUserBot(), getFirstName(), getLastName(), getUsername(), getLang());
    }
}
