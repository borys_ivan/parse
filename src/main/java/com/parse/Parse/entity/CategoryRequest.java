package com.parse.Parse.entity;

import javax.persistence.*;

@Entity
@Table(name = "CategoryRequest")
public class CategoryRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @Column(name = "name")
    private String name;


    @Column(name = "request")
    private String request;


    public CategoryRequest() {
    }

    public CategoryRequest(String name, String request) {
        this.name = name;
        this.request = request;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "CategoryRequest{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", request='" + request + '\'' +
                '}';
    }
}
