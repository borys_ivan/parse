package com.parse.Parse.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "userListProducts")
public class UserListProducts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /*@NotNull
    @Column(name = "idOfUser")
    private Integer idOfUser;
    @Column(name = "idOfProduct")
    private Integer idOfProduct;
    @Column(name = "idOfCompany")
    private Integer idOfCompany;*/


    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product")
    private Product product;

    //@ManyToOne
    //@JoinColumn(name="id_company", nullable=false)
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_company")
    private Company company;

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_users_bot")
    private UsersBot usersBot;


    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Column(name = "dateTime")
    private Date dateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public UsersBot getUsersBot() {
        return usersBot;
    }

    public void setUsersBot(UsersBot usersBot) {
        this.usersBot = usersBot;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "UserListProducts{" +
                "id=" + id +
                ", product=" + product +
                ", company=" + company +
                ", usersBot=" + usersBot +
                ", dateTime=" + dateTime +
                '}';
    }
}