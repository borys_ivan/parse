package com.parse.Parse.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;
    @Column(name = "parse")
    private Boolean parse;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "linkPDF",length = 1000)
    private String linkPDF;

    @Column(name = "isPDF")
    private Boolean isPDF;

    @OneToMany(mappedBy="company")
    private List<UserListProducts> userListProducts;

    public Company() {
    }

    public Company(String name, Boolean parse) {
        this.name = name;
        this.parse = parse;
    }

    public Company(String name, Boolean parse, Date startTime) {
        this.name = name;
        this.parse = parse;
        this.startTime = startTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getParse() {
        return parse;
    }

    public void setParse(Boolean parse) {
        this.parse = parse;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getLinkPDF() {
        return linkPDF;
    }

    public void setLinkPDF(String linkPDF) {
        this.linkPDF = linkPDF;
    }

    public Boolean getPDF() {
        return isPDF;
    }

    public void setPDF(Boolean PDF) {
        isPDF = PDF;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parse=" + parse +
                ", startTime=" + startTime +
                ", linkPDF='" + linkPDF + '\'' +
                ", isPDF=" + isPDF +
                '}';
    }
}
