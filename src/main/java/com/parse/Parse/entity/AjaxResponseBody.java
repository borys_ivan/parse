package com.parse.Parse.entity;

import org.springframework.data.domain.Page;

import java.util.List;

public class AjaxResponseBody {

    String msg;
    List<Product> result;
    Page<Product> pageResult;
    Integer totalPage;
    String company;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Product> getResult() {
        return result;
    }

    public void setResult(List<Product> result) {
        this.result = result;
    }

    public Page<Product> getPageResult() {
        return pageResult;
    }

    public void setPageResult(Page<Product> pageResult) {
        this.pageResult = pageResult;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
