package com.parse.Parse.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "CategoryProduct")
public class CategoryProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nameCategory")
    private String nameCategory;
    @Column(name = "imageCat")
    private String imageCat;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    //@OneToMany(mappedBy = "сategoryProduct")
    //private CategoryListProduct categoryListProduct;
    @OneToMany(mappedBy="category")
    private List<CategoryListProduct> categoryListProducts;

    public String getImageCat() {
        return imageCat;
    }

    public void setImageCat(String imageCat) {
        this.imageCat = imageCat;
    }

    @Override
    public String toString() {
        return "CategoryProduct{" +
                "id=" + id +
                ", nameCategory='" + nameCategory + '\'' +
                ", imageCat='" + imageCat + '\'' +
                ", categoryListProducts=" + categoryListProducts +
                '}';
    }
}
