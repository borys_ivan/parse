package com.parse.Parse.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private double price;
    @Column(name = "oldPrice")
    private double oldPrice;
    @Column(name = "dateAdd")
    private Date dateAdd;
    @Column(name = "dateUpdate")
    private Date dateUpdate;
    @Column(name = "period")
    private String period;
    @Column(name = "company")
    private String company;
    @Column(name = "visible")
    private boolean visible;
    @Column(name = "image")
    private String image;
    @Column(name = "referLink")
    private String referLink;
    @Column(name = "gender")
    private String gender;


    @OneToOne(mappedBy = "product")
    private Relevance relevance;

    @OneToMany(mappedBy="product")
    private List<UserListProducts> userListProducts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visble) {
        this.visible = visble;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReferLink() {
        return referLink;
    }

    public void setReferLink(String referLink) {
        this.referLink = referLink;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", oldPrice=" + oldPrice +
                ", dateAdd=" + dateAdd +
                ", dateUpdate=" + dateUpdate +
                ", period='" + period + '\'' +
                ", company='" + company + '\'' +
                ", visible=" + visible +
                ", image='" + image + '\'' +
                ", referLink='" + referLink + '\'' +
                ", gender='" + gender + '\'' +
                //", relevance=" + relevance +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId() == product.getId() &&
                Double.compare(product.getPrice(), getPrice()) == 0 &&
                Double.compare(product.getOldPrice(), getOldPrice()) == 0 &&
                isVisible() == product.isVisible() &&
                Objects.equals(getName(), product.getName()) &&
                Objects.equals(getDateAdd(), product.getDateAdd()) &&
                Objects.equals(getDateUpdate(), product.getDateUpdate()) &&
                Objects.equals(getPeriod(), product.getPeriod()) &&
                Objects.equals(getCompany(), product.getCompany()) &&
                Objects.equals(getImage(), product.getImage()) &&
                Objects.equals(getReferLink(), product.getReferLink()) &&
                Objects.equals(getGender(), product.getGender());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPrice(), getOldPrice(), getDateAdd(), getDateUpdate(), getPeriod(), getCompany(), isVisible(), getImage(), getReferLink(), getGender());
    }
}
