package com.parse.Parse.entity;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

import static java.lang.Math.ceil;


public class PaginateRequest {

    private int currentPage;
    //private int totalCountPages;
    private int countElementOnPage;
    private int limit;
    private int offset;
    private List<Product> list;

    public PaginateRequest(){

    }

    public PaginateRequest(int currentPage, int countElementOnPage , List<Product> list) {
        this.currentPage = currentPage;
        //this.totalCountPages = totalCountPages;
        this.countElementOnPage = countElementOnPage;
        //this.limit = limit;
        //this.offset = offset;
        this.list = list;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /*public int getTotalCountPages() {
        return totalCountPages;
    }

    public void setTotalCountPages(int totalCountPages) {
        this.totalCountPages = totalCountPages;
    }*/

    public int getCountElementOnPage() {
        return countElementOnPage;
    }

    public void setCountElementOnPage(int countElementOnPage) {
        this.countElementOnPage = countElementOnPage;
    }

    /*public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }*/

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }

    public int countPageProcess(){

       return (int) Math.ceil(list.size()/(double) countElementOnPage);

    }

    public HashMap<String,Integer> nextPage(int currentPage){

        HashMap<String,Integer> valuePage = new HashMap<>();

        valuePage.put("offset",countElementOnPage*currentPage);
        valuePage.put("limit",(countElementOnPage*currentPage)+countElementOnPage);

        return valuePage;

    }

    @Override
    public String toString() {
        return "PaginateRequest{" +
                "currentPage=" + currentPage +
                ", countElementOnPage=" + countElementOnPage +
                ", limit=" + limit +
                ", offset=" + offset +
                ", list=" + list +
                '}';
    }
}
