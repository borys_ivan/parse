package com.parse.Parse.entity;

import java.util.Map;
import java.util.Set;

public class GeoListOfShops{

    private Set<Integer> IdOfShops;
    private Map<Double, Shops> listOfShops;

    public GeoListOfShops(Set<Integer> idOfShops, Map<Double, Shops> listOfShops) {
        IdOfShops = idOfShops;
        this.listOfShops = listOfShops;
    }

    public Set<Integer> getIdOfShops() {
        return IdOfShops;
    }

    public void setIdOfShops(Set<Integer> idOfShops) {
        IdOfShops = idOfShops;
    }

    public Map<Double, Shops> getListOfShops() {
        return listOfShops;
    }

    public void setListOfShops(Map<Double, Shops> listOfShops) {
        this.listOfShops = listOfShops;
    }
}
