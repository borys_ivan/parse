package com.parse.Parse.entity;

import java.util.Date;

public class EntityProduct {

    private int id;
    private String name;
    private double price;
    private double oldPrice;
    private Date dateAdd;
    private String period;
    private String company;
    private String image;
    private String gender;
    private String referLink;

    public EntityProduct(int id, String name, double price, double oldPrice, Date dateAdd, String period, String company,String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.oldPrice = oldPrice;
        this.dateAdd = dateAdd;
        this.period = period;
        this.company = company;
        this.image = image;
    }

    public EntityProduct(int id, String name, double price, double oldPrice, Date dateAdd, String period, String company, String image, String gender, String referLink) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.oldPrice = oldPrice;
        this.dateAdd = dateAdd;
        this.period = period;
        this.company = company;
        this.image = image;
        this.gender = gender;
        this.referLink = referLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Date getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Date dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReferLink() {
        return referLink;
    }

    public void setReferLink(String referLink) {
        this.referLink = referLink;
    }

    @Override
    public String toString() {
        return "EntityProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", oldPrice=" + oldPrice +
                ", dateAdd=" + dateAdd +
                ", period='" + period + '\'' +
                ", company='" + company + '\'' +
                ", image='" + image + '\'' +
                ", gender='" + gender + '\'' +
                ", referLink='" + referLink + '\'' +
                '}';
    }
}
