package com.parse.Parse.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "requestUserBot")
public class RequestUserBot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    //@Column(name = "idUserBot")
    //private int idUserBot;

    @Column(name = "request")
    private String request;

    @Column(name = "countRequest")
    private Integer countRequest;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUserBot")
    private UsersBot usersBot;

    public RequestUserBot(String request, Integer countRequest, UsersBot usersBot) {
        this.request = request;
        this.countRequest = countRequest;
        this.usersBot = usersBot;
    }

    public RequestUserBot() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Integer getCountRequest() {
        return countRequest;
    }

    public void setCountRequest(Integer countRequest) {
        this.countRequest = countRequest;
    }

    public UsersBot getUsersBot() {
        return usersBot;
    }

    public void setUsersBot(UsersBot usersBot) {
        this.usersBot = usersBot;
    }

    @Override
    public String toString() {
        return "RequestUserBot{" +
                "id=" + id +
                ", request='" + request + '\'' +
                ", countRequest='" + countRequest + '\'' +
                ", usersBot=" + usersBot +
                '}';
    }
}
