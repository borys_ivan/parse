package com.parse.Parse.entity;

import javax.persistence.*;

@Entity
@Table(name = "CategoryOfShop")
public class CategoryShop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "nameOfCategory")
    private String nameOfCategory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfCategory() {
        return nameOfCategory;
    }

    public void setNameOfCategory(String nameOfCategory) {
        this.nameOfCategory = nameOfCategory;
    }

    @Override
    public String toString() {
        return "CategoryShop{" +
                "id=" + id +
                ", nameOfCategory='" + nameOfCategory + '\'' +
                '}';
    }
}
