package com.parse.Parse.repository;

import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Relevance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository("relevanceRepository")
public interface RelevanceRepository extends JpaRepository<Relevance, Integer> {

    Relevance findFirstByProduct(Product product);

    Relevance findFirstByIdAndState(Product product,String state);

    List<Relevance> findAllByState(String state);


    /*@Query( value = "UPDATE relevance SET state='Actually' WHERE state='Checked'",
            nativeQuery = true)
    void actuallyProduct();*/
    @Transactional
    @Modifying
    @Query( value = "UPDATE relevance SET state='Actually' WHERE state='Checked'",nativeQuery = true)
    void actuallyProduct();

    @Transactional
    @Modifying
    @Query( value = "UPDATE relevance SET state='Hide' FROM product WHERE relevance.id_product=product.id and relevance.state!='Checked' and product.company=:company",
            nativeQuery = true)
    void oldProduct(@Param("company") String company);

    @Transactional
    @Modifying
    @Query( value = "SELECT product.* FROM product,relevance WHERE relevance.id_product=product.id and relevance.state='Hide' and product.company=:company",
            nativeQuery = true)
    List<Object[]> findHide(@Param("company") String company);

    @Transactional
    @Modifying
    @Query(value="update product set visible=false from relevance where relevance.id_product=product.id and relevance.state='Hide' and product.company=:company"
            ,nativeQuery = true)
    void hideProduct(@Param("company") String company);


}
