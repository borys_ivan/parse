package com.parse.Parse.repository;

import com.parse.Parse.entity.Access;
import com.parse.Parse.entity.CategoryRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("accessRepository")
public interface AccessRepository extends JpaRepository<Access,Integer> {

    public Access findFirstById(Integer id);
}
