package com.parse.Parse.repository;

import com.parse.Parse.entity.CategoryListProduct;
import com.parse.Parse.entity.CategoryProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("categoryListProductRepository")
public interface CategoryListProductRepository extends JpaRepository<CategoryListProduct,Integer> {

    List<CategoryListProduct>findByCategory(CategoryProduct cat);

    List<CategoryListProduct>findById(int id);

    CategoryListProduct findFirstByName(String name);

}
