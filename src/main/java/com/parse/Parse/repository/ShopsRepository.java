package com.parse.Parse.repository;

import com.parse.Parse.entity.Role;
import com.parse.Parse.entity.Shops;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("shopsRepository")
public interface ShopsRepository extends JpaRepository<Shops,Integer> {

    public Shops findByAddress(String address);

    List<Shops> findAllByIdOfCompany(Integer idOfCompany);

}
