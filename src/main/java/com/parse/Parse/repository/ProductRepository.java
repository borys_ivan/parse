package com.parse.Parse.repository;


import com.parse.Parse.entity.Product;
import com.parse.Parse.entity.Relevance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productRepository")
public interface ProductRepository extends JpaRepository<Product,Integer> {

   Product findFirstByNameAndCompany(String name, String company);

   Product findFirstById(Integer id);

   Product findByRelevance(Relevance relevance);

   List<Product> findAllByNameContaining(String name);

   List<Product> findAllByNameContainingAndCompany(String name,String company);

   List<Product> findAllByNameContainingIgnoreCaseAndCompany(String name,String company, Sort sort);

   List<Product> findAllByNameContainingIgnoreCase(String name, Sort sort);

   List<Product> findAllByCompany(String company);

   List<Product> findAllByCompanyAndVisible(String company,Boolean visible);

   List<Product> findAllByVisible(Boolean visible);

   List<Product> findAllByVisible(Boolean visible,Sort sort);

   Page<Product> findAll(Pageable pageable);

   Page<Product> findAllByVisible(Boolean visible,Pageable pageable);

   Page<Product> findAllByCompany(String company,Pageable pageable);

   Page<Product> findAllByCompanyAndVisible(String company,Boolean visible,Pageable pageable);

   Product findByNameAndCompany(String name, String company);

   List<Product> findAllByNameContainingIgnoreCaseAndVisible(String name,Boolean visible , Sort sort);

   List<Product> findAllByNameContainingIgnoreCaseAndCompanyAndVisible(String name,String company,Boolean visible , Sort sort);

   List<Product> findAllByNameContainingIgnoreCaseAndCompanyAndVisible(String name,String company,Boolean visible , Pageable pageable);
}
