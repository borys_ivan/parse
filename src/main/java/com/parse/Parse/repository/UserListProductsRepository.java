package com.parse.Parse.repository;

import com.parse.Parse.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userListProductsRepository")
public interface UserListProductsRepository extends JpaRepository<UserListProducts,Integer> {

    public UserListProducts findByUsersBotAndProductAndCompany(UsersBot usersBot, Product product, Company company);

    public List<UserListProducts> findAllByUsersBot(UsersBot users);

}
