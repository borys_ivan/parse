package com.parse.Parse.repository;

import com.parse.Parse.entity.UsersBot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("usersBotRepository")
public interface UsersBotRepository extends JpaRepository<UsersBot,Integer> {

    UsersBot findByUsername(String username);

    UsersBot findByFirstName(String firstName);

    UsersBot findByLastName(String lastName);

    UsersBot findByIdUserBot(Integer IdUserBot);

    UsersBot findByLang(String lang);

}
