package com.parse.Parse.repository;

import com.parse.Parse.entity.CategoryRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("categoryRequestRepository")
public interface CategoryRequestRepository extends JpaRepository<CategoryRequest,Integer> {

   CategoryRequest findFirstByName(String request);

}
