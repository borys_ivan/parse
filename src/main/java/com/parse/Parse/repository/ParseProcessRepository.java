package com.parse.Parse.repository;


import com.parse.Parse.entity.ParseProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("parseProcessRepository")
public interface ParseProcessRepository extends JpaRepository<ParseProcess,Integer> {
}
