package com.parse.Parse.repository;

import com.parse.Parse.entity.Company;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("companyRepository")
public interface CompanyRepository extends JpaRepository<Company,Integer> {

    List<Company> findAllByParse(Boolean parse);

    Company findAllByName(String name);

    List<Company> findAll(Sort sort);
}
