package com.parse.Parse.repository;

import com.parse.Parse.entity.ProcessValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("ProcessValueRepository")
public interface ProcessValueRepository extends JpaRepository<ProcessValue,String> {

    ProcessValue findAllByName(String name);

}
