package com.parse.Parse.repository;

import com.parse.Parse.entity.RequestUserBot;
import com.parse.Parse.entity.UsersBot;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("requestUserBotRepository")
public interface RequestUserBotRepository extends JpaRepository<RequestUserBot,Integer> {

    RequestUserBot findByRequest(String request);

    List<RequestUserBot> findByUsersBot(UsersBot usersBot, Sort sort);

    RequestUserBot findByRequestAndUsersBot(String request, UsersBot usersBot);


}
