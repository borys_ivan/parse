package com.parse.Parse.repository;

import com.parse.Parse.entity.CategoryProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("categoryProductRepository")
public interface CategoryProductRepository extends JpaRepository<CategoryProduct,Integer> {

    //List<CategoryProductService> findAll();


}
