package com.parse.Parse.repository;

import com.parse.Parse.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users,Integer> {
    Users findByName(String username);
}
