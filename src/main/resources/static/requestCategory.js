function requestCategory(cat) {

    $('#pagination-list').empty();

    $.ajax({
        contentType: "application/json",
        url: "/general-reguest",
        type: "GET",
        data: {category: cat},
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('.panel-body').empty();

            $('#pagination-list').empty();
            $('#pagination-list-company').empty();

            var requestTable = "<div class=\"container\">\n" +
                "<div style=\"display: flex;\n" +
                "    flex-wrap: wrap;\n" +
                "    align-items: flex-end;\" class=\"row\">";


            $.each(data.result, function (index, value) {

                requestTable += "<div class=\"col-xs-3\">" +
                    "<table class=\"table table-striped table-bordered\">";
                requestTable += "<tr><th style='background-color: white;' colspan='2'><img width='250' height='250' src=" + value.image + "></th></tr>";

                if(value.price!==0) {

                    requestTable += "<tr><th colspan='2'>" + value.name + "</th></tr>";
                    requestTable += "<tr><th style='color: green;width: 50%;text-align: center'>" + value.price + ".грн" + "</th>" +
                        "<th style='text-align: center'>" + Math.round(value.oldPrice === 0 ? 0 : ((value.oldPrice - value.price) / value.oldPrice) * 100) + "%</th></tr>";
                    requestTable += "<tr><th style='text-align: center;color: orangered'>" + value.oldPrice + ".грн" + "</th>" +
                        "<th style='text-align: center'>" + value.company + "</th></tr>";

                }

                requestTable += "</table></div>";

            });

            if (data.result === 0) {

                $('.panel-body').html(requestTable += "<tr><td colspan='2'>" + data["msg"] + "</td></tr>");
                requestTable += "</table>";

            } else {

                requestTable += "</table>";
                $('.panel-body').html(requestTable);

            }
        }


    })

}