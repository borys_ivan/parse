$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var search = {}
    search["name"] = $("#name").val();
    search["company"] = $("#company").val();

    if(search["name"]!=="" && search["company"]!=="" || search["name"]!=="" && search["company"] ===""){

        //$('#pagination-list').empty();

    }


    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/search/products",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('.panel-body').empty();

            var requestTable = "<div class=\"container\">\n" +
                "<div style=\"display: flex;\n" +
                "    flex-wrap: wrap;\n" +
                "    align-items: flex-end;\" class=\"row\">";

            if(data.result!=null) {

                $('#pagination-list').empty();
                $('#pagination-list-company').empty();

            $.each(data.result, function (index, value) {

                requestTable += "<div class=\"col-xs-3\">" +
                    "<table class=\"table table-striped table-bordered\">";
                requestTable += "<tr><th style='background-color: white;' colspan='2'><img width='250' height='250' src=" + value.image + "></th></tr>";

                if(value.price!==0) {

                    requestTable += "<tr><th colspan='2'>" + value.name + "</th></tr>";
                    requestTable += "<tr><th style='color: green;width: 50%;text-align: center'>" + value.price + ".грн" + "</th>" +
                        "<th style='text-align: center'>" + Math.round(value.oldPrice === 0 ? 0 : ((value.oldPrice - value.price) / value.oldPrice) * 100) + "%</th></tr>";
                    requestTable += "<tr><th style='text-align: center;color: orangered'>" + value.oldPrice + ".грн" + "</th>" +
                        "<th style='text-align: center'>" + value.company + "</th></tr>";

                }

                requestTable += "</table></div>";

            });


            if (data.result.length === 0) {

                $('.panel-body').html(requestTable += "<tr><td colspan='2'>" + data["msg"] + "</td></tr>");
                requestTable += "</table>";

            } else {

                requestTable += "</table>";
                $('.panel-body').html(requestTable);

            }

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        }


            if(data.pageResult!=null) {

                $('#pagination-list').empty();

                $.each(data.pageResult.content, function (index, value) {

                    requestTable += "<div class=\"col-xs-3\">" +
                        "<table class=\"table table-striped table-bordered\">";
                    requestTable += "<tr><th style='background-color: white;' colspan='2'><img width='250' height='250' src=" + value.image + "></th></tr>";

                    if(value.price!==0) {

                        requestTable += "<tr><th colspan='2'>" + value.name + "</th></tr>";
                        requestTable += "<tr><th style='color: green;width: 50%;text-align: center'>" + value.price + ".грн" + "</th>" +
                            "<th style='text-align: center'>" + Math.round(value.oldPrice === 0 ? 0 : ((value.oldPrice - value.price) / value.oldPrice) * 100) + "%</th></tr>";
                        requestTable += "<tr><th style='text-align: center;color: orangered'>" + value.oldPrice + ".грн" + "</th>" +
                            "<th style='text-align: center'>" + value.company + "</th></tr>";

                    }

                    requestTable += "</table></div>";

                });


                if (data.pageResult.content.length === 0) {

                    $('.panel-body').html(requestTable += "<tr><td colspan='2'>" + data["msg"] + "</td></tr>");
                    requestTable += "</table>";

                } else {

                    requestTable += "</table>";
                    $('.panel-body').html(requestTable);


                    var pages = data.totalPage;

                    create_pagination(pages,data)

                }

                console.log("SUCCESS : ", data);
                $("#btn-search").prop("disabled", false);

            }



        },
        error: function (e) {

            var requestTable = "<table class='table table-striped table-bordered'>";
            if (e.responseText.result == null) {
                $('.panel-body').html(requestTable += "<tr><td colspan='2'>Write something</td></tr>");
            }
            requestTable += "</div></div>";

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}


function create_pagination(pages,data) {

    $('#pagination-list-company').bootpag({
        total: pages,
        maxVisible: 5,
        wrapClass: 'pagination pagination-lg',
        activeClass: 'activeClass',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first',
    }).on("page", function (event, num) {


        var company = data.company;

        click_ajax_page(num,company);

    });

}